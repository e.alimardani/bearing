import { createStore , applyMiddleware , compose } from 'redux';
import thunk from 'redux-thunk';

import reducers from '../Reducer/Index';

const store = createStore(
    reducers,
    undefined,
    compose(
        applyMiddleware(thunk)
    )
);

export default store;