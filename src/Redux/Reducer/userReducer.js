import {SET_USER} from "../Actions/Type";

const initialState = {
    firstname: null,
    lastname:null,
    nationnal_code:null,
    phone_number:null,
    apiToken: null,
    role:null,
    id:null,
    email:null,
    username:null,
    car_type:null,
    plak_no:null,
};

export default user = (state = initialState, action = {}) => {

    switch (action.type)
    {
        case SET_USER:
            const { user } = action;
            return {
                firstname: user.firstname,
                lastname:user.lastname,
                nationnal_code:user.nationnal_code,
                phone_number:user.phone_number,
                apiToken: user.apiToken,
                role:user.role,
                id:user.id,
                email: user.email,
                username:user.username,
                car_type:user.car_type,
                plak_no:user.plak_no
            };
            break;
        default:
            return state;
    }
}