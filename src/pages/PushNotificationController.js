import React, { Component } from "react";
import {Platform} from 'react-native';
import firebase  from 'react-native-firebase';
import {connect} from "react-redux";
import AsyncStorage from '@react-native-community/async-storage';

class PushNotificationController extends Component {
    constructor(props) {
        super(props);

        //console.log(props);
        this.getToken = this.getToken.bind(this);
        this.checkPermission = this.checkPermission.bind(this);
        this.requestPermission = this.requestPermission.bind(this);
        this.setBadge = this.setBadge.bind(this);
        this.handleNotif = this.handleNotif.bind(this);
    }

    async componentDidMount() {

        if(await AsyncStorage.getItem('androidChannel') === null && Platform.OS === 'android'){
            const channel = new firebase.notifications.Android.Channel('default', 'Default', firebase.notifications.Android.Importance.Max)
                .setDescription('Pishbiner Notification Channel');
            firebase.notifications().android.createChannel(channel);
            await AsyncStorage.setItem('androidChannel','true')
        }

        await this.checkPermission();

        await this.setBadge(0);

        this.removeNotificationListener = firebase.notifications().onNotification(async (notification) => {
            //console.log('receive');
            await this.showNotification(notification);
        });

        this.removeNotificationOpenedListener = firebase.notifications().onNotificationOpened(async (notificationOpen) => {
            //console.log('open');
            await this.handleNotif(notificationOpen.notification.data);
        });

        const initNotification = await firebase.notifications().getInitialNotification();
        if (initNotification) {
            //console.log('initial');
            this.handleNotif(initNotification.notification.data);
        }
    }

    componentWillUnmount() {
        this.removeNotificationOpenedListener();
        this.removeNotificationListener();
    }

    async handleNotif(data){
        console.warn(data);
    }

    async showNotification(notification){
        const newNotification = new firebase.notifications.Notification()
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setBody(notification.body)
            .setData(notification.data);

        if(Platform.OS === 'android'){
            newNotification
                .android.setChannelId('default')
                .android.setSmallIcon('ic_notification')
                .android.setColor('#013a79');

            if(notification.data.image){
                newNotification
                    .android.setBigPicture(notification.data.image)
                    .android.setLargeIcon(notification.data.image);
            }
        }
        else {
            newNotification.
                ios.setBadge(0);

            if(notification.data.image){
                newNotification.ios.setLaunchImage(notification.data.image);
            }
        }

        firebase.notifications().displayNotification(newNotification)
    }

    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            await this.getToken();
        } else {
            await this.requestPermission();
        }
    }
    async getToken() {
        let fcmToken = await firebase.messaging().getToken();
        this.registerToken(fcmToken)
        console.log(fcmToken)
    }

    async registerToken(token)
    {
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("reg_id", token);
            formData.append("d_type", Platform.OS === 'ios' ? 0 : 1);

            if(this.props.user.apiToken !== null){
                formData.append("usertoken", this.props.user.apiToken);
            }

            let response = await fetch(this.props.global.baseApiUrl + '/notification/adddevice',
                {
                    method: "POST",
                    body: formData
                });
            console.log(response)

            //console.log(Platform.OS);

        } catch (error) {
            console.log(error);
        }
    }
    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            await this.getToken();
        } catch (error) {
            console.log('permission rejected');
        }
    }
    async setBadge(count){
        if(Platform.OS === 'ios'){
            const notification = new firebase.notifications.Notification();
            notification.ios.setBadge(count);

            firebase.notifications().displayNotification(notification);
        }
    }

    render() {
        return null;
    }

}

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,null)(PushNotificationController);