import React, {Component} from 'react';
import {View, Animated,FlatList, StatusBar, Image,ImageBackground, Dimensions, Linking, Text, TouchableOpacity,StyleSheet,ActivityIndicator
    ,Platform,Slider} from 'react-native';
import BottomNavigate from '../assets/components/bottomNavigate';
import Icon from "react-native-vector-icons/Feather";
import IconFooter from 'react-native-vector-icons/FontAwesome5';
import IconF from "react-native-vector-icons/FontAwesome5";
import IconM from "react-native-vector-icons/MaterialIcons";
import {Header, Body, Button, Left, Right, Footer, Content, Container, Form, Item, Input,FooterTab} from "native-base";
import {Actions} from "react-native-router-flux";
import Modal from "react-native-modal";
import {connect} from "react-redux";
import {setUser} from '../Redux/Actions/index';
import PushNotif from './PushNotificationController';
import Helper from '../assets/components/Helper'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
class Profile extends Component {

    state = {
        isModalFirstName:false,
        isModalLastName:false,
        isModalNational:false,
        isModalPhone:false,
        isModalUsername:false,
        isModalPassword:false,
        isModalSetPlate:false,
        isModalChangeCar:false,
        isModalCar_typeById:false,
        isModalCar_type:false,
        Loading:false,
        NewFirstName:'',
        NewLastName:'',
        NewNational:'',
        NewPhone:'',
        NewUsename:'',
        NewPassword:'',
        NewCarType:'',
        NewPlate:'',
        message:'',
        plateCarDriver1:'',
        plateCarDriver2:'',
        plateCarDriver3:'',
        plateCarDriver4:'',
        CarTypesById:[],
        carTypes:[],
        loading_carType:false,
        carSelectedId_second:'',
        carSelectedName_second:'',
        carSelectedId:'',
        carSelectedName:'',
        loading_carTypeById:false

    }
    constructor(props) {
        
        super(props);
    }
    componentWillMount(){
        this.GetCarById()
    }
    async GetCarById(){
        if(this.props.user.role=="driver"){
            try {
                let formData = new FormData();
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("car_id", this.props.user.car_type);
                console.log(formData)

                let response = await fetch(this.props.global.baseApiUrl + '/product/get_carname_by_id',
                    {
                        method: "POST",
                        body: formData,
                    });
                    let json = await response.json();
                    console.log(json)
    
                    if(json.success){
                        
                        this.setState({CarTypesById:json.data})    
                    }
                    else{
                        true  
                    }
            } catch (error) {
                console.log(error)         
            }
        }

    }
    async getCarType(){
        try {

            let formData = new FormData();

            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + "/product/get_carCategory_list",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                console.log(json)

                this.setState({carTypes:json.data,loading_carType:false})
            
            } catch (error) {
                console.log(error)
            }
    }
    async getCarTypeById(){
        try {

            let formData = new FormData();

            formData.append("admintoken", "2dab06cddd5a3d6e6a40b874a457c25de769963bf4485c3e004b570bf74a4118");
            formData.append("car_category_id", this.state.carSelectedId);

            let response = await fetch(this.props.global.baseApiUrl + "/product/get_car_by_carCategory_id",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                //console.log(response)

                this.setState({carTypesById:json.data,loading_carTypeById:false})
            
            } catch (error) {
                console.log(error)
            }
    }
    renderCarType({item}){
        return (
            <TouchableOpacity style={{padding:15,borderBottomColor:'#eaeaea',borderBottomWidth:1}}
                onPress={()=>this.setState({carSelectedId:item.id , carSelectedName:item.name , isModalCar_type:false,isModalCar_typeById:true,loading_carTypeById:true},()=>this.getCarTypeById())}>
                <Text style={{fontFamily:'IRANSansMobile',fontSize:17,textAlign:'center',paddingRight:10}}>{item.name}</Text>
            </TouchableOpacity>
        );
    }
    renderCarTypeById({item}){
        return (
            <TouchableOpacity style={{padding:15,borderBottomColor:'#eaeaea',borderBottomWidth:1}}
                onPress={()=>this.setState({carSelectedId_second:item.id , carSelectedName_second:item.name ,isModalCar_typeById:false,isModalChangeCar:true})}>
                <Text style={{fontFamily:'IRANSansMobile',fontSize:17,textAlign:'center',paddingRight:10}}>{item.name}</Text>
            </TouchableOpacity>
        );
    }
    async UpdateUser(
        username,
        firstname,
        lastname,
        password,
        phone_number,
        national_code,
        plate,
        car_type_id,
    ){
        this.setState({Loading:true,message:''})

        if(this.props.user.role=="driver"){
            try {
                let formData = new FormData();
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("usertoken", this.props.user.apiToken);
                formData.append("username", username);
                formData.append("firstname", firstname);
                formData.append("lastname", lastname);
                {password !=''? formData.append("password", password) : true}
                formData.append("phone_number", phone_number);
                formData.append("nationnal_code", national_code);
                formData.append("plak_no",plate);
                formData.append("car_type", car_type_id);

                console.log(formData)
                let response = await fetch(this.props.global.baseApiUrl + '/user/update',
                    {
                        method: "POST",
                        body: formData,
                    });
                    let json = await response.json();
                    console.log(json)
    
                    if(json.success){
                        this.addInRedux(json)
                        this.setState({Loading:false})    
                    }
                    else{
                        this.setState({Loading:false})    
                    }
            } catch (error) {
                console.log(error)         
            }
        }
        else{
            try {
                let formData = new FormData();
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("usertoken", this.props.user.apiToken);
                formData.append("username", username);
                formData.append("firstname", firstname);
                formData.append("lastname", lastname);
                {password !=''? formData.append("password", password) : true}
                formData.append("phone_number", phone_number);
                formData.append("nationnal_code",national_code);
                console.log(formData)
                let response = await fetch(this.props.global.baseApiUrl + '/user/update',
                    {
                        method: "POST",
                        body: formData,
                    });
                    let json = await response.json();
                    console.log(json)
                    if(json.success){
                        this.addInRedux(json)
                        this.setState({Loading:false})    
                    }
                    else{
                        this.setState({Loading:false,message:json.messages[0]})    
                    }
            } catch (error) {
                console.log(error)         
            }
        }

    }
    async addInRedux(json){

        await this.props.setUser({
            firstname: json.data.firstname,
            lastname:json.data.lastname,
            id:json.data.id,
            nationnal_code: json.data.nationnal_code,
            phone_number:json.data.phone_number,
            role:json.data.role,
            apiToken:json.data.token,
            username:json.data.username,
            car_type: (json.data.car_type)? json.data.car_type: null,
            //image:json.data.image,
            plak_no:(json.data.plak_no) ? json.data.plak_no : null
        });

    }
    changeName(){
    if(this.state.NewFirstName ==''){
        this.setState({message:'نام نمیتواند خالی باشد'})
    }else{
        this.UpdateUser(
            this.props.user.username,
            this.state.NewFirstName,
            this.props.user.lastname,
            '',
            this.props.user.phone_number,
            this.props.user.nationnal_code
        ).then(()=>this.setState({isModalFirstName:false,NewFirstName:'', message:''}))
    }
    }
    changeLastName(){
        if(this.state.NewLastName ==''){
            this.setState({message:'نام خانوادگی نمیتواند خالی باشد'})
        }else{
            this.UpdateUser(
                this.props.user.username,
                this.props.user.firstname,
                this.state.NewLastName,
                '',
                this.props.user.phone_number,
                this.props.user.nationnal_code
            ).then(()=>this.setState({isModalLastName:false,NewLastName:'', message:''}))
        }
    }
    changeNationalCode(){
        if(this.state.NewNational ==''){
            this.setState({message:'کدملی نمیتواند خالی باشد'})
        }else{
            this.UpdateUser(
                this.props.user.username,
                this.props.user.firstname,
                this.props.user.lastname,
                '',
                this.props.user.phone_number,
                this.state.NewNational
            ).then(()=>this.setState({isModalNational:false,NewNational:'', message:''}))
        }
    }
    changePhone(){
        if(this.state.NewPhone ==''){
            this.setState({message:'شماره همراه نمیتواند خالی باشد'})
        }else{
            this.UpdateUser(
                this.props.user.username,
                this.props.user.firstname,
                this.props.user.lastname,
                '',
                this.state.NewPhone,
                this.props.user.nationnal_code
            ).then(()=>this.setState({isModalPhone:false,NewPhone:'', message:''}))
        }
    }
    changeUserName(){
        if(this.state.NewUsename ==''){
            this.setState({message:'نام کاربری نمیتواند خالی باشد'})
        }else{
            this.UpdateUser(
                this.state.NewUsename,
                this.props.user.firstname,
                this.props.user.lastname,
                '',
                this.props.user.phone_number,
                this.props.user.nationnal_code
            ).then(()=>this.setState({isModalUsername:false,NewUsename:'', message:''}))
        }
    }
    changePassword(){
        if(this.state.NewPassword ==''){
            this.setState({message:'رمزعبور نمیتواند خالی باشد'})
        }else{
            this.UpdateUser(
                this.props.user.username,
                this.props.user.firstname,
                this.props.user.lastname,
                this.state.NewPassword,
                this.props.user.phone_number,
                this.props.user.nationnal_code
            ).then(()=>this.checkPass())
        }
    }
    checkPass(){
        if(this.state.message == ''){
            this.setState({isModalPassword:false,NewPassword:'', message:''})
        }else{true}
    }
    changePlate(){
        if(this.state.NewPlate.length < 8){
            this.setState({message:'شماره پلاک نمیتواند خالی باشد'})
        }else{
            this.UpdateUser(
                this.props.user.username,
                this.props.user.firstname,
                this.props.user.lastname,
                '',
                this.props.user.phone_number,
                this.props.user.nationnal_code,
                this.state.NewPlate
            ).then(()=>this.setState({isModalSetPlate:false,NewPlate:'', message:'',plateCarDriver1:'',plateCarDriver2:'',plateCarDriver3:'',plateCarDriver4:''}))
        }
    }
    changeCar(){
        if(this.state.carSelectedName_second == ''){
            this.setState({message:'شماره پلاک نمیتواند خالی باشد'})
        }else{
            this.UpdateUser(
                this.props.user.username,
                this.props.user.firstname,
                this.props.user.lastname,
                '',
                this.props.user.phone_number,
                this.props.user.nationnal_code,
                this.props.user.plak_no,
                this.state.carSelectedId_second
            ).then(()=>this.setState({isModalChangeCar:false, message:''},()=>this.GetCarById()))
        }
    }
    render() {
        return (
            <Container style={{backgroundColor: '#fff', flex: 1}}>
                <Header style={{backgroundColor:'#2395ff',height:80,paddingTop:10,opacity:.9}}>
                    <Left style={{flex:.3,alignItems:'center',justifyContent:'center'}}>
                        <TouchableOpacity onPress={()=>Actions.pop()}>
                            <IconFooter style={{color:'white',fontSize:28}} name="arrow-left"/>
                        </TouchableOpacity>
                    </Left>
                    <Body/>
                    <Right style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                        <Text style={style.editText}>ویرایش پروفایل</Text>
                    </Right>
                </Header>
                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />            
                <Content>
                    <TouchableOpacity onPress={()=>this.setState({isModalFirstName:true})} style={[style.TouchItem,{marginTop:15}]}>
                        <Text style={style.TextProfile}>نام : </Text>
                        <Text style={style.TextInput}>{this.props.user.firstname}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.setState({isModalLastName:true})} style={style.TouchItem}>
                        <Text style={style.TextProfile}>نام خانوادگی : </Text>
                        <Text style={style.TextInput}>{this.props.user.lastname}</Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity onPress={()=>this.setState({isModalNational:true})} style={style.TouchItem}>
                        <Text style={style.TextProfile}>کد ملی : </Text>
                        <Text style={style.TextInput}>{Helper.ToPersianNumber(this.props.user.nationnal_code)}</Text>
                    </TouchableOpacity> */}
                    <View style={style.TouchItem}>
                        <Text style={style.TextProfile}>شماره همراه : </Text>
                        <Text style={style.TextInput}>{Helper.ToPersianNumber(this.props.user.phone_number)}</Text>
                    </View>
                    {/* <TouchableOpacity onPress={()=>this.setState({isModalUsername:true})} style={style.TouchItem}>
                        <Text style={style.TextProfile}>نام کاربری : </Text>
                        <Text style={style.TextInput}>{this.props.user.username}</Text>
                    </TouchableOpacity> */}
                    {/* <TouchableOpacity onPress={()=>this.setState({isModalPassword:true})} style={style.TouchItem}>
                        <Text style={style.TextProfile}>رمز عبور : </Text>
                        <Text style={[style.TextInput,{marginBottom:-10}]}>******</Text>
                    </TouchableOpacity> */}
                    {this.props.user.role === "driver" &&
                    <TouchableOpacity onPress={()=>this.setState({isModalCar_type:true , loading_carType:true},()=>this.getCarType())} style={style.TouchItem}>
                        <Text style={style.TextProfile}>نوع خودرو : </Text>
                        <Text style={[style.TextInput,{flex:1}]}>{this.state.CarTypesById.category_name} {","} {this.state.CarTypesById.car_name}</Text>
                    </TouchableOpacity>
                    }
                    {this.props.user.role === "driver"  &&
                    <TouchableOpacity onPress={()=>this.setState({isModalSetPlate:true})} style={[style.TouchItem]}>
                        <Text style={style.TextProfile}>پلاک خودرو : </Text>
                        <View style={{flex:1,backgroundColor:'#eee',paddingTop:0}}>
                            <ImageBackground source={require('../assets/images/plate.jpg')} style={{width:150,height:45,marginHorizontal:10,alignSelf:'center',justifyContent:'center'}} resizeMode='contain'>
                                <View style={{width:135,flexDirection:'row-reverse',flex:1,marginHorizontal:10,justifyContent:'space-around',alignItems:'center'}}>
                                    <View style={{flex:.3,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                                        <Text style={{textAlign:'center',padding:0,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:18,color:'black'}}>{Helper.ToPersianNumber(this.props.user.plak_no.slice(6,8))}</Text>                                   
                                    </View>
                                    <View style={{flex:1,flexDirection:'row-reverse',marginHorizontal:15}}>
                                        <Text style={{textAlign:'center',padding:0,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:18,color:'black'}}>{Helper.ToPersianNumber(this.props.user.plak_no.slice(3,6))}</Text>
                                        <Text style={{textAlign:'center',marginBottom:5,padding:0,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:18,color:'black'}}>{this.props.user.plak_no.slice(2,3)}</Text>
                                        <Text style={{textAlign:'center',padding:0,marginRight:-10,justifyContent:'center',flex:.7,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:18,color:'black'}}>{Helper.ToPersianNumber(this.props.user.plak_no.slice(0,2))}</Text>
                                    </View>
                                </View>
                            </ImageBackground>
                        </View>
                    </TouchableOpacity>
                    }
                </Content>
                {/*first name*/}
                <Modal
                    style={style.modalStyle}
                    isVisible={this.state.isModalFirstName}
                    onBackdropPress={() => this.setState({ isModalFirstName: false,NewFirstName:'',message:''})}
                    onBackButtonPress={() => this.setState({ isModalFirstName: false,NewFirstName:'',message:''})}>
                    <Container style={style.ContainerStyle}>
                        <View style={style.ViewModal}>
                            <Text style={style.titleModal}>ویرایش نام</Text>
                            <View style={[style.TouchItem,{height:50}]}>
                                <Text style={style.TextProfile}>نام : </Text>
                                <Input
                                    value={this.state.NewFirstName}
                                    onChangeText={(text)=>this.setState({NewFirstName:text})}
                                    placeholder={this.props.user.firstname}
                                    style={style.InputTitle}/>
                            </View>
                            {this.state.message !='' && <Text style={style.errorText}>{this.state.message}</Text>}
                            {this.state.Loading && <ActivityIndicator style={{marginBottom:10}} size="large" color="#2395ff"/>}
                            {!this.state.Loading &&
                            <TouchableOpacity
                                    onPress={()=>this.changeName()}
                                    style={style.BtnSubmit}>
                                <Text style={style.acceptText}>تایید</Text>
                            </TouchableOpacity>
                            }
                        </View>
                    </Container>
                </Modal>

                {/*last name*/}
                <Modal
                    style={style.modalStyle}
                    isVisible={this.state.isModalLastName}
                    onBackdropPress={() => this.setState({ isModalLastName: false,message:'',NewLastName:''})}
                    onBackButtonPress={() => this.setState({ isModalLastName: false,message:'',NewLastName:''})}>
                    <Container style={style.ContainerStyle}>
                        <View style={style.ViewModal}>
                            <Text style={style.titleModal}>ویرایش نام خانوادگی</Text>
                            <View style={[style.TouchItem,{height:50}]}>
                                <Text style={style.TextProfile}>نام خانوادگی : </Text>
                                <Input
                                    value={this.state.NewLastName}
                                    onChangeText={(text)=>this.setState({NewLastName:text})}
                                    placeholder={this.props.user.lastname}
                                    style={style.InputTitle}/>
                            </View>
                            {this.state.message !='' && <Text style={style.errorText}>{this.state.message}</Text>}
                            {this.state.Loading && <ActivityIndicator style={{marginBottom:10}} size="large" color="#2395ff"/>}
                            {!this.state.Loading &&
                            <TouchableOpacity onPress={()=>this.changeLastName()} style={style.BtnSubmit}>
                                <Text style={style.acceptText}>تایید</Text>
                            </TouchableOpacity>
                            }
                        </View>
                    </Container>
                </Modal>

                {/*national*/}
                <Modal
                    style={style.modalStyle}
                    isVisible={this.state.isModalNational}
                    onBackdropPress={() => this.setState({ isModalNational: false,message:'',NewNational:'' })}
                    onBackButtonPress={() => this.setState({ isModalNational: false,message:'',NewNational:''})}>
                    <Container style={style.ContainerStyle}>
                        <View style={style.ViewModal}>
                            <Text style={style.titleModal}>ویرایش کدملی</Text>
                            <View style={[style.TouchItem,{height:50}]}>
                                <Text style={style.TextProfile}>کدملی : </Text>
                                <Input
                                    value={this.state.NewNational}
                                    onChangeText={(text)=>this.setState({NewNational:text})}
                                    keyboardType="numeric"
                                    placeholder={Helper.ToPersianNumber(String(this.props.user.nationnal_code))}
                                    style={style.InputTitle}/>
                            </View>
                            {this.state.message !='' && <Text style={style.errorText}>{this.state.message}</Text>}
                            {this.state.Loading && <ActivityIndicator style={{marginBottom:10}} size="large" color="#2395ff"/>}
                            {!this.state.Loading &&
                            <TouchableOpacity onPress={()=>{this.changeNationalCode()}} style={style.BtnSubmit}>
                                <Text style={style.acceptText}>تایید</Text>
                            </TouchableOpacity>
                            }
                        </View>
                    </Container>
                </Modal>

                {/*phone*/}
                <Modal
                    style={style.modalStyle}
                    isVisible={this.state.isModalPhone}
                    onBackdropPress={() => this.setState({ isModalPhone: false,message:'',NewPhone:''  })}
                    onBackButtonPress={() => this.setState({ isModalPhone: false,message:'',NewPhone:'' })}>
                    <Container style={style.ContainerStyle}>
                        <View style={style.ViewModal}>
                            <Text style={style.titleModal}>ویرایش شماره همراه</Text>
                            <View style={[style.TouchItem,{height:50}]}>
                                <Text style={style.TextProfile}>شماره همراه : </Text>
                                <Input
                                    value={this.state.NewPhone}
                                    onChangeText={(text)=>this.setState({NewPhone:text})}
                                    maxLength={11}
                                    keyboardType="numeric"
                                    placeholder={Helper.ToPersianNumber(String(this.props.user.phone_number))}
                                    style={style.InputTitle}/>
                            </View>
                            {this.state.message !='' && <Text style={style.errorText}>{this.state.message}</Text>}
                            {this.state.Loading && <ActivityIndicator style={{marginBottom:10}} size="large" color="#2395ff"/>}
                            {!this.state.Loading &&
                            <TouchableOpacity onPress={()=>{this.changePhone()}} style={style.BtnSubmit}>
                                <Text style={style.acceptText}>تایید</Text>
                            </TouchableOpacity>
                            }
                        </View>
                    </Container>
                </Modal>

                {/*username*/}
                <Modal
                    style={style.modalStyle}
                    isVisible={this.state.isModalUsername}
                    onBackdropPress={() => this.setState({ isModalUsername: false,message:'',NewUsename:''})}
                    onBackButtonPress={() => this.setState({ isModalUsername: false,message:'',NewUsename:''})}>
                    <Container style={style.ContainerStyle}>
                        <View style={style.ViewModal}>
                            <Text style={style.titleModal}>ویرایش نام کاربری</Text>
                            <View style={[style.TouchItem,{height:50}]}>
                                <Text style={style.TextProfile}>نام کاربری : </Text>
                                <Input
                                    value={this.state.NewUsename}
                                    onChangeText={(text)=>this.setState({NewUsename:text})}
                                    placeholder={this.props.user.username}
                                    style={style.InputTitle}/>
                            </View>
                            {this.state.message !='' && <Text style={style.errorText}>{this.state.message}</Text>}
                            {this.state.Loading && <ActivityIndicator style={{marginBottom:10}} size="large" color="#2395ff"/>}
                            {!this.state.Loading &&
                            <TouchableOpacity onPress={()=>{this.changeUserName()}} style={style.BtnSubmit}>
                                <Text style={style.acceptText}>تایید</Text>
                            </TouchableOpacity>
                            }
                        </View>
                    </Container>
                </Modal>

                {/*password*/}
                <Modal
                    style={style.modalStyle}
                    isVisible={this.state.isModalPassword}
                    onBackdropPress={() => this.setState({ isModalPassword: false })}
                    onBackButtonPress={() => this.setState({ isModalPassword: false})}
                    backdropOpacity={0}>
                    <Container style={style.ContainerStyle}>
                        <View style={style.ViewModal}>
                            <Text style={style.titleModal}>تغییر رمزعبور</Text>
                            <View style={[style.TouchItem,{height:50}]}>
                                <Text style={style.TextProfile}>رمزعبور : </Text>
                                <Input 
                                    secureTextEntry={true}
                                    value={this.state.NewPassword}
                                    onChangeText={(text)=>this.setState({NewPassword:text})}
                                    placeholder="******"
                                    style={style.InputTitle}/>
                            </View>
                            {this.state.message !='' && <Text style={style.errorText}>{this.state.message}</Text>}
                            {this.state.Loading && <ActivityIndicator style={{marginBottom:10}} size="large" color="#2395ff"/>}
                            {!this.state.Loading &&
                            <TouchableOpacity onPress={()=>this.changePassword()} style={style.BtnSubmit}>
                                <Text style={style.acceptText}>تایید</Text>
                            </TouchableOpacity>
                            }
                        </View>
                    </Container>
                </Modal>

                {/*car type*/}
                <Modal
                    style={style.modalStyle}
                    isVisible={this.state.isModalChangeCar}
                    onBackdropPress={() => this.setState({ isModalChangeCar: false })}
                    onBackButtonPress={() => this.setState({ isModalChangeCar: false})}>
                    <Container style={style.ContainerStyle}>
                        <View style={style.ViewModal}>
                            <Text style={style.titleModal}>تغییر نوع خودرو</Text>
                            <View style={[style.TouchItem,{height:60}]}>
                                <Text style={style.TextProfile}>نوع خودرو : </Text>
                                <Text style={[style.InputTitle,{flex:1}]}>
                                {this.state.carSelectedName} {","} {this.state.carSelectedName_second}
                                </Text>
                            </View>
                            {this.state.message !='' && <Text style={style.errorText}>{this.state.message}</Text>}
                            {this.state.Loading && <ActivityIndicator style={{marginBottom:10}} size="large" color="#2395ff"/>}
                            {!this.state.Loading &&
                            <TouchableOpacity onPress={()=>this.changeCar()} style={style.BtnSubmit}>
                                <Text style={style.acceptText}>تایید</Text>
                            </TouchableOpacity>
                            }
                        </View>
                    </Container>
                </Modal>
                
                {/*car plate*/}
                {this.props.user.role === "driver"  &&
                <Modal style={{borderRadius:50,alignItems:'center',justifyContent:'center',alignSelf:'center'}} isVisible={this.state.isModalSetPlate}
                    onBackdropPress={() => this.setState({ isModalSetPlate: false ,NewPlate:'', message:'',plateCarDriver1:'',plateCarDriver2:'',plateCarDriver3:'',plateCarDriver4:'' })}
                    onBackButtonPress={() => this.setState({ isModalSetPlate: false ,NewPlate:'', message:'',plateCarDriver1:'',plateCarDriver2:'',plateCarDriver3:'',plateCarDriver4:''})}>
                    <Container style={{paddingTop:10,borderRadius:50,alignItems:'center',justifyContent:'center',backgroundColor:'transparent'}}>
                        <View style={{backgroundColor:'white',borderRadius:20,paddingTop:10}}>
                        <Text style={style.titleModal}>تغییر شماره پلاک</Text>
                        <ImageBackground source={require('../assets/images/plate.jpg')} style={{width:250,height:100,marginHorizontal:10,alignSelf:'center',justifyContent:'center'}} resizeMode='contain'>
                            <View style={{flexDirection:'row-reverse',width:230,marginHorizontal:25,justifyContent:'space-around',alignItems:'center'}}>
                                <View style={{flex:.3,flexDirection:'row',justifyContent:'center'}}>
                                <Input
                                        onChangeText={(text)=>this.setState({plateCarDriver1:text})}
                                        value={this.state.plateCarDriver1}
                                        placeholder={this.props.user.plak_no.slice(6,8)}
                                        keyboardType="numeric"
                                        maxLength={2}
                                        style={{textAlign:'center',padding:0,height:100,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:22,color:'black'}}/>                                    
                                </View>
                                <View style={{flex:.7,flexDirection:'row-reverse'}}>
                                    <Input
                                        onChangeText={(text)=>this.setState({plateCarDriver2:text})}
                                        value={this.state.plateCarDriver2}
                                        placeholder={this.props.user.plak_no.slice(3,6)}
                                        keyboardType="numeric"
                                        maxLength={3}
                                        style={{textAlign:'center',padding:0,height:100,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:22,color:'black'}}/>
                                    {/* <Text  style={{textAlign:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:45,color:'black'}}>۹۶۸</Text> */}
                                    <Input
                                        onChangeText={(text)=>this.setState({plateCarDriver3:text})}
                                        value={this.state.plateCarDriver3}
                                        placeholder={this.props.user.plak_no.slice(2,3)}
                                        maxLength={1}
                                        style={{textAlign:'center',marginBottom:5,padding:0,height:100,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:22,color:'black'}}/>
                                    <Input
                                        onChangeText={(text)=>this.setState({plateCarDriver4:text})}
                                        value={this.state.plateCarDriver4}
                                        placeholder={this.props.user.plak_no.slice(0,2)}
                                        keyboardType="numeric"
                                        maxLength={2}
                                        style={{textAlign:'center',padding:0,height:100,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:22,color:'black'}}/>
                                </View>
                            </View>
                            </ImageBackground>
                            {this.state.message !='' && <Text style={style.errorText}>{this.state.message}</Text>}
                            {this.state.Loading && <ActivityIndicator style={{marginBottom:10}} size="large" color="#2395ff"/>}
                            {!this.state.Loading &&
                            <TouchableOpacity onPress={()=>{this.setState({NewPlate:this.state.plateCarDriver4+this.state.plateCarDriver3+this.state.plateCarDriver2+this.state.plateCarDriver1},()=>this.changePlate())}} style={{backgroundColor:'#2395ff',borderRadius:10,padding:10,marginHorizontal:38,marginBottom:10}}>
                                <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',color:'white',fontSize:18}}>تایید</Text>
                            </TouchableOpacity>
                            }
                        </View>
                    </Container>
                </Modal>
                }

                {/* car_type */}
                <Modal style={{marginVertical:70,borderRadius:50}} isVisible={this.state.isModalCar_type}
                    onBackdropPress={() => this.setState({ isModalCar_type: false })} onBackButtonPress={() => this.setState({ isModalCar_type: false})}>
                    <Container style={{paddingHorizontal:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>خودرو مورد نظر را انتخاب کنید</Text>
                        {/* <View style={{height:50,width:"90%",marginTop:5}}>
                        <Input
                            style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                            placeholder="جستجو ..."                        
                            onChangeText={text => this.searchFilterCarType(text)}
                            autoCorrect={false}             
                        />  
                        </View> */}
                        <Content style={{width:'100%',marginBottom:10}}>
                            {(this.state.loading_carType) && <ActivityIndicator color   ="#2395ff" size="large"/>}
                            {(!this.state.loading_carType) && 
                                <FlatList
                                    data={this.state.carTypes}
                                    renderItem={this.renderCarType.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            }
                        </Content>
                        
                    </Container>
                </Modal>
                {/* car_type by id*/}
                <Modal style={{marginVertical:70,borderRadius:50}} isVisible={this.state.isModalCar_typeById}
                    onBackdropPress={() => this.setState({ isModalCar_typeById: false })} onBackButtonPress={() => this.setState({ isModalCar_typeById: false})}>
                    <Container style={{paddingHorizontal:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>نوع خودرو مورد نظر را انتخاب کنید</Text>
                        {/* <View style={{height:50,width:"90%",marginTop:5}}>
                        <Input
                            style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                            placeholder="جستجو ..."                        
                            onChangeText={text => this.searchFilterCarType(text)}
                            autoCorrect={false}             
                        />  
                        </View> */}
                        <Content style={{width:'100%',marginBottom:10}}>
                            {(this.state.loading_carTypeById) && <ActivityIndicator color   ="#2395ff" size="large"/>}
                            {(!this.state.loading_carTypeById) && 
                                <FlatList
                                    data={this.state.carTypesById}
                                    renderItem={this.renderCarTypeById.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            }
                        </Content>
                        
                    </Container>
                </Modal>
            </Container>
        )
    }
}
const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Profile);

const style = StyleSheet.create({
    errorText:{
        color:'red',
        textAlign:'center',
        fontFamily:'IRANSansMobile',
        paddingBottom:5,
        marginTop:-10
    },
    logo_top: {width: width * .11, height: width * .13},
    TouchItem:{
        flexDirection:'row-reverse',
        alignItems:'center',
        borderColor:'#2395ff',
        borderWidth:1,
        borderRadius:10,
        padding:10,
        margin:15,
        marginHorizontal:30,
        marginTop:0,
        backgroundColor:'#eee'
    },
    TextProfile:{
        fontFamily:'IRANSansMobile',
        color:'black',
        opacity:.8,
        fontSize:16
    },
    TextInput:{
        fontFamily:'IRANSansMobile',
        color:'black',
        fontSize:16
    },
    editText:{
        color:'white',
        fontFamily:'IRANSansMobile',
        fontSize:16
    },
    acceptText:{
        textAlign:'center',
        fontFamily:'IRANSansMobile',
        color:'white',
        fontSize:18
    },
    BtnSubmit:{
        backgroundColor:'#2395ff',
        borderRadius:10,
        padding:10,
        marginHorizontal:80,
        marginBottom:15
    },
    InputTitle:{
        fontFamily:'IRANSansMobile',
        color:'black',
        fontSize:16,
        padding:0 ,
        textAlign:'right'
    },
    titleModal:{
        textAlign:'center',
        fontFamily:'IRANSansMobile',
        color:'black',
        fontSize:18,
        marginBottom:10
    },
    ViewModal:{
        backgroundColor:'white',
        borderRadius:20,
        paddingTop:10,
        width:width*.8,
    },
    ContainerStyle:{
        paddingTop:10,
        borderRadius:50,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'transparent'
    },
    modalStyle:{
        borderRadius:50,
        alignItems:'center',
        justifyContent:'center',
        alignSelf:'center'
    }

});