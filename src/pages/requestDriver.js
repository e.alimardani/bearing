import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    StatusBar,
    Image,
    Dimensions,
    Text,
    Platform,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import {Header, Body, Button, Left, Input, Item, Content, Container} from "native-base";
import Icon from "react-native-vector-icons/SimpleLineIcons";
import Modal from "react-native-modal";
import CodeInput from 'react-native-confirmation-code-input';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;


export default class requestDriver extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <Container style={style.view_home_container}>


                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                <View style={{width: width, height: height * .30, backgroundColor: '#000'}}>
                    <Image source={require('../assets/images/mappng.png')} style={style.img_top_home}/>
                </View>
                <View style={{
                    width: width,
                    height: height * .70,
                    backgroundColor: '#7af06c',
                    borderTopRightRadius: width * .15,
                    borderTopLeftRadius: width * .15
                }}>
                    <Modal isVisible={true}
                           deviceWidth={width}
                           deviceHeight={height * .30}
                           animationType={"slide"}
                           hasBackdrop={true}
                           coverScreen={false}
                           backdropColor={'#203fff'}
                           backdropOpacity={0.70}
                           transparent={false}
                           onRequestClose={() => {
                               alert("Modal has been closed.")
                           }}
                        // onBackdropPress={() => this.setState({isModalVisibleSignUpDriver: !this.state.isModalVisibleSignUpDriver})}
                           style={style.styleModalSignUpDriver}
                    >
                        <View style={[style.view_containerModalSignUp,]}>
                            <TouchableOpacity title="Hide modal"
                                              onPress={() => this.setState({isModalVisibleSignUpDriver: false})}
                                              style={style.exitModalLogin}>
                                <Image source={require('../assets/images/request.png')}
                                       style={style.img_exitModalLogin}
                                       resizeMode={'contain'}/>
                            </TouchableOpacity>
                            <Content style={{width:width}}>
                                <View style={{alignItems: 'center', justifyContent: 'center',}}>
                                    <Text style={style.btnLoginAccount}>درخواست حمل و نقل <Text
                                        style={style.txt_confirm}>(در
                                        حال تایید)</Text></Text>
                                    <View style={style.border_underline}/>

                                    <Text style={style.txt_request_name}>نام درخواست کننده : <Text
                                        style={style.txt_confirm}>محسن
                                        اقتدارنسب</Text></Text>
                                    <Text style={style.btnLoginAccount1}>اطلاعات مبدا</Text>
                                    <Image source={require('../assets/images/placeholder.png')} resizeMode={'contain'}
                                           style={style.img_placeholder}/>
                                    <View style={style.view_txt_placeholder}>
                                        <Text style={style.btnLoginAccount1}>تهران</Text>
                                        <Text style={style.btnLoginAccount1}>مشهد</Text>
                                    </View>
                                    <View style={style.view_txt_address}>
                                        <Text style={style.btnLoginAccount12}><Text>نشانی دقیق: </Text>مشهد، خیابان
                                            سعیدی،
                                            کوی بهشتی، کوچه 29، بلوک 32، واحد 18</Text>
                                    </View>
                                    <View style={style.view_txt_address1}>
                                        <Text style={style.btnLoginAccount12}><Text>نوع ماشین مورد
                                            نیاز: </Text>کامیون</Text>
                                        <Text style={style.btnLoginAccount12}><Text>تناژ بار: </Text>حدود 4 تن</Text>
                                    </View>
                                    <Text style={style.txt_count_fix}>قیمت پیشنهادی صاحب بار</Text>
                                    <View style={style.view_txt_count}>
                                        <Text style={style.txt_count}>{'400.000'} تومان</Text>
                                    </View>

                                    <TouchableOpacity activeOpacity={.5} style={style.btnSignUpModal}
                                                      onPress={this.toggleModalConfirmSignUpDriver}>
                                        <Text style={style.txt_btn_login}>تایید درخواست صاحب بار</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity activeOpacity={.5}
                                                      onPress={() => this.setState({isModalVisibleSignUpDriver: false})}>
                                        <Text style={style.cancelRequest}>لغو درخواست</Text>
                                    </TouchableOpacity>
                                </View>
                            </Content>
                        </View>
                    </Modal>
                </View>
            </Container>
        )
    }
}

const style = StyleSheet.create({
    view_txt_placeholder: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        width: width * .92
    },
    img_placeholder: {width: width * .90, height: height * .05,},
    border_underline: {borderWidth: width * .001, borderColor: '#e3e3e3', width: width},
    view_home_container: {justifyContent: 'center', margin: 0, padding: 0, alignItems: 'center', flex: 1,},
    img_top_home: {
        width: width,
        height: height * .40,
        margin: 0,
        padding: 0
    },
    view_center_home: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: height * .40,
    },
    view_btn_login: {
        backgroundColor: '#41ea13', marginTop: height * .02,
        height: height * .09,
        width: width * .80,
        justifyContent: 'space-around', alignItems: 'center', flexDirection: 'row'
    },
    txt_btn_login: {
        fontFamily:'IRANYEKANLIGHT',
        fontSize: width / 20,
        color: '#ffffff'
    },
    styleModalSignUpDriver: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
        padding: 0,
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    view_containerModalSignUp: {
        height: height * .80,
        width: width,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
        justifyContent: 'center', alignItems: 'center',
        paddingHorizontal: width * .05
    },
    img_exitModalLogin: {
        width: width * .18,
        height: width * .18,
        borderRadius: width * .09
    },
    exitModalLogin: {
        position: 'absolute',
        top: height * (-.04),
        width: width * .13,
        height: width * .13,
        borderRadius: width * .07,
        borderColor: '#acc4fb',
        borderWidth: width * .01,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex:1
    },
    btnLoginAccount: {
        fontFamily: 'IRANYEKANLIGHT',
        fontSize: width / 25,
        color: '#222222',
        marginTop: height * .06,
        marginBottom: height * .04
    },
    txt_request_name: {
        fontFamily: 'IRANYEKANLIGHT',
        fontSize: width / 25,
        color: '#222222',
        marginTop: height * .03,
        marginBottom: height * .04
    },
    btnLoginAccount1: {
        fontFamily: 'IRANYEKANBOLD',
        fontSize: width / 25,
        color: '#222222',

        marginBottom: height * .01
    }, btnLoginAccount12: {
        fontFamily: 'IRANYEKANBOLD',
        fontSize: width / 32,
        color: '#222222',

        marginBottom: height * .01
    },
    txt_count_fix: {
        fontFamily: 'IRANYEKANBOLD',
        fontSize: width / 25,
        color: '#222222',
        marginBottom: height * .01
    },
    view_txt_address: {
        marginBottom: height * .01,
        backgroundColor: '#f2f5fa',
        borderRadius: width * .02,
        width: width * .90,
        height: height * .08,
        justifyContent: 'center',
        alignItems: 'center'
    },
    view_txt_count: {
        marginBottom: height * .01,
        backgroundColor: '#2395ff',
        borderRadius: width * .01,
        width: width * .38,
        height: height * .05,
        justifyContent: 'center',
        alignItems: 'center'
    },
    view_txt_address1: {
        marginBottom: height * .01,
        backgroundColor: '#f2f5fa',
        borderRadius: width * .02,
        width: width * .90,
        height: height * .08,
        paddingHorizontal: width * .02,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    txt_count: {
        fontFamily: 'IRANYEKANLIGHT',
        fontSize: width / 28,
        color: '#fff',
    },

    txt_confirm: {color: '#222222'},
    cancelRequest: {
        fontFamily: 'IRANYEKANLIGHT',
        fontSize: width / 25,
        color: '#2395ff',
        marginTop: height * .01,
        marginBottom: height * .01
    },
    btnSignUpModal: {
        marginTop: height * .03,
        width: width * .80,
        height: height * .08,
        backgroundColor: '#2395ff',
        justifyContent: 'center',
        alignItems: 'center'
    },
});