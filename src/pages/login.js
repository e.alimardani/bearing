import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    StatusBar,
    Image,
    Dimensions,
    Text,
    Platform,
    TouchableOpacity,
    BackHandler,
    ActivityIndicator,
    FlatList,
    ImageBackground
    
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Form,Input, Item, Content, Container,Button} from "native-base";
import Icon from "react-native-vector-icons/SimpleLineIcons";
import Modal from "react-native-modal";
import ServerData from "../../connection/connect";
import config from "../../connection/config";
import {Actions} from "react-native-router-flux";
import {ProgressDialog} from 'react-native-simple-dialogs';
import ModalDropdown from 'react-native-modal-dropdown';
import CodeInput from 'react-native-confirmation-code-input';
import {connect} from "react-redux";
import {setUser} from '../Redux/Actions/index';
import Helper from '../assets/components/Helper'
import { helpers } from 'rx';
import { throwStatement } from '@babel/types';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Connect = new ServerData();

class login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            LoadingLogin:true,
            isModalVisibleLogin: false,
            isModalVisibleSignUpDriver: false,
            display_confirm_signUp: 'none',
            display_Driver_signUp: 'none',
            display_User_signUp: 'none',
            firstName: '',
            lastName: '',
            email: '',
            phoneNumber: '',
            nationalCode: '',
            username: '',
            password: '',
            txtWrongUser_Pass: '',
            txtWrongServer: '',

            firstNameDriver: '',
            lastNameDriver: '',
            emailDriver: '',
            phoneNumberDriver: '',
            nationalCodeDriver: '',
            usernameDriver: '',
            passwordDriver: '',
            plateCarDriver: '',
            txtWrongUser_PassDriver: '',
            txtWrongServerDriver: '',
            flex_wrongUserName: 'none',
            flex_wrongServer: 'none',
            flex_wrongPassword: 'none',
            // start .... signUp .... User ......
            flex_wrongFirstName: 'none',
            flex_wrongLastName: 'none',
            flex_wrongNational: 'none',
            flex_wrongEmail: 'none',
            flex_wrongPhone: 'none',
            flex_wrongUser_Name: 'none',
            flex_wrongPass: 'none',

            flex_wrongFirstNameDriver: 'none',
            flex_wrongLastNameDriver: 'none',
            flex_wrongNationalDriver: 'none',
            flex_wrongEmailDriver: 'none',
            flex_wrongPhoneDriver: 'none',
            flex_wrongUser_NameDriver: 'none',
            flex_wrongPassDriver: 'none',
            flex_wrongPlateCarDriver: 'none',
            flex_wrongTypeCarDriver: 'none',
            progressVisible: false,
            userItemError: false,
            userItemSuccess: false,
            passItemError: false,
            passItemSuccess: false,
            passwordItemError: false,
            passwordItemSuccess: false,
            firstNameItemError: false,
            firstNameItemSuccess: false,
            lastNameItemError: false,
            lastNameItemSuccess: false,
            emailItemError: false,
            emailItemSuccess: false,
            phoneNumberItemError: false,
            phoneNumberItemSuccess: false,
            nationalCodeItemError: false,
            nationalCodeItemSuccess: false,
            usernameItemError: false,
            usernameItemSuccess: false,

            passwordItemErrorDriver: false,
            passwordItemSuccessDriver: false,
            firstNameItemErrorDriver: false,
            firstNameItemSuccessDriver: false,
            lastNameItemErrorDriver: false,
            lastNameItemSuccessDriver: false,
            emailItemErrorDriver: false,
            emailItemSuccessDriver: false,
            phoneNumberItemErrorDriver: false,
            phoneNumberItemSuccessDriver: false,
            nationalCodeItemErrorDriver: false,
            nationalCodeItemSuccessDriver: false,
            usernameItemErrorDriver: false,
            usernameItemSuccessDriver: false,
            plateCarItemErrorDriver: false,
            plateCarItemSuccessDriver: false,
            carTypeItemErrorDriver: false,
            carTypeItemSuccessDriver: false,

            carType: [],
            carTypeById:[],
            idCarType:'',
            isModalSetPlate:false,
            plateCarDriver1:'',
            plateCarDriver2:'',
            plateCarDriver3:'',
            plateCarDriver4:'',
            LoadingCarType:false,
            LoadingCarTypeById:false,
            carType_selected:'',
            carType_selectedId:'',
            isModalCarTypeById:false,
            carType_selectedById:'',
            curTime:'',
            canGetNewCode:'',
            isModalCode:false,
            isModalForgetPassword:false,
            message:'',
            carType_selectedById_id:'',
            isModalAddNumber:false,
            isModalCodeForget:false,
            NewPass:'',
            RepeatNewPass:'',
            isModalNewPass:false,
            user_id:''
        }
    }

    async getCarType() {
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            console.log(formData)
            let response = await fetch(this.props.global.baseApiUrl + '/product/get_carCategory_list',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)

                if (json.success === true) {
                    this.setState({carType:json.data,LoadingCarType:false})
                    
                    //this.addInRedux(json)

                } else {

                    this.setMessageServerSignUpUser(json);
                }
                

        } catch (error) {
            console.log(error)         
        }
    };
    async getCarTypeById() {
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("car_category_id", this.state.carType_selectedId);

            console.log(formData)
            let response = await fetch(this.props.global.baseApiUrl + '/product/get_car_by_carCategory_id',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)

                if (json.success === true) {
                    this.setState({carTypeById:json.data,LoadingCarTypeById:false})
                    
                    //this.addInRedux(json)

                } else {

                }
                

        } catch (error) {
            console.log(error)         
        }
    };
    toPersian(context) {
		const arrayMap = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
		const newContext = context.toString().replace(/\d/g, (index) => arrayMap[index]);
		return newContext;
	}
    startTimer() {
        clearInterval(this.codInterval)
		let timeLeft = 59;
		this.setState({ canGetNewCode: false });

		this.setState({ curTime: "۰ : " + this.toPersian(timeLeft) });

		this.codInterval = setInterval(() => {
			timeLeft > 10 ? this.setState({ curTime: "۰ : " + this.toPersian(--timeLeft) }) : this.setState({ curTime: "۰ : ۰" + this.toPersian(--timeLeft) });

			if (timeLeft === 0) {
				clearInterval(this.codInterval)
                this.setState({ curTime: "" });
                this.setState({ canGetNewCode: true });
			}
		}, 1000);
    }
    showRecode() {
		if (this.state.canGetNewCode) {
			return (
				<Button onPress={() => this.sendAgain()} style={{ flex: 1, justifyContent: "center", borderRadius: 5, flexGrow: 1,marginHorizontal:40,backgroundColor:'#2395ff' }}>
					<Text style={{ fontSize: 14,textAlign:'center',fontFamily:'IRANSansMobile',color:'white' }}>ارسال مجدد کد فعال سازی</Text>
				</Button>
			);
		}
		if (!this.state.canGetNewCode) {
			return (
				<Button disabled style={{ borderRadius: 5, opacity: 0.5 ,backgroundColor:'#2395ff'}}>
					<Text style={{ paddingHorizontal:10,fontSize: 14,textAlign:'center',fontFamily:'IRANSansMobile',color:'white' }}>ارسال مجدد کد فعال سازی</Text>
				</Button>
			);
		}
    }
    showTimer() {
		if (this.state.canGetNewCode) {
			return null;
		}
		if (!this.state.canGetNewCode) {
			return (
				<View style={{ backgroundColor:'#ed5d81',opacity:.9,width: 55, height: 45,marginRight:10, alignItems: "center",justifyContent:'center', borderRadius: 5 }}>
					<Text style={{fontSize: 14, color: "white",fontFamily:'IRANSansMobile' }}>{this.state.curTime}</Text>
				</View>
			);
		}
	}
    sendAgain() {
        this.setState({ canGetNewCode: false });
        this.startTimer();
        this.ResendSms()
    }

    async VerifyResetPass(code){
        this.setState({progressVisible: true});
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("code", code);
            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + '/user/verify_reset_pass',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)

                if (json.success === true) {
                    this.addInRedux(json)
                    this.setState({progressVisible: false, isModalCodeForget:false,message:'', isModalNewPass:true});
                } else {
                    this.setState({progressVisible: false , message:json.message});
                }

        } catch (error) {
            console.log(error)         
        }
    }

    async ResetPass(){
        this.setState({progressVisible: true});
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("phone_number", this.state.phoneNumber);
            console.log(formData)
            let response = await fetch(this.props.global.baseApiUrl + '/user/reset_pass_with_mobile',
                {
                    method: "POST",
                    body: formData,
                });
                console.log(response)
                let json = await response.json();
                console.log(json)

                if (json.success) {

                    this.startTimer();
                    this.setState({
                        phoneNumber:'',
                        isModalCodeForget:true,
                        isModalAddNumber:false,
                        progressVisible:false,
                        message:json.sms
});
                } else {
                    this.setState({progressVisible: false, message:json.messages});
                }
                

        } catch (error) {
            console.log(error)         
        }
    }
    componentWillMount() {
        this.GetData();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick = () => {
        return true;
    };

    async Login(){
        this.setState({progressVisible: true});
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("username", this.state.username);
            // formData.append("password", this.state.password);
            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + '/user/login',
                {
                    method: "POST",
                    body: formData,
                });
                console.log(response)
                let json = await response.json();
                console.log(json)

                if (json.success === true) {
                    this.startTimer();

                    this.setState({ user_id:json.user_id,isModalCode:true ,progressVisible:false, LoadingLogin:false,isModalVisibleLogin:false , username:''})
                    // this.addInRedux(json).then(()=>{
                    //     this.setState({progressVisible: false, isModalVisibleLogin:false , username:'', password:'' });
                    //     if(this.props.user.role === "customer"){
                    //         Actions.replace('home');
                    //         this.setState({LoadingLogin:false})
                    //     }else{
                    //         Actions.replace('mainPageDriver');
                    //         this.setState({LoadingLogin:false})
                    //     }
                    // })
                }
                else {
                    this.setState({
                        message: json.messages,
                        progressVisible: false
                    });
                }
        } catch (error) {
            console.log(error)         
        }
    }

    async addInRedux(json){

            await AsyncStorage.setItem('UserToken', json.data.token);
            await AsyncStorage.setItem('role', json.data.role);

            await this.props.setUser({
                firstname: json.data.firstname,
                lastname:json.data.lastname,
                email:json.data.email,
                id:json.data.id,
                nationnal_code: json.data.nationnal_code,
                phone_number:json.data.phone_number,
                role:json.data.role,
                apiToken:json.data.token,
                username:json.data.username,
                car_type: (json.data.car_type)? json.data.car_type: null,
                image:json.data.image,
                plak_no:(json.data.plak_no) ? json.data.plak_no : null
            });

            // if(json.data.role === "customer"){
            //     await Actions.replace('home');
            //     await this.setState({LoadingLogin:false})
            // }else{
            //     await Actions.replace('mainPageDriver');
            //     this.setState({LoadingLogin:false})
            // }
    }

    toggleModalLogin = () => {
        this.setState({isModalVisibleLogin: !this.state.isModalVisibleLogin});
    };
    UserSignUp = () => {
        this.setState({
            flex_wrongFirstName: 'none',
            flex_wrongServer: 'none',
            flex_wrongLastName: 'none',
            flex_wrongNational: 'none',
            flex_wrongEmail: 'none',
            flex_wrongPhone: 'none',
            flex_wrongUser_Name: 'none',
            flex_wrongPass: 'none',
            // flex_wrongPlateCarDriver: 'none',
        });
        let error = false;

        if (this.state.firstName === '') {
            error = true;
            this.setState({
                flex_wrongFirstName: 'flex',
                firstNameItemError: true,
                firstNameItemSuccess: false,
            });
        } else {
            this.setState({
                firstNameItemSuccess: true,
                firstNameItemError: false
            });
        }
        if (this.state.lastName === '') {
            error = true;

            this.setState({
                flex_wrongLastName: 'flex',
                lastNameItemError: true,
                lastNameItemSuccess: false
            });
        } else {
            this.setState({
                lastNameItemError: false,
                lastNameItemSuccess: true
            });
        }
        if (this.state.email === '') {
            error = true;

            this.setState({
                flex_wrongEmail: 'flex',
                emailItemError: true,
                emailItemSuccess: false
            });
        } else {
            this.setState({
                emailItemError: false,
                emailItemSuccess: true
            });
        }
        if (this.state.phoneNumber.length < 11) {
            error = true;

            this.setState({
                flex_wrongPhone: 'flex',
                phoneNumberItemError: true,
                phoneNumberItemSuccess: false
            });
        } else {
            this.setState({
                phoneNumberItemError: false,
                phoneNumberItemSuccess: true
            });
        }
        if (this.state.nationalCode.length < 10) {
            error = true;

            this.setState({
                flex_wrongNational: 'flex',
                nationalCodeItemError: true,
                nationalCodeItemSuccess: false
            });
        } else {
            this.setState({
                nationalCodeItemError: false,
                nationalCodeItemSuccess: true
            });
        }
        if (this.state.username === '') {
            error = true;

            this.setState({
                flex_wrongUser_Name: 'flex',
                usernameItemError: true,
                usernameItemSuccess: false
            });
        } else {
            this.setState({
                usernameItemError: false,
                usernameItemSuccess: true
            });
        }
        if (this.state.password === '') {
            error = true;

            this.setState({
                flex_wrongPass: 'flex',
                passwordItemError: true,
                passwordItemSuccess: false
            });
        } else {
            this.setState({
                passwordItemError: false,
                passwordItemSuccess: true
            });
        }
        if (error == false) {

            this.setState({progressVisible: true});
            this.SignUpCustomer()
        }
    };
    async ResendSms(){
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("user_id",  this.state.user_id);

            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + '/user/resend_sms',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)

                if (json.success === true) {

                    this.setState({message:''});
                } else {
                    this.setState({progressVisible: false,message:json.message});
                }

        } catch (error) {
            console.log(error)         
        }
    }
    async SignUpCustomer(){
        this.setState({progressVisible: true});
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("firstname",  this.state.firstName);
            formData.append("lastname", this.state.lastName);
            //formData.append("phone_number", this.state.phoneNumber);
            formData.append("nationnal_code", this.state.nationalCode);
            formData.append("username", this.state.phoneNumber);
            //formData.append("password", this.state.password);
            formData.append("role", 'customer');
            console.log(formData)
            let response = await fetch(this.props.global.baseApiUrl + '/user/adduser',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)

                if (json.success === true) {

                    // this.addInRedux(json)
                    this.startTimer();
                    
                    this.setState({
                        user_id:json.data.id,
                        progressVisible: false,
                        isModalCode:true, 
                        isModalVisibleValidationDriver:false,
                        firstName:'',
                        lastName:'',
                        phoneNumber:'',
                        nationalCode:'',
                        });
                } else {
                    this.setState({progressVisible: false, message:json.messages});
                }
                

        } catch (error) {
            console.log(error)         
        }
    }
    async SignUpDriver(){
        this.setState({progressVisible: true});
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("firstname",  this.state.firstNameDriver);
            formData.append("lastname", this.state.lastNameDriver);
            formData.append("username", this.state.phoneNumberDriver);
            formData.append("nationnal_code", this.state.nationalCodeDriver);
            // formData.append("username", this.state.usernameDriver);
            //formData.append("password", this.state.passwordDriver);
            formData.append("car_type_id",  this.state.carType_selectedById_id);
            formData.append("plak_no",  this.state.plateCarDriver);
            formData.append("role", 'driver');
            console.log(formData)
            let response = await fetch(this.props.global.baseApiUrl + '/user/adduser',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)

                if (json.success === true) {

                    // this.addInRedux(json)
                    this.startTimer();
                    this.setState({
                        user_id:json.data.id,
                        progressVisible: false,
                        isModalCode:true,
                        isModalVisibleValidationDriver:false,
                        firstNameDriver:'',
                        lastNameDriver:'',
                        nationalCodeDriver:'',
                        phoneNumberDriver:'',
                        // usernameDriver:'',
                        // passwordDriver:'',
                        plateCarDriver:'',
                        carType_selectedById_id:'',
                        carType_selectedById:''});
                } else {
                    this.setState({progressVisible: false, message:json.messages});
                }
                

        } catch (error) {
            console.log(error)         
        }
    }
    async GetUser(userToken){
        console.log(userToken)
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", userToken);

            let response = await fetch(this.props.global.baseApiUrl + '/user/get',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(response)

                if(json.success){
                    this.addInRedux(json).then(()=>{
                        if(this.props.user.role === "customer"){
                            Actions.replace('home');
                            this.setState({LoadingLogin:false})
                        }
                        else{
                            Actions.replace('mainPageDriver');
                            this.setState({LoadingLogin:false})
                        }
                    })
                    
                }
                else{
                    this.setState({LoadingLogin:false})
                }
        } catch (error) {
            console.log(error)         
        }
    }
    async GetData() {
        try {
            const token = await AsyncStorage.getItem('UserToken');
            console.log(token)
            if (token === null ) {
                Actions.replace('login');
                this.setState({LoadingLogin:false})
            } else{
                this.GetUser(token)
            }

        } catch (error) {
            console.log(error)
        }

    }
    async setMessageServerSignUpUser(result) {
        await this.setState({
            flex_wrongServer: 'flex',
            txtWrongServer: result.messages
        });
    };
    async setMessageServerValidationDriver(result) {
        await this.setState({
            flex_wrongServerDriver: 'flex',
            txtWrongServerDriver: result.messages
        });
    };
    renderCarType({item}){
        return(
            <TouchableOpacity style={{alignItems:'center',justifyContent:'center',paddingVertical:10,borderBottomColor:'#aaaa',borderBottomWidth:0.5}}
                onPress={()=>this.setState({
                    isModalCarType:false,
                    isModalCarTypeById:true,
                    carType_selected:item.name,
                    carType_selectedId:item.id,
                    LoadingCarTypeById:true
                    },()=>this.getCarTypeById())}>
                <Text style={{fontFamily:'IRANSansMobile'}}>{item.name}</Text>
            </TouchableOpacity>
            )
    }
    renderCarTypeById({item}){
        return(
            <TouchableOpacity style={{alignItems:'center',justifyContent:'center',paddingVertical:10,borderBottomColor:'#aaaa',borderBottomWidth:0.5}}
                onPress={()=>this.setState({
                    isModalCarTypeById:false,
                    carType_selectedById:item.name,
                    carType_selectedById_id:item.id
                    })}>
                <Text style={{fontFamily:'IRANSansMobile'}}>{item.name}</Text>
            </TouchableOpacity>
            )
    }
    async VerifyCode(code){

        this.setState({progressVisible: true});
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("user_id", this.state.user_id);
            formData.append("code", code);
            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + '/user/verify_user_with_sms',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)

                if (json.success === true) {
                    this.addInRedux(json).then(()=>{
                            this.setState({progressVisible: false, isModalVisibleLogin:false , username:'', password:'',curTime:'' });
                            if(this.props.user.role === "customer"){
                                Actions.replace('home');
                                this.setState({LoadingLogin:false})
                            }else{
                                Actions.replace('mainPageDriver');
                                this.setState({LoadingLogin:false})
                            }
                        })

                    this.setState({progressVisible: false, isModalCode:false,message:'',curTime: "" });
                } else {
                    this.setState({progressVisible: false , message:json.message,curTime: "" });
                }

        } catch (error) {
            console.log(error)         
        }
    }
    ValidationCustomer(){
        if(this.state.firstName == ''){
            this.setState({message:'لطفا نام خود وارد کنید'})
        }
        else if(this.state.lastName == ''){
            this.setState({message:'لطفا نام خانوادگی  خود را وارد کنید'})
        }
        else if(this.state.nationalCode == ''){
            this.setState({message:'لطفا کدملی را وارد کنید'})

        }
        else if(this.state.phoneNumber == ''){
            this.setState({message:'لطفا شماره همراه را وارد کنید'})
        }
        // else if(this.state.username == ''){
        //     this.setState({message:'لطفا نام کاربری را وارد کنید'})

        // }
        // else if(this.state.password == ''){
        //     this.setState({message:'لطفا رمز عبور را وارد کنید'})
        // }
        else{
            this.setState({message:''})
            this.SignUpCustomer()
        }
    }
    ValidationDriver(){
        if(this.state.firstNameDriver === ''){
            this.setState({message:'لطفا نام خود وارد کنید'})
        }
        else if(this.state.lastNameDriver === ''){
            this.setState({message:'لطفا نام خانوادگی  خود را وارد کنید'})
        }
        else if(this.state.phoneNumberDriver === ''){
            this.setState({message:'لطفا شماره همراه را وارد کنید'})
        }
        else if(this.state.nationalCodeDriver === ''){
            this.setState({message:'لطفا کدملی را وارد کنید'})

        }
        // else if(this.state.usernameDriver === ''){
        //     this.setState({message:'لطفا نام کاربری را وارد کنید'})

        // }
        // else if(this.state.passwordDriver === ''){
        //     this.setState({message:'لطفا رمز عبور را وارد کنید'})

        // }
        else if(this.state.plateCarDriver.length < 8 ){
            this.setState({message:'لطفا پلاک خودرو را انتخاب کنید'})
        }
        else if(!this.state.carType_selectedById_id){
            this.setState({message:'لطفا نوع خودرو انتخاب کنید'})

        }
        else{
            this.setState({ message:''})
            this.SignUpDriver()
        }
    }
    ValidationLogin(){
        if(this.state.username === ''){
            this.setState({message:'لطفا شماره همراه خود را وارد کنید'})

        }
        // else if(!this.state.password){
        //     this.setState({message:'لطفا رمز عبور خود را وارد کنید'})

        // }
        else{
            this.setState({ message:''})
            this.Login()
        }
    }
    async UpdatePass(){
        this.setState({progressVisible: true});

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("password", this.state.NewPass);
            formData.append("username", this.props.user.username);
            formData.append("nationnal_code", this.props.user.nationnal_code);
            formData.append("phone_number", this.props.user.phone_number);
            console.log(formData)
            let response = await fetch(this.props.global.baseApiUrl + '/user/update',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)

                if(json.success){
                    this.setState({progressVisible: false,isModalNewPass:false,progressVisible: true});

                    if(this.props.user.role === "customer"){
                        await Actions.replace('home');
                        await this.setState({LoadingLogin:false})
                    }else{
                        await Actions.replace('mainPageDriver');
                        this.setState({LoadingLogin:false})
                    }
                }
                else{
                    this.setState({progressVisible: false});
                    this.setState({LoadingLogin:false})
                }
        } catch (error) {
            console.log(error)         
        }
    }

    render() {
        return (
            <Container style={style.view_home_container}>
                <ProgressDialog
                    visible={this.state.progressVisible}
                    message="لطفا منتظر بمانید..."
                />
                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
                <Image source={require('../assets/images/background.png')} style={style.img_container_home}
                       resizeMode={'cover'}/>
                {/*<View style={style.view_top_home}>*/}
                <Image source={require('../assets/images/trans_blue1.png')} style={style.img_top_home}/>
                <Image source={require('../assets/images/phone_logo.png')} resizeMode={'cover'}
                       style={style.img_phone_logo}/>
                {/*</View>*/}
                <Content>
                    <View style={style.view_center_home}>
                        {/* <Text style={style.txt_top_center_login1}>اپلیکیشن حمل و نقل</Text> */}
                        <Text style={style.txt_top_center_login2}>اپلیکیشن تیکسان بار</Text>
                        <Text style={style.txt_top_center_login3}>هشدار:جهت جلوگیری از هرگونه سو استفاده </Text>
                        <Text style={style.txt_top_center_login3}>
1.هیچ گونه وجه قبل از بار گیری پرداخت نشود</Text>
<Text style={style.txt_top_center_login3}>
2.قبل از تحویل بار به راننده از صحت شماره پلاک و مشخصات راننده اطمینان حاصل نمایید شماره پلاک و کد ملی راننده را از کارت شناسایی راننده یاداشت نمایید.</Text>
                        <Icon name="arrow-down" type={'SimpleLineIcons'} color={"#c8c8c8"} size={width * .08} style={{marginBottom:-10}}/>
                        {(this.state.LoadingLogin) && <ActivityIndicator size="large" color="#2395ff" />}
                        {(!this.state.LoadingLogin) && <View>
                            <View style={style.view_img_steering_wheel}>
                                <View style={style.view_steering_wheel}>
                                    <TouchableOpacity activeOpacity={.5}
                                    onPress={()=>this.setState({
                                        isModalVisibleValidationDriver: true,
                                        display_Driver_signUp: 'flex',
                                        display_confirm_signUp: 'none',
                                        display_User_signUp: 'none'
                                    })}>
                                        <Image source={require('../assets/images/steering-wheel.png')} resizeMode={'cover'}
                                            style={style.img_steering_wheel}/>
                                    </TouchableOpacity>
                                    <Text style={style.txt_steering_wheel}>من راننده هستم!</Text>
                                </View>
                                <View style={style.view_steering_wheel}>
                                    <TouchableOpacity activeOpacity={.5}
                                    onPress={()=>this.setState({
                                        isModalVisibleValidationDriver:true ,
                                        display_Driver_signUp: 'none',
                                        display_confirm_signUp: 'none',
                                        display_User_signUp: 'flex'
                                        })}>
                                        <Image source={require('../assets/images/user-login.png')} resizeMode={'cover'}
                                            style={style.img_steering_wheel}/>
                                    </TouchableOpacity>
                                    <Text style={style.txt_steering_wheel}>من صاحب بار هستم!</Text>
                                </View>
                            </View>

                            <TouchableOpacity activeOpacity={.5} style={style.view_btn_login}
                                            onPress={this.toggleModalLogin}>
                                <Icon name="arrow-left" type={'SimpleLineIcons'} color={"#40ba13"} size={width * .05}/>
                                <Text style={style.txt_btn_login}>قبلا عضو شده ام میخواهم وارد شوم</Text>
                            </TouchableOpacity>
                        </View>}
                        {/* <Image source={require('../assets/images/Layer7.png')} resizeMode={'cover'}
                               style={style.img_bottom_home}/> */}
                    </View>
                </Content>
                {/* code forget*/}
                <Modal style={{margin:0,justifyContent:'flex-end'}} isVisible={this.state.isModalCodeForget} backdropOpacity={0.80} backdropColor='#203fff' backdropOpacity={0.8}
                    onBackdropPress={() => this.setState({ isModalCodeForget: false })} onBackButtonPress={() => this.setState({ isModalCodeForget: false})}>
                    <View style={[style.view_containerModalLogin,{height:"50%"}]}>
                        <TouchableOpacity title="Hide modal" onPress={this.toggleModalLogin}
                                          style={style.exitModalLogin}>
                            <Image source={require('../assets/images/loginicon.png')} style={style.img_exitModalLogin}
                                   resizeMode={'contain'}/>
                        </TouchableOpacity>
                        <Text style={[style.btnLoginAccount,{marginBottom:0}]}>کد تایید هویت</Text>
                        <Content style={{width: width * .90,flex:1}}>

                            <CodeInput
                                ref="codeInputRef1"
                                codeLength={6}
                                space={6}
                                size={50}
                                keyboardType="numeric"
                                inputPosition='center'
                                style={{color:'black',borderBottomColor:'#2395ff',fontSize:26,borderBottomWidth:1,marginHorizontal:10,textAlign:'center',paddingBottom:5}}
                                onFulfill={(code) => this.VerifyResetPass(code)}
                            />
                            {(this.state.message !='' )&&
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:16,color:'red',textAlign:'center',marginTop:10,marginBottom:-5}}>{this.state.message}</Text>
                                    }
                            <View style={{flexDirection:'row-reverse',justifyContent:'center',alignItems:'center',marginTop:15}}>
                                {/* {this.showRecode()}
                                {this.showTimer()} */}
                            </View>
                        </Content>
                        
                    </View>
                </Modal>
                {/* add number */}
                <Modal style={{margin:0,justifyContent:'flex-end'}}
                    isVisible={this.state.isModalAddNumber} backdropOpacity={0.80} backdropColor='#203fff' backdropOpacity={0.8}
                    onBackdropPress={() => this.setState({ isModalAddNumber: false })} onBackButtonPress={() => this.setState({ isModalAddNumber: false})}>
                    <View style={style.view_containerModalLogin}>
                        <TouchableOpacity title="Hide modal" onPress={this.toggleModalLogin}
                                          style={style.exitModalLogin}>
                            <Image source={require('../assets/images/loginicon.png')} style={style.img_exitModalLogin}
                                   resizeMode={'contain'}/>
                        </TouchableOpacity>
                        <Text style={style.btnLoginAccount}>شماره همراه خود را وارد کنید</Text>
                        <Content style={{width: width * .90,flex:1}}>
                            <Item regular>
                                <Input
                                    maxLength={11}
                                    keyboardType="numeric"
                                    autoFocus={true}
                                    style={style.txt_codeVerify}
                                    placeholder='شماره همراه'
                                    placeholderTextColor={'#999999'}
                                    onChangeText={(text) => this.setState({phoneNumber: text})}
                                />
                            </Item>
                            <TouchableOpacity activeOpacity={.5} style={style.btnLogin} onPress={()=>this.ResetPass()}>
                                <Text style={style.txt_btn_login}>تایید</Text>
                            </TouchableOpacity>
                        </Content>

                    </View>
                </Modal>
                {/* new pass */}
                <Modal style={{margin:0,justifyContent:'flex-end'}}
                    isVisible={this.state.isModalNewPass} backdropOpacity={0.80} backdropColor='#203fff' backdropOpacity={0.8}
                    onBackdropPress={() => this.setState({ isModalNewPass: false })} onBackButtonPress={() => this.setState({ isModalNewPass: false})}>
                    <View style={style.view_containerModalLogin}>
                        <TouchableOpacity title="Hide modal" onPress={this.toggleModalLogin}
                                          style={style.exitModalLogin}>
                            <Image source={require('../assets/images/loginicon.png')} style={style.img_exitModalLogin}
                                   resizeMode={'contain'}/>
                        </TouchableOpacity>
                        <Text style={style.btnLoginAccount}>رمز عبور جدید</Text>
                        <Content style={{width: width * .90,flex:1}}>
                            <Item regular>
                                <Input
                                    secureTextEntry={true}
                                    style={style.txt_codeVerify}
                                    placeholder='رمز عبور جدید را وارد کنید'
                                    placeholderTextColor={'#999999'}
                                    onChangeText={(text) => this.setState({NewPass: text})}
                                />
                            </Item>
                            <Item regular style={{marginTop: height * .04}} >
                                <Input
                                    style={style.txt_codeVerify}
                                    placeholder='تکرار رمز عبور جدید'
                                    placeholderTextColor={'#999999'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => this.setState({RepeatNewPass: text})}
                                />
                            </Item>
                            {(this.state.message !='' )&&
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:16,color:'red',textAlign:'center',marginTop:5,marginBottom:-15}}>{this.state.message}</Text>
                                    }
                            <TouchableOpacity activeOpacity={.5} style={style.btnLogin} onPress={()=>this.UpdatePass()}>
                                <Text style={style.txt_btn_login}>تایید</Text>
                            </TouchableOpacity>
                        </Content>

                    </View>
                </Modal>
                {/* login */}
                <Modal style={{margin:0,justifyContent:'flex-end'}}
                    isVisible={this.state.isModalVisibleLogin} backdropOpacity={0.80} backdropColor='#203fff' backdropOpacity={0.8}
                    onBackdropPress={() => this.setState({ isModalVisibleLogin: false,message:'' })} onBackButtonPress={() => this.setState({ isModalVisibleLogin: false,message:''})}>
                    <View style={style.view_containerModalLogin}>
                        <TouchableOpacity title="Hide modal" onPress={this.toggleModalLogin}
                                          style={style.exitModalLogin}>
                            <Image source={require('../assets/images/loginicon.png')} style={style.img_exitModalLogin}
                                   resizeMode={'contain'}/>
                        </TouchableOpacity>
                        <Text style={style.btnLoginAccount}>ورود به حساب کاربری</Text>
                        <Content style={{width: width * .90,flex:1}}>
                            <Item regular>
                                <Input
                                    keyboardType="numeric"
                                    maxLength={11}
                                    style={[style.txt_codeVerify,{padding:0}]}
                                    placeholder='شماره همراه خود را وارد کنید'
                                    placeholderTextColor={'#999999'}
                                    onChangeText={(text) => this.setState({username: text})}
                                />
                            </Item>
                            {/* <Item regular style={{marginTop: height * .04}} >
                                <Input
                                    style={style.txt_codeVerify}
                                    placeholder='رمز عبور خود را وارد کنید'
                                    placeholderTextColor={'#999999'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => this.setState({password: text})}
                                />
                            </Item> */}
                            {(this.state.message !='' )&&
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:16,color:'red',textAlign:'center',marginTop:5,marginBottom:-15}}>{this.state.message}</Text>
                                    }
                            <TouchableOpacity activeOpacity={.5} style={style.btnLogin} onPress={()=>this.ValidationLogin()}>
                                <Text style={[style.txt_btn_login,{color:'white'}]}>ورود به حساب کاربری</Text>
                            </TouchableOpacity>
                                {/* <TouchableOpacity activeOpacity={.5} onPress={()=>this.setState({isModalAddNumber:true , isModalVisibleLogin:false})}>
                                    <Text style={[style.sigUpInLogin,{textAlign:'right',marginRight:20}]}>فراموشی رمز عبور</Text>
                                </TouchableOpacity> */}
                        </Content>

                    </View>
                </Modal>
                {/* code */}
                <Modal style={{margin:0,justifyContent:'flex-end'}} isVisible={this.state.isModalCode} backdropOpacity={0.80} backdropColor='#203fff' backdropOpacity={0.8}>
                    <View style={[style.view_containerModalLogin,{height:"50%"}]}>
                        <TouchableOpacity title="Hide modal" onPress={this.toggleModalLogin}
                                          style={style.exitModalLogin}>
                            <Image source={require('../assets/images/loginicon.png')} style={style.img_exitModalLogin}
                                   resizeMode={'contain'}/>
                        </TouchableOpacity>
                        <Text style={[style.btnLoginAccount,{marginBottom:0}]}>کد تایید هویت</Text>
                        <Content style={{width: width * .90,flex:1}}>

                            <CodeInput
                                ref="codeInputRef1"
                                codeLength={6}
                                space={6}
                                size={50}
                                keyboardType="numeric"
                                inputPosition='center'
                                style={{color:'black',borderBottomColor:'#2395ff',fontSize:26,borderBottomWidth:1,marginHorizontal:10,textAlign:'center',paddingBottom:5}}
                                onFulfill={(code) => this.VerifyCode(code)}
                            />
                            {(this.state.message !='' )&&
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:16,color:'red',textAlign:'center',marginTop:10,marginBottom:-5}}>{this.state.message}</Text>
                                    }
                            <View style={{flexDirection:'row-reverse',justifyContent:'center',alignItems:'center',marginTop:15}}>
                                {this.showRecode()}
                                {this.showTimer()}
                            </View>
                        </Content>
                        
                    </View>
                </Modal>
                {/* sign up */}
                <Modal style={{margin:0,marginTop:80}} isVisible={this.state.isModalVisibleValidationDriver} backdropOpacity={0.80} backdropColor='#203fff' backdropOpacity={0.8}
                    onBackdropPress={() => this.setState({ isModalVisibleValidationDriver: false,message:'' })} onBackButtonPress={() => this.setState({ isModalVisibleValidationDriver: false,message:''})}>
                    <Container style={{borderTopRightRadius:50,paddingTop:10,borderTopLeftRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <TouchableOpacity title="Hide modal" onPress={() => this.setState({isModalVisibleValidationDriver: false})} style={style.exitModalLogin}>
                            <Image source={require('../assets/images/usericon.png')} style={style.img_exitModalLogin} resizeMode={'contain'}/>
                        </TouchableOpacity>
                        <Text style={[style.btnLoginAccount, {display: this.state.display_User_signUp}]}>عضویت صاحب
                            بار!</Text>
                        <Text style={[style.btnLoginAccount, {display: this.state.display_Driver_signUp}]}>عضویت
                            راننده!</Text>
                        <Content style={{flex:1}}>
                            <View style={{display: this.state.display_Driver_signUp}}>
                                <View style={[style.view_Confirm_SignUp,]}>
                                    <Form style={{width: width * .80,}}>
                                        <Item regular style={style.ItemInputSignUp}
                                              error={this.state.firstNameItemErrorDriver}
                                              success={this.state.firstNameItemSuccessDriver}>
                                            <Input
                                                style={style.InputSignUp}
                                                placeholder='نام'
                                                placeholderTextColor={'#999999'}
                                                value={this.state.firstNameDriver}
                                                onChangeText={(text) => this.setState({firstNameDriver: text})}
                                            />
                                        </Item>
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongFirstNameDriver}]}>{'نام را وارد نمایید'}</Text>
                                        <Item regular style={style.ItemInputSignUp}
                                              error={this.state.lastNameItemErrorDriver}
                                              success={this.state.lastNameItemSuccessDriver}>
                                            <Input
                                                style={style.InputSignUp}
                                                placeholder='نام خانوادگی'
                                                placeholderTextColor={'#999999'}
                                                value={this.state.lastNameDriver}
                                                onChangeText={(text) => this.setState({lastNameDriver: text})}
                                            />
                                        </Item>
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongLastNameDriver}]}>{'نام خانوادگی را وارد نمایید'}</Text>
                                        <Item regular style={style.ItemInputSignUp}
                                              error={this.state.nationalCodeItemErrorDriver}
                                              success={this.state.nationalCodeItemSuccessDriver}>
                                            <Input
                                                maxLength={10}
                                                keyboardType={'numeric'}
                                                style={style.InputSignUp}
                                                placeholder='کد ملی'
                                                placeholderTextColor={'#999999'}
                                                value={this.state.nationalCodeDriver}
                                                onChangeText={(text) => this.setState({nationalCodeDriver: text})}
                                            />
                                        </Item>
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongNationalDriver}]}>{'کد ملی باید 10 عدد باشد'}</Text>
                                        <Item regular style={style.ItemInputSignUp}
                                              error={this.state.phoneNumberItemErrorDriver}
                                              success={this.state.phoneNumberItemSuccessDriver}>
                                            <Input
                                                maxLength={11}
                                                keyboardType={'numeric'}
                                                style={style.InputSignUp}
                                                placeholder='شماره همراه'
                                                placeholderTextColor={'#999999'}
                                                value={this.state.phoneNumberDriver}
                                                onChangeText={(text) => this.setState({phoneNumberDriver: text})}
                                            />
                                        </Item>
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongPhoneDriver}]}>{'شماره موبایل باید 11 عدد باشد'}</Text>
                                        {/* <Item regular style={style.ItemInputSignUp}
                                              error={this.state.usernameItemErrorDriver}
                                              success={this.state.usernameItemSuccessDriver}>
                                            <Input
                                                style={style.InputSignUp}
                                                placeholder='نام کاربری'
                                                placeholderTextColor={'#999999'}
                                                value={this.state.usernameDriver}
                                                onChangeText={(text) => this.setState({usernameDriver: text})}
                                            />
                                        </Item>
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongUser_NameDriver}]}>{'نام کاربری را وارد نمایید'}</Text>
                                        <Item regular style={style.ItemInputSignUp}
                                              error={this.state.passwordItemErrorDriver}
                                              success={this.state.passwordItemSuccessDriver}>
                                            <Input
                                                secureTextEntry={true}
                                                style={style.InputSignUp}
                                                placeholder='رمز عبور'
                                                placeholderTextColor={'#999999'}
                                                value={this.state.passwordDriver}
                                                onChangeText={(text) => this.setState({passwordDriver: text})}
                                            />
                                        </Item>
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongPassDriver}]}>{'رمز عبور را وارد نمایید'}</Text> */}
                                        <TouchableOpacity style={{justifyContent:'center',borderColor:'#e1e1e1',borderWidth:1,marginTop:10,height:60}} onPress={()=>this.setState({isModalSetPlate:true},()=>this.getCarType())}>
                                            {this.state.plateCarDriver=='' &&
                                            <View style={{position:"absolute",right:5,fontFamily:'IRANSansMobile'}}>
                                                <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.7}}>پلاک ماشین </Text>
                                            </View>
                                            }
                                            
                                            {this.state.plateCarDriver !='' &&
                                                <View style={{backgroundColor:'white',borderRadius:20,paddingTop:0}}>
                                                    <ImageBackground source={require('../assets/images/plate.jpg')} style={{width:180,height:45,marginHorizontal:10,alignSelf:'center',justifyContent:'center'}} resizeMode='contain'>
                                                        <View style={{width:160,flexDirection:'row-reverse',flex:1,marginHorizontal:10,justifyContent:'space-around',alignItems:'center'}}>
                                                            <View style={{flex:.2,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                                                                <Text style={{textAlign:'center',padding:0,textAlignVertical:'center',justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:18,color:'black'}}>{Helper.ToPersianNumber(this.state.plateCarDriver1)}</Text>                                   
                                                            </View>
                                                            <View style={{flex:1,flexDirection:'row-reverse',marginHorizontal:15}}>
                                                                <Text style={{textAlign:'center',padding:0,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:18,color:'black'}}>{Helper.ToPersianNumber(this.state.plateCarDriver2)}</Text>
                                                                <Text style={{textAlign:'center',marginBottom:0,padding:0,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:18,color:'black'}}>{this.state.plateCarDriver3}</Text>
                                                                <Text style={{textAlign:'center',padding:0,marginRight:-10,justifyContent:'center',flex:.7,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:18,color:'black'}}>{Helper.ToPersianNumber(this.state.plateCarDriver4)}</Text>
                                                            </View>
                                                        </View>
                                                    </ImageBackground>
                                                </View>
                                            }
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{justifyContent:'center',borderColor:'#e1e1e1',borderWidth:1,marginTop:10,height:60,marginBottom:10}} onPress={()=>this.setState({isModalCarType:true,LoadingCarType:true},()=>this.getCarType())}>
                                            <View style={{position:"absolute",right:5,fontFamily:'IRANSansMobile'}}>
                                                {this.state.carType_selectedId == '' && <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.7}}>نوع خودرو</Text>}
                                                {this.state.carType_selectedId != '' && <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:1,color:'black'}}>{this.state.carType_selected} {","} {this.state.carType_selectedById}</Text>}

                                            </View>
                                        </TouchableOpacity>
                                    </Form>
                                    {(this.state.message !='' )&&
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:16,color:'red'}}>{this.state.message}</Text>
                                    }
                                    <TouchableOpacity activeOpacity={.5} style={style.btnSignUpModal}
                                                      onPress={()=>this.ValidationDriver()}>
                                        <Text style={[style.txt_btn_login,{color:'white'}]}>ثبت اطلاعات</Text>
                                    </TouchableOpacity>


                                </View>
                                <TouchableOpacity activeOpacity={.5}
                                                  style={[style.cancel_request, {display: this.state.display_Driver_signUp}]}
                                                  onPress={() => this.setState({isModalVisibleValidationDriver: false,message:''})}>
                                    <Text style={style.cancelRequest}>لغو</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{display: this.state.display_User_signUp}}>
                                <View style={[style.view_Confirm_SignUp,]}>

                                    {/*<View style={{flex: 1,}}>*/}
                                    {/*<Text style={style.txt_topSignUp}>اطلاعات هویتی مربوط را با دقت یادداشت کنید تا در هنگام*/}
                                    {/*    تحویل و دریافت بار ، مشکل ایجاد نشود.</Text>*/}
                                    {/*</View>*/}
                                    <Form style={{width: width * .80,}}>

                                        <Item regular style={style.ItemInputSignUp}
                                              error={this.state.firstNameItemError}
                                              success={this.state.firstNameItemSuccess}>
                                            <Input
                                                style={style.InputSignUp}
                                                placeholder='نام'
                                                placeholderTextColor={'#999999'}
                                                value={this.state.firstName}
                                                onChangeText={(text) => this.setState({firstName: text})}
                                            />
                                        </Item>
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongFirstName}]}>{'نام را وارد نمایید'}</Text>
                                        <Item regular style={style.ItemInputSignUp} error={this.state.lastNameItemError}
                                              success={this.state.lastNameItemSuccess}>
                                            <Input
                                                style={style.InputSignUp}
                                                placeholder='نام خانوادگی'
                                                placeholderTextColor={'#999999'}
                                                value={this.state.lastName}
                                                onChangeText={(text) => this.setState({lastName: text})}
                                            />
                                        </Item>
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongLastName}]}>{'نام خانوادگی را وارد نمایید'}</Text>
                                        <Item regular style={style.ItemInputSignUp}
                                              error={this.state.nationalCodeItemError}
                                              success={this.state.nationalCodeItemSuccess}>
                                            <Input
                                                maxLength={10}
                                                keyboardType={'numeric'}
                                                style={style.InputSignUp}
                                                placeholder='کد ملی'
                                                placeholderTextColor={'#999999'}
                                                value={this.state.nationalCode}
                                                onChangeText={(text) => this.setState({nationalCode: text})}
                                            />
                                        </Item>
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongNational}]}>{'کد ملی باید 10 عدد باشد'}</Text>
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongEmail}]}>{'ایمیل را وارد کنید'}</Text>
                                        <Item regular style={style.ItemInputSignUp}
                                              error={this.state.phoneNumberItemError}
                                              success={this.state.phoneNumberItemSuccess}>
                                            <Input
                                                maxLength={11}
                                                keyboardType={'numeric'}
                                                style={style.InputSignUp}
                                                placeholder='موبایل'
                                                placeholderTextColor={'#999999'}
                                                value={this.state.phoneNumber}
                                                onChangeText={(text) => this.setState({phoneNumber: text})}
                                            />
                                        </Item>
                                        {/* <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongPhone}]}>{'شماره موبایل باید 11 عدد باشد'}</Text>
                                        <Item regular style={style.ItemInputSignUp} error={this.state.usernameItemError}
                                              success={this.state.usernameItemSuccess}>
                                            <Input
                                                style={style.InputSignUp}
                                                placeholder='نام کاربری'
                                                placeholderTextColor={'#999999'}
                                                value={this.state.username}
                                                onChangeText={(text) => this.setState({username: text})}
                                            />
                                        </Item>
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongUser_Name}]}>{'نام کاربری را وارد نمایید'}</Text>
                                        <Item regular style={style.ItemInputSignUp} error={this.state.passwordItemError}
                                              success={this.state.passwordItemSuccess}>
                                            <Input
                                                secureTextEntry={true}
                                                style={style.InputSignUp}
                                                placeholder='رمز عبور'
                                                placeholderTextColor={'#999999'}
                                                value={this.state.password}
                                                onChangeText={(text) => this.setState({password: text})}
                                            />
                                        </Item>
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongPass}]}>{'رمز عبور را وارد نمایید'}</Text> */}
                                        <Text
                                            style={[style.wrongPassword, {display: this.state.flex_wrongServer}]}>{this.state.txtWrongServer}</Text>

                                    </Form>
                                    {(this.state.message !='' )&&
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:16,color:'red'}}>{this.state.message}</Text>
                                    }
                                    <TouchableOpacity activeOpacity={.5} style={[style.btnSignUpModal,{marginTop:10}]}
                                                      onPress={()=>this.ValidationCustomer()}>
                                        <Text style={[style.txt_btn_login,{color:'white'}]}>ثبت اطلاعات</Text>
                                    </TouchableOpacity>


                                </View>
                                <TouchableOpacity activeOpacity={.5}
                                                  style={[style.cancel_request, {display: this.state.display_User_signUp}]}
                                                  onPress={() => this.setState({isModalVisibleValidationDriver: false,message:''})}>
                                    <Text style={style.cancelRequest}>لغو</Text>
                                </TouchableOpacity>
                            </View>
                        </Content>

                    </Container>
                </Modal>
                {/*car type*/}
                <Modal style={{borderRadius:50,marginVertical:height*.15,alignItems:'center',justifyContent:'center',alignSelf:'center'}} isVisible={this.state.isModalCarType}
                    onBackdropPress={() => this.setState({ isModalCarType: false })} onBackButtonPress={() => this.setState({ isModalCarType: false})}>
                    <Container style={{paddingTop:10,borderRadius:50,alignItems:'center',justifyContent:'center',backgroundColor:'transparent',width:"100%"}}>
                        <View style={{backgroundColor:'white',borderRadius:20,paddingTop:10}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10,alignSelf:'center'}}>خودرو خود را انتخاب کنید</Text>
                        {/* <View style={{height:50,width:width*.65,marginTop:5,alignSelf:'center'}}>
                            <Input
                                style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                                placeholder="جستجو ..."                        
                                onChangeText={text => this.searchFilterFunction(text)}
                                autoCorrect={false}             
                            />  
                        </View> */}

                            {(this.state.LoadingCarType) &&
                            <View style={{width:width*.7,alignSelf:'center',justifyContent:'center',alignItems:'center'}}>
                                <ActivityIndicator color="#2395ff" size="large"/>
                            </View>
                            }
                            {(!this.state.LoadingCarType) && 
                                <FlatList
                                    data={this.state.carType}
                                    renderItem={this.renderCarType.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ width:width*.7,backgroundColor: 'transparent',paddingHorizontal:20 }}
                                />
                            }
                        </View>
                    </Container>
                </Modal>
                {/*car type By id*/}
                <Modal style={{borderRadius:50,marginVertical:height*.15,alignItems:'center',justifyContent:'center',alignSelf:'center'}} isVisible={this.state.isModalCarTypeById}
                    onBackdropPress={() => this.setState({ isModalCarTypeById: false })} onBackButtonPress={() => this.setState({ isModalCarTypeById: false})}>
                    <Container style={{paddingTop:10,borderRadius:50,alignItems:'center',justifyContent:'center',backgroundColor:'transparent',width:"100%"}}>
                        <View style={{backgroundColor:'white',borderRadius:20,paddingTop:10}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10,alignSelf:'center'}}>نوع خودرو خود را انتخاب کنید</Text>
                        {/* <View style={{height:50,width:width*.65,marginTop:5,alignSelf:'center'}}>
                            <Input
                                style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                                placeholder="جستجو ..."                        
                                onChangeText={text => this.searchFilterFunction(text)}
                                autoCorrect={false}             
                            />  
                        </View> */}

                            {(this.state.LoadingCarTypeById) &&
                            <View style={{width:width*.7,alignSelf:'center',justifyContent:'center',alignItems:'center'}}>
                                <ActivityIndicator color="#2395ff" size="large"/>
                            </View>
                            }
                            {(!this.state.LoadingCarTypeById) && 
                                <FlatList
                                    data={this.state.carTypeById}
                                    renderItem={this.renderCarTypeById.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ width:width*.7,backgroundColor: 'transparent',paddingHorizontal:20 }}
                                />
                            }
                        </View>
                    </Container>
                </Modal>
                {/*car plate*/}
                <Modal style={{borderRadius:50,alignItems:'center',justifyContent:'center',alignSelf:'center'}} isVisible={this.state.isModalSetPlate}
                    onBackdropPress={() => this.setState({ isModalSetPlate: false })} onBackButtonPress={() => this.setState({ isModalSetPlate: false})}>
                    <Container style={{paddingTop:10,borderRadius:50,alignItems:'center',justifyContent:'center',backgroundColor:'transparent'}}>
                        <View style={{backgroundColor:'white',borderRadius:20,paddingTop:10}}>
                        <ImageBackground source={require('../assets/images/plate.jpg')} style={{width:250,height:100,marginHorizontal:10,alignSelf:'center',justifyContent:'center'}} resizeMode='contain'>
                            <View style={{flexDirection:'row-reverse',width:230,marginHorizontal:10,justifyContent:'space-around',alignItems:'center'}}>
                                <View style={{flex:.3,flexDirection:'row',justifyContent:'center'}}>
                                <Input
                                        onChangeText={(text)=>this.setState({plateCarDriver1:text})}
                                        value={this.state.plateCarDriver1}
                                        placeholder="۸۸"
                                        placeholderTextColor={"#aaaa"}
                                        keyboardType="numeric"
                                        maxLength={2}
                                        style={{textAlign:'center',padding:0,height:100,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:22,color:'black'}}/>                                    
                                </View>
                                <View style={{flex:1,flexDirection:'row-reverse',marginHorizontal:20}}>
                                    <Input
                                        onChangeText={(text)=>this.setState({plateCarDriver2:text})}
                                        value={this.state.plateCarDriver2}
                                        placeholder="۸۸۸"
                                        placeholderTextColor={"#aaaa"}
                                        keyboardType="numeric"
                                        maxLength={3}
                                        style={{textAlign:'center',padding:0,height:100,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:22,color:'black'}}/>
                                    {/* <Text  style={{textAlign:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:45,color:'black'}}>۹۶۸</Text> */}
                                    <Input
                                        onChangeText={(text)=>this.setState({plateCarDriver3:text})}
                                        value={this.state.plateCarDriver3}
                                        placeholder="پ"
                                        placeholderTextColor={"#aaaa"}
                                        maxLength={1}
                                        style={{textAlign:'center',marginBottom:5,padding:0,height:100,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:22,color:'black'}}/>
                                    <Input
                                        onChangeText={(text)=>this.setState({plateCarDriver4:text})}
                                        value={this.state.plateCarDriver4}
                                        placeholderTextColor={"#aaaa"}
                                        placeholder="۸۸"
                                        keyboardType="numeric"
                                        maxLength={2}
                                        style={{textAlign:'center',padding:0,height:100,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:22,color:'black'}}/>
                                </View>
                            </View>
                            </ImageBackground>
                            <TouchableOpacity onPress={()=>{this.setState({isModalSetPlate:false}),
                                this.setState({plateCarDriver:this.state.plateCarDriver4+this.state.plateCarDriver3+this.state.plateCarDriver2+this.state.plateCarDriver1})}} style={{backgroundColor:'#2395ff',borderRadius:10,padding:10,marginHorizontal:38,marginBottom:10}}>
                                <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',color:'white',fontSize:18}}>تایید</Text>
                            </TouchableOpacity>
                        </View>
                    </Container>
                </Modal>
                

                {/*<View style={[style.view_Confirm_SignUp, {display: this.state.display_confirm_signUp}]}>*/}
                {/*    <Text style={style.btnLoginAccount}>عضویت نهایی راننده!</Text>*/}
                {/*    <View style={{flex: 1,}}>*/}
                {/*        <Text style={style.txt_topSignUp}>جهت تایید عضویت، کد 5 رقمی که برای شماره همراه شما*/}
                {/*            پیامک شده است را در مستطیل زیر*/}
                {/*            وارد کنید و در ادامه بر دکمه آبی زیر بزنید.</Text>*/}
                {/*    </View>*/}
                {/*    <Item regular style={style.ItemInputSignUp}>*/}
                {/*        <Input*/}
                {/*            style={style.InputSignUp1}*/}
                {/*            placeholder='رمز را وارد کنید'*/}
                {/*            placeholderTextColor={'#999999'}*/}
                {/*        />*/}
                {/*    </Item>*/}
                {/*    <TouchableOpacity activeOpacity={.5} style={style.btnSignUpModal}*/}
                {/*                      onPress={this.toggleModalConfirmSignUpDriver}>*/}
                {/*        <Text style={style.txt_btn_login}>تایید رمز و پایان عضویت</Text>*/}
                {/*    </TouchableOpacity>*/}
                {/*</View>*/}
            </Container>
        )
    }
}
const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(login);

const style = StyleSheet.create({
    cancel_request: {alignItems: 'center', justifyContent: 'center'},
    wrongPassword: {color: '#ff1512', fontSize: width / 25, fontFamily: 'IRANSansMobile', marginTop: height * .01},
    txt_topSignUp: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#777777',
        borderRadius: width * .02,
        borderWidth: width * .004,
        paddingHorizontal: width * .05,
        paddingVertical: height * .04,
        marginHorizontal: width * .05,
        borderColor: '#e1e1e1',
        lineHeight: height * .04
    },
    view_Confirm_SignUp: {alignItems: 'center', justifyContent: 'center', width: width,},

    cancelRequest: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#2395ff',
        marginTop: height * .01,
        marginBottom: height * .01
    },
    btnSignUpModal: {
        width: width * .80,
        height: 50,
        backgroundColor: '#2395ff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    ItemInputSignUp: {marginTop: height * .01, width: width * .80},
    InputSignUp: {color: '#999999', fontSize: width / 25, textAlign: 'right',fontFamily:'IRANSansMobile',color:'black',padding:0},
    InputSignUp1: {color: '#999999', fontSize: width / 25, textAlign: 'center',fontFamily:'IRANSansMobile'},
    view_containerModalSignUp: {
        height: height*.9 ,
        width: width,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
         alignItems: 'center',
        // paddingHorizontal: width * .05
    },
    styleModalSignUpDriver: {
        flex:1,
        alignItems: 'center',
        marginBottom: 0,
        padding: 0,
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    txt_codeVerify: {color: 'black', fontSize: width / 22, fontFamily: 'IRANSansMobile', textAlign: 'center'},

    btnLoginAccount: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#222222',
        marginTop: height * .04,
        marginBottom: height * .04
    },
    img_exitModalLogin: {
        width: width * .18,
        height: width * .18,
        borderRadius: width * .09
    },
    exitModalLogin: {
        position: 'absolute',
        top: height * (-.04),
        width: width * .13,
        height: width * .13,
        borderRadius: width * .07,
        borderColor: '#3d56ee',
        borderWidth: width * .01,
        justifyContent: 'center',
        alignItems: 'center'
    },
    view_containerModalLogin: {
        height:"60%",
        width: width,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
        alignItems: 'center',
        paddingHorizontal: width * .05
    },
    StyleModalLogin: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
        padding: 0,
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    sigUpInLogin: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#2395ff',
        marginTop: height * .01
    },
    btnLogin: {
        marginTop: height * .03,
        width: width * .80,
        height: 50,
        backgroundColor: '#2395ff',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf:'center'
    },
    view_home_container: {justifyContent: 'center', alignItems: 'center', flex: 1},
    img_container_home: {
        position: 'absolute',
        zIndex: 0

    },
    img_top_home: {
        width: width,
        height: height * .40,
        position: 'absolute',
        zIndex: 1,
        top: -20
    },
    img_phone_logo: {
        width: width * .80,
        height: width * .70,
        position: 'absolute',
        right: width * .18,
        top: 0 ,
        zIndex: 1
    },
    view_top_home: {
        width: width,
        height: height * .40,
        // backgroundColor: '#493ff1',
        zIndex: 2,
        position: 'absolute',
        top: 0,
        alignItems: 'center'
    },

    view_center_home: {
        width: width,
        // height: height * .20,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: height *.35,
        // paddingBottom: height * .02,
        // zIndex: 2,
        // flex: 1,
        // position: 'absolute',
        // top: height * .43,
        // backgroundColor: '#ed5d81',
    },
    txt_top_center_login1: {fontSize: width / 35, fontFamily: 'IRANSansMobile', color: '#000',marginBottom:-10},
    txt_top_center_login2: {
        marginBottom:20,
        fontSize: width / 17, ...Platform.select({
            ios: {
                fontFamily: 'IRANSansMobile_Bold',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: 'IRANSansMobile_Bold',
            }
        }), color: '#000'
    },
    txt_top_center_login3: {
        fontSize: 15,
        fontFamily: 'IRANSansMobile',
        color: '#8a8a8a',
        lineHeight: height * .04,
        marginHorizontal: width * .08,
        textAlign:'center',
        opacity:.8
    },
    view_img_steering_wheel: {
        flexDirection: 'row',
        marginTop: height * .001,
        justifyContent: 'space-around',
        // backgroundColor: '#000',
        width: width * .80
    },
    img_down_arrow: {width: width * .10, height: width * .10},
    view_steering_wheel: {flex:1,justifyContent: 'center', alignItems: 'center'},
    txt_steering_wheel: {fontSize: width / 25, marginTop: height * .01,textAlign:'center', fontFamily: 'IRANSansMobile'},
    img_steering_wheel: {width: width * .30, height: width * .30,},
    view_btn_login: {
        marginTop: height * .01,
        // paddingHorizontal: width * .07,
        // paddingVertical: height * .02,
        height: height * .07,
        width: width * .80,
        justifyContent: 'space-around', alignItems: 'center', flexDirection: 'row'
    },
    txt_btn_login: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#40ba13'
    },
    img_bottom_home: {
        width: width * .15,
        height: width * .15,
        marginTop: height * .02
    }
});
