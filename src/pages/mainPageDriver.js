import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    StatusBar,
    Image,
    Dimensions,
    ScrollView,
    Text,
    TouchableOpacity,
    FlatList,
    ActivityIndicator,
    Animated,
    Slider,
    Linking
} from 'react-native';
import BottomNavigate from '../assets/components/bottomNavigate';
import {Header, Body, Button, Left, Right, Footer, Content, Container, Input,Icon} from "native-base";
import {Actions} from "react-native-router-flux";
import Modal from "react-native-modal";
import Icond from "react-native-vector-icons/FontAwesome5";
import {connect} from "react-redux";
import PushNotif from './PushNotificationController'
import Helper from '../assets/components/Helper'
import IconFooter from 'react-native-vector-icons/FontAwesome5';
import CodePin from 'react-native-pin-code'
import styles from '../assets/styles/home_css';
import { helpers } from 'rx';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class mainPageDriver extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchBar: '',
            message:'',
            messageRec:'',
            isModalSupport:false,
            noReq:null,
            itemShow:[],
            isModalSearchBar: false,
            isModalFinancial: false,
            isModalProduct:false,
            LoadingProduct:false,
            LoadingMain:true,
            display_Footer: 'flex',
            AllProduct:[],
            product_id_selected:'',
            origin_selected:'',
            origin_address_selected:'',
            destination_selected:'',
            destination_address_selected:'',
            price_selected:'',
            weight_selected:'',
            cargo_type_selected:'',
            isModalDriverAccept:false,
            LoadingAccept:false,
            isModalacceptProduct:false,
            typeProduct:null,
            contSize:null,
            origin:'',
            part1:'',
            part2:'',
            totalType:'',
            destination:'',
            isModalReiceve:false,
            product_id_selected_Reiceve:'',
            bijak_no:'',
            typeProductF:'',
            description:'',
            removeAnim: new Animated.Value(0.8),
            off:false,
            price_driver:0,
            isModaldriverOrders:false,
            LoadingDriverOrders:false,
            DriverOrders:[],
            LoadingRec:false,
            DoneAnim: new Animated.Value(0),
            Done:false,
            showadd:true,
            showmin:true,
            isModalFilter:false,
            weightFilter:0,
            packageType:[],
            LoadingFilter:false,
            carSelectedName:'',
            carSelectedName_second:'',
            isModalOrderDetail:false,
            distance_Selected:'',
            temp:false,
            isModalCar_type:false,
            isModalCar_typeById:false,
            carTypes:[],
            carTypesById:[],
            loading_carTypeById:false,
            loading_carType:false,
            carSelectedId:'',
            carSelectedName:'',
            carSelectedId_second:'',
            carSelectedName_second:'',
            isModalProvincelist:false,
            isModalProvincelistDst:false,
            LoadingProvince:false,
            dataUserProvince:[],
            Province_selected_name:'',
            Province_selected_nameDst:'',
            arrayholder:[]

        }
    }

    Grow(){
        Animated.timing(this.state.removeAnim,{
            toValue:1.3,
            duration:800,
            useNativeDriver:true
        }).start();
    }
    Less(){
        Animated.timing(this.state.removeAnim,{
            toValue:1,
            duration:800,
            useNativeDriver:true
        }).start();
    }
    GrowDone(){
        Animated.timing(this.state.DoneAnim,{
            toValue:1,
            duration:800,
            useNativeDriver:true
        }).start();
    }
    ModalSupport(tru) {
        this.setState({isModalSupport: tru});
    }
    componentDidMount(){
        this.setState({LoadingProduct:true,isModalProduct:true},()=>this.GetAll())
        setInterval(() => {
            if(!this.state.off){
                this.Grow(),
                this.setState({off:true})
            }
            else{
                this.Less()
                this.setState({off:false})
            }
        }, 800);
    }
    
    async DriverOrders(){
        try {
            let formData = new FormData();

            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);

            let response = await fetch(this.props.global.baseApiUrl + "/product/getdrivercargo",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                if(json.success){
                    console.log("asdasds"+json)
                    this.setState({LoadingDriverOrders:false, DriverOrders:json.data})
                }
                else{
                    this.setState({LoadingDriverOrders:false, message:'باری وجود ندارد'})
                }
                
            } catch (error) {
                console.log(error)
            }
    }

    renderDriverOrders({item}){
        return (
            <View style={[styles.viewContainerFieldApproved,{justifyContent:'space-between',paddingHorizontal:20}]}>
                <TouchableOpacity onPress={()=>this.setState({
                        product_id_selected:item.id,
                        isModalOrderDetail:true,
                        origin_selected:item.origin,
                        destination_selected:item.destination,
                        destination_address_selected:item.destination_address,
                        price_selected:item.price,
                        origin_address_selected:item.origin_address,
                        weight_selected:item.weight,
                        cargo_type_selected:item.cargo_type,
                        pakage_type_selected:item.package_type,
                        distance_Selected:item.distance,
                        description_selected:item.description,
                        carSelectedName:item.car_category_name,
                        carSelectedName_second:item.car_name})}>
                    <Text style={style.txt_showAll}>مشاهده جزئیات</Text>
                </TouchableOpacity>
                <View style={[styles.viewCenterFieldApproved,{flexDirection:'column',alignItems:'flex-end'}]}>
                    <Text style={{fontFamily:'IRANSansMobile',color:'black',fontSize:13,paddingBottom:5}}>
                    <Text style={{fontWeight:'bold',fontSize:14}}>نوع بار : </Text>{item.cargo_type}</Text>

                    <Text style={[styles.txt_view_top_header_menu2,{fontSize:14}]}>
                    <Text style={{fontWeight:'bold',fontSize:14}}>قیمت : </Text>{Helper.ToPersianNumber(item.price)} تومان</Text>
                </View>
            </View>
        );
        
    }

    renderBars = ({item}) => {
        // console.log(item)
                return (
                    <TouchableOpacity
                    onPress={()=>this.setState({
                        product_id_selected:item.id,
                        isModalDriverAccept:true,
                        origin_selected:item.origin,
                        destination_selected:item.destination,
                        destination_address_selected:item.destination_address,
                        price_selected:item.price,
                        origin_address_selected:item.origin_address,
                        weight_selected:item.weight,
                        cargo_type_selected:item.cargo_type,
                        pakage_type_selected:item.package_type,
                        description_selected:item.description,
                        carSelectedName:item.car_category_name,
                        carSelectedName_second:item.car_name,
                        distance_Selected:item.distance
                        })}
                    style={style.viewBar}>
                        <View style={{height:120 , width:120,  borderRadius:60,padding:5,marginVertical:5,borderColor:'#2395ff',borderWidth:1,alignItems:'center',justifyContent:'center'}}>
                         <Text style={{fontSize:13,color:'#aaa',fontFamily:'IRANSansMobile',fontSize:45,position:'absolute',opacity:.5}}>{Helper.ToPersianNumber(String(item.number))}</Text>
                         <Text style={[style.txt_view_top_header_menu2,{fontSize:10,textAlign:'center',marginTop:2}]}>{item.origin}</Text>
                            <Image source={require('../assets/images/map_small.png')}
                                style={[style.menu_top_placeholder,{width: width * .2}]}
                                resizeMode={'contain'}/>
                            <Text style={[style.txt_view_top_header_menu2,{fontSize:10,textAlign:'center',marginTop:2}]}>{item.destination}</Text>
                            <Text style={[style.txt_view_top_header_menu3,{marginTop:2,fontSize:12,color:'#2395ff',paddingHorizontal:5}]}>{Helper.ToPersianNumber(item.price)} تومان</Text>
                        </View>
                </TouchableOpacity>
                )
    };

    renderBarsReiceve = ({item}) => {
        if(item){
            return (

                <TouchableOpacity
                onPress={()=>this.setState({
                    product_id_selected_Reiceve:item.id,
                    isModalacceptProduct:true,
                    isModalReiceve:false
                    })}
                style={style.viewBar}>
                    <View style={{height:100 ,zIndex:99, width:100, position:'absolute', borderRadius:50, borderColor:'#2395ff',borderWidth:1}}/>
                    <Image source={require('../assets/images/Circle.png')} style={style.view_request2} resizeMode={'contain'}/>
                    <View style={[style.viewRequest_under,{position:'absolute',top:35,alignItems:'center'}]}>
                        <Text style={[style.txt_view_top_header_menu2,{fontSize:12}]}>{item.origin}</Text>
                        <Image source={require('../assets/images/map_small.png')}
                            style={[style.menu_top_placeholder,{width: width * .2}]}
                            resizeMode={'contain'}/>
                        <Text style={[style.txt_view_top_header_menu2,{fontSize:12}]}>{item.destination}</Text>
                        <Text style={[style.txt_view_top_header_menu3,{marginTop:5,fontSize:12}]}>{Helper.ToPersianNumber(item.price)} تومان</Text>
                    </View>
            </TouchableOpacity>
            )
        }
        else 
        {
            this.setState({message:'درخواستی وجود ندارد'})
        }

    }

    renderFinancial = ({item, index}) => {
        return (
            <View style={style.viewContainerFieldFinancial}>
                <View style={style.viewLeftFieldFinancial}>
                    <Text style={style.txt_showAllFinancial}>مشاهده جزئیات</Text>
                </View>
                <View style={style.viewCenterFieldFinancial}>
                    <Text style={style.txt_Financial_Price}>{item.price} تومان</Text>
                    <Image source={require('../assets/images/success.png')}
                           style={style.success_Financial_Img}
                           resizeMode={'contain'}/>
                </View>
                <View style={style.viewRightFieldFinancial}>
                    <Text style={style.txt_view_top_header_menu2}>{index}</Text>
                </View>
            </View>
        );
    };

    show_SearchBar = (tru) => {
        alert(tru)
        // this.setState({isModalSearchBar:tru,display_Footer:'none'})
    };
    Financial = () => {
        alert('sdfsf')
    }
    async GetAll(){
        try {
            this.setState({LoadingProduct:true})
            let formData = new FormData();

            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + "/product/all",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                console.log(json)
                if(json.success){
                    this.setState({LoadingProduct:false,LoadingMain:false,AllProduct:json.data})

                }else{
                    this.setState({LoadingProduct:false,LoadingMain:false, message:json.messages})
                }
                //console.log(this.state.AllProduct)
            
            } catch (error) {
                console.log(error)
            }
    }

    sample(num){
        if(this.state.LoadingMain){
            return(<ActivityIndicator color="#2395ff"/>)
        }
        else if(this.state.AllProduct[num]){
                return(
                    <View style={{height:80 , width:80,  borderRadius:40,padding:5,marginVertical:5,borderColor:'#2395ff',borderWidth:1,alignItems:'center',justifyContent:'center'}}>
                         <Text style={[style.txt_view_top_header_menu2,{fontSize:8,textAlign:'center',marginTop:2}]}>{this.state.AllProduct[num].origin}</Text>
                            <Image source={require('../assets/images/map_small.png')}
                                style={[style.menu_top_placeholder,{width: width * .2}]}
                                resizeMode={'contain'}/>
                            <Text style={[style.txt_view_top_header_menu2,{fontSize:8,textAlign:'center',marginTop:2}]}>{this.state.AllProduct[num].destination}</Text>
                            <Text style={[style.txt_view_top_header_menu3,{marginTop:2,fontSize:8,color:'#2395ff',paddingHorizontal:5}]}>{Helper.ToPersianNumber(this.state.AllProduct[num].price)} تومان</Text>
                        </View>
                )
        }else{
            return(<View style={[style.view_request,{borderColor:'#2395ff',borderWidth:1}]}/>)
        }
    }

    async DriverAccept(){     
              
        this.setState({LoadingAccept:true,message:''})

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("product_id", this.state.product_id_selected);
            formData.append("usertoken", this.props.user.apiToken);
            //formData.append("driver_price", this.state.price_selected);
            console.log(formData)
            let response = await fetch(this.props.global.baseApiUrl + "/product/driveraccept",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                console.log(json)
                //this.setState({temp:true})
                if(json.status){
                    this.setState({LoadingAccept:false , Done:true,isModalDriverAccept:false,message:''},()=>this.GrowDone())
                }else if(!json.status){
                    this.setState({LoadingAccept:false ,message:json.messages})

                }
            
            } catch (error) {
                console.log(error)
            }
    }
    async DriverCancel(){     
              
        this.setState({LoadingAccept:true,message:''})

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("product_id", this.state.product_id_selected);
            formData.append("usertoken", this.props.user.apiToken);
            console.log(formData)
            let response = await fetch(this.props.global.baseApiUrl + "/product/Driver_Cancel_product",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                console.log(json)
                this.setState({temp:true})
                if(json.status){
                    this.setState({LoadingAccept:false , Done:true,isModalDriverAccept:false,message:''},()=>this.GrowDone())
                }else if(!json.status){
                    if(json.message == 'there is not any reg_id to send in DB for this ID!!!!'){
                        this.setState({LoadingAccept:false ,message:'این بار قبلا توسط شما تایید نشده'})
                    }else{
                        this.setState({LoadingAccept:false ,message:json.messages})
                    }
                }
            
            } catch (error) {
                console.log(error)
            }
    }

    clearAcceptParameters(){
        this.setState({
            typeProductF:'',
            product_id_selected_Reiceve:'',
            typeProduct:null,
            bijak_no:'',
            contSize:null,
            part1:'',
            part2:'',
            destination:'',
            origin:'',
            totalType:'',
            description:''
        })
    }

    checkDriverReceive(){
        if(this.state.typeProduct === null){
            this.setState({messageRec:'لطفا یک وسیله نقلیه انتخاب کنید'})
        }
        else if(this.state.contSize === null && this.state.typeProduct == 1){
            this.setState({messageRec:'لطفا سایز کانتینر را انتخاب کنید'})

        }
        else if(this.state.bijak_no === ''){
            this.setState({messageRec:'لطفا شماره بیجک را وارد کنید'})

        }
        else if(this.state.part1 === '' && this.state.contSize == 40){
            this.setState({messageRec:'لطفا کد را وارد کنید'})

        }
        else if((this.state.part1 === '' || this.state.part2 === '') && this.state.contSize == 20){
            this.setState({messageRec:'لطفا کد را وارد کنید'})

        }
        else if(this.state.origin === ''){
            this.setState({messageRec:'لطفا یک مبدا انتخاب کنید'})

        }
        else if(this.state.destination === ''){
            this.setState({messageRec:'لطفا یک مقصد انتخاب کنید'})

        }
        else if(this.state.totalType === '' && this.state.typeProduct == 1){
            this.setState({messageRec:'لطفا نوع بار را وارد کنید'})

        }
        else if(this.state.typeProductF === '' && this.state.typeProduct == 2){
            this.setState({messageRec:'لطفا نوع بار را وارد کنید'})
        }
        else if(this.state.description === ''){
            this.setState({messageRec:'لطفا توضیحات را وارد کنید'})

        }
        else{
            this.setState({messageRec:''})
            this.DriverReceive()
        }
    }
    async DriverReceive(){

        if(this.state.typeProduct == 1){
            this.setState({LoadingRec:true})

            try {
                //this.setState({LoadingProduct:true})
                let formData = new FormData();
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("usertoken", this.props.user.apiToken);
                formData.append("type", this.state.typeProduct);
                formData.append("bijak_no", this.state.bijak_no);
                formData.append("container_size", this.state.contSize);
                formData.append("container_code1", this.state.part1);
                formData.append("container_code2", this.state.part2);
                formData.append("destination", this.state.destination);
                formData.append("origin", this.state.origin);
                formData.append("cargo_type", this.state.totalType);
                formData.append("description", this.state.description);
                
                let response = await fetch(this.props.global.baseApiUrl + "/order/driverreceipt",
                    {
                        method: "POST",
                        body: formData
                    });
                    let json = await response.json();
                    console.log(formData)
                    console.log(json)
                    if(json.success == true){
                        
                        this.setState({LoadingRec:false, Done:true,isModalacceptProduct:false, messageRec:''},()=>this.GrowDone(),this.clearAcceptParameters())
                    }
                    else{
                        this.setState({messageRec:json.messages})
                        this.setState({LoadingRec:false})
                    }
                    //this.setState({LoadingProduct:false,LoadingMain:false,AllProduct:json.data})
                    //console.log(this.state.AllProduct)
                
                } catch (error) {
                    console.log(error)
                }

        }else{
            
            this.setState({LoadingRec:true})

            try {
                let formData = new FormData();
    
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("usertoken", this.props.user.apiToken);
                // formData.append("product_id", this.state.product_id_selected_Reiceve);
                //formData.append("cargo_type", this.state.typeProductF);
                formData.append("bijak_no", this.state.bijak_no);
                formData.append("destination", this.state.destination);
                formData.append("origin", this.state.origin);
                formData.append("description", this.state.description);
                formData.append("type", this.state.typeProduct);
                formData.append("bulk_type",this.state.typeProductF);
                console.log(formData)
    
                let response = await fetch(this.props.global.baseApiUrl + "/order/driverreceipt?",
                    {
                        method: "POST",
                        body: formData
                    });
                    let json = await response.json();
                    console.log(json)
                    if(json.success == true){
                        this.setState({LoadingRec:false ,isModalacceptProduct:false, Done:true, isModalacceptProduct:false , messageRec:''},()=>this.GrowDone(),this.clearAcceptParameters())
                    }else{
                        this.setState({messageRec:json.messages})
                        this.setState({LoadingRec:false })
                    }
                    //console.log(this.state.AllProduct)
                
                } catch (error) {
                    console.log(error)
                }
        }

    }
    renderPackageType({item}){
        return (
            <TouchableOpacity style={{padding:15,borderBottomColor:'#eaeaea',borderBottomWidth:1}}
                onPress={()=>this.setState({PackageTypeName:item.name ,isModalPackageType:false,})}>
                <Text style={{fontFamily:'IRANSansMobile',fontSize:17,textAlign:'center',paddingRight:10}}>{item.name}</Text>
            </TouchableOpacity>
        );
    }
    async getPackageType(){
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            console.log(formData)
            let response = await fetch(this.props.global.baseApiUrl + '/product/get_package_type_list',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)

                if (json.success) {
                    this.setState({packageType:json.data,LoadingFilter:false})
                    
                    //this.addInRedux(json)

                } else {

                }
                
        } catch (error) {
            console.log(error)         
        }
    }
    handleEmpty() {
        if(this.state.LoadingDriverOrders)
        {
            return(
                <ActivityIndicator  color="#2395ff" size='large' />
            );
        }
        else
        {
            return(
                <Text style={{fontFamily:'IRANSansMobile'}}>باری وجود ندارد</Text>
            );
        }
    }
    handleEmptyProduct() {
        if(this.state.LoadingProduct)
        {
            return(
                <ActivityIndicator  color="#2395ff" size='large' />
            );
        }
        else
        {
            return(
                <Text style={{fontFamily:'IRANSansMobile',textAlign:'center'}}>باری وجود ندارد</Text>
            );
        }
    }
    async getCarType(){
        try {

            let formData = new FormData();

            formData.append("admintoken", "2dab06cddd5a3d6e6a40b874a457c25de769963bf4485c3e004b570bf74a4118");

            let response = await fetch(this.props.global.baseApiUrl + "/product/get_carCategory_list",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                //console.log(response)

                this.setState({carTypes:json.data,loading_carType:false})
            
            } catch (error) {
                console.log(error)
            }
    }

    async getCarTypeById(){
        try {

            let formData = new FormData();

            formData.append("admintoken", "2dab06cddd5a3d6e6a40b874a457c25de769963bf4485c3e004b570bf74a4118");
            formData.append("car_category_id", this.state.carSelectedId);

            let response = await fetch(this.props.global.baseApiUrl + "/product/get_car_by_carCategory_id",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                //console.log(response)

                this.setState({carTypesById:json.data,loading_carTypeById:false})
            
            } catch (error) {
                console.log(error)
            }
    }
    renderCarType({item}){
        return (
            <TouchableOpacity style={{padding:15,borderBottomColor:'#eaeaea',borderBottomWidth:1}}
                onPress={()=>this.setState({carSelectedId:item.id , carSelectedName:item.name , isModalCar_type:false,isModalCar_typeById:true,loading_carTypeById:true},()=>this.getCarTypeById())}>
                <Text style={{fontFamily:'IRANSansMobile',fontSize:17,textAlign:'center',paddingRight:10}}>{item.name}</Text>
            </TouchableOpacity>
        );
    }
    renderCarTypeById({item}){
        return (
            <TouchableOpacity style={{padding:15,borderBottomColor:'#eaeaea',borderBottomWidth:1}}
                onPress={()=>this.setState({carSelectedId_second:item.id , carSelectedName_second:item.name ,isModalCar_typeById:false,})}>
                <Text style={{fontFamily:'IRANSansMobile',fontSize:17,textAlign:'center',paddingRight:10}}>{item.name}</Text>
            </TouchableOpacity>
        );
    }
    
    async Provincelist(){
        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + "/user/get_province_list",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                console.log(json)
                this.setState({dataUserProvince:json.data ,arrayholder:json.data , LoadingProvince:false})
            } catch (error) {
                console.log(error)
            }
    }
    renderProvincelist({item}){
        return(
            <TouchableOpacity style={{alignItems:'center',justifyContent:'center',paddingVertical:10,borderBottomColor:'#aaaa',borderBottomWidth:0.5}}
                onPress={()=>this.setState({
                    //Province_selected:item.id,
                    isModalProvincelist:false, 
                    Province_selected_name:item.name,
                    // isModalProvincelistById:true,
                    // LoadingProvinceById:true
                    })}>
                <Text style={{fontFamily:'IRANSansMobile'}}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    renderProvincelistDst({item}){
        return(
            <TouchableOpacity style={{alignItems:'center',justifyContent:'center',paddingVertical:10,borderBottomColor:'#aaaa',borderBottomWidth:0.5}}
                onPress={()=>this.setState({
                    //Province_selectedDst:item.id,
                    isModalProvincelistDst:false, 
                    Province_selected_nameDst:item.name,
                    // isModalProvincelistByIdDst:true,
                    // LoadingProvinceById:true
                    })}>
                <Text style={{fontFamily:'IRANSansMobile'}}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    searchFilterFunction (text) {    
        const newData = this.state.arrayholder.filter(item => {      
          const itemData = `${item.name.toUpperCase()}`;
          
           const textData = text.toUpperCase();
            
           return itemData.indexOf(textData) > -1;    
        });
        
        this.setState({ dataUserProvince: newData });  
    };

    async Filtering(){
        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("car_filter_id", this.state.carSelectedId_second);
            formData.append("origin", this.state.Province_selected_name);
            formData.append("destination", this.state.Province_selected_nameDst);
            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + "/product/allProducts_with_filter",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                console.log(json)
                this.setState({AllProduct:json.data ,isModalFilter:false})
            } catch (error) {
                console.log(error)
            }
    }

    render() {
        return (
            <Container style={{backgroundColor: '#000', flex: 1}}>
                    <PushNotif/>
                    <Image source={require('../assets/images/background.png')} style={style.img_container_home}
                           resizeMode={'cover'}/>
                           <View style={{backgroundColor:'#aaa', position:'absolute',top:0,height:'100%',width:'100%',opacity:.12}}/>
                    <Image source={require('../assets/images/mappp.png')} style={style.img_top_home}/>

                    <Header transparent style={style.view_top_header}>
                        <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                        
                        <Right style={{flex: 1}}>
                            <Button transparent>
                                <TouchableOpacity activeOpacity={.5} onPress={() => Actions.drawerOpen()}
                                                  style={style.view_menu_top}>
                                    <Image source={require('../assets/images/menu.png')} style={style.menu_top}
                                           resizeMode={'contain'}/>
                                </TouchableOpacity>
                            </Button>
                        </Right>
                    </Header>
                    <Content>
                        <View style={style.view_menu_home12}>
                            <View style={style.view_top_header_menu}>
                                <Text style={style.txt_view_top_header_menu}>درخواست های حمل و نقل از شما</Text>
                            </View>
                            <View style={style.view_center_menu}>
                                {this.sample(0)}
                                {this.sample(1)}
                                {this.sample(2)}

                            </View>
                            <TouchableOpacity activeOpacity={.5} style={style.view_btn_showAll} onPress={()=>this.setState({LoadingProduct:true,isModalProduct:true},()=>this.GetAll())}>
                                <Text style={style.txt_view_top_header_menu1}>مشاهده تمام درخواست ها</Text>
                            </TouchableOpacity>
                        </View>
                        {/* <View style={style.view_menu_home1}>
                            <Image source={require('../assets/images/lifterak.png')} style={style.tik_img1}
                                   resizeMode={'contain'}/>
                            <Text style={style.txt_menu1}>سرویس حمل‌ونقل با لیفتراک
                                به سرویس‌ها اضافه شد</Text>
                        </View> */}
                         <View style={style.view_center_home}>
                            {/*<TouchableOpacity activeOpacity={.6}>
                            <View style={style.view_menu_home}>
                                <Image source={require('../assets/images/transaction.png')} style={style.tik_img}
                                       resizeMode={'contain'}/>
                                <Text style={style.txt_menu}>تراکنش های مالی شما</Text>
                                <Image source={require('../assets/images/transection_dot.png')}
                                       style={style.transection_img}
                                       resizeMode={'contain'}/>
                            </View>
                            </TouchableOpacity>
                            */}
                            <TouchableOpacity onPress={()=>this.setState({isModaldriverOrders:true,LoadingDriverOrders:true},()=>this.DriverOrders())} style={style.view_menu_home}>
                                <Image source={require('../assets/images/tik.png')} style={style.tik_img}
                                       resizeMode={'contain'}/>
                                <Text style={style.txt_menu}>فهرست بارهای پذیرفته شده</Text>
                                <Image source={require('../assets/images/transaction_dot1.png')}
                                       style={style.transection_img}
                                       resizeMode={'contain'}/>
                            </TouchableOpacity>
                        </View>      
                    </Content>
                    {/* success */}
                    <Modal isVisible={this.state.Done} animationOut="fadeOut" onBackdropPress={()=>this.setState({Done:false})} onBackButtonPress={()=>this.setState({Done:false})}>
                        <View style={{ flex: 1 ,justifyContent:'center'}}>
                        <Animated.View style={{scaleX:this.state.DoneAnim,
                                        scaleY:this.state.DoneAnim}}>
                                    <TouchableOpacity
                                        onPress={()=>this.setState({Done:false})}
                                        style={{height:150,width:150,elevation:75, borderRadius:75,backgroundColor:'#25B92E',alignSelf:'center',alignItems:'center',justifyContent:'center'}}>
                                        <IconFooter size={width * .1} color='white' name='check'/>
                                        <Text style={{color:'white',fontFamily:'IRANSansMobile', textAlign:'center',fontSize:13}}>با موفقیت انجام شد</Text>
                                    </TouchableOpacity>
                            </Animated.View>
                        </View>
                    </Modal>
                    <Modal style={{margin:0,marginTop:120,marginBottom:0}} isVisible={this.state.isModaldriverOrders} backdropOpacity={0.80} backdropColor='#203fff' backdropOpacity={0.8}
                        onBackdropPress={() => this.setState({ isModaldriverOrders: false,message:'' })} onBackButtonPress={() => this.setState({ isModaldriverOrders: false,message:''})}>
                        <Container style={{borderTopRightRadius:50,paddingTop:10,borderTopLeftRadius:50,alignItems:'center',justifyContent:'center'}}>
                            <View style={styles.view_top_Approved}>
                                <View style={styles.viewTop_topApproved}>
                                    <Image source={require('../assets/images/tik.png')} style={styles.tik_imgTopApproved}
                                        resizeMode={'contain'}/>
                                </View>
                                <View style={styles.viewTop_centerApproved}>
                                    <Text style={styles.txt_topApproved}>فهرست بارهای پذیرفته شده</Text>
                                </View>
                                <View style={styles.viewTop_bottomApproved}>
                                    <Image source={require('../assets/images/transaction_dot1.png')}
                                        style={styles.transection_img}
                                        resizeMode={'contain'}/>
                                </View>
                            </View>
                            <Content style={{width:'100%'}}>
                                {(this.state.message !== '') &&  <Text style={{textAlign:'center',fontFamily:'IRANSansMobile'}}>{this.state.message}</Text>}
                                {(this.state.LoadingDriverOrders) &&  <ActivityIndicator color="#2395ff" size="large"/>}
                                {(!this.state.LoadingDriverOrders) && 
                                    <FlatList
                                        data={this.state.DriverOrders}
                                        renderItem={this.renderDriverOrders.bind(this)}
                                        keyExtractor={(item) => item.id.toString()}
                                        style={{ backgroundColor: 'transparent' }}
                                        
                                    />
                                }
                            </Content>
                            
                        </Container>
                    </Modal>
                    <Modal style={{margin:0,marginTop:45}} isVisible={this.state.isModalReiceve} backdropOpacity={0.80}  
                        onBackdropPress={() => this.setState({ isModalReiceve: false })} onBackButtonPress={() => this.setState({ isModalReiceve: false})}>
                        <View style={style.viewContainer_SearchBar}>
                            <View style={[style.viewContainer_InputSearchBar,{justifyContent:'center',alignItems:'center'}]}>
                                <View style={style.view_iconInputBar}>
                                    <TouchableOpacity activeOpacity={.7}
                                                      onPress={() => this.setState({isModalReiceve: false,message:''})}>
                                        <Icond size={width * .05} color={'#000'} name={'times'}/>
                                    </TouchableOpacity>
                                    <Icond size={width * .05} color={'#000'} name={'search'}/>
                                </View>
                                <View style={style.view_InputBar}>
                                    <Input
                                        style={style.txt_searchBar}
                                        placeholder='جستجو ...'
                                        placeholderTextColor={'#999999'}
                                        onChangeText={(text) => this.setState({searchBar: text})}
                                    />
                                </View>
                            </View>
                            {/* {(this.state.message !== '') && <Text>{this.state.message}</Text>} */}

                            {(this.state.LoadingProduct) &&  <ActivityIndicator color="#2395ff" size="large"/>}
                            {(!this.state.LoadingProduct) && 
                            <View style={style.viewContainerBar}>
                                <FlatList
                                    numColumns={3}
                                    style={{width:width, marginBottom:70}}
                                    columnWrapperStyle={{justifyContent:'space-around'}}
                                    data={this.state.AllProduct}
                                    renderItem={this.renderBarsReiceve}
                                    keyExtractor={(item, index) => index.toString()}/>
                                    

                            </View>
                            }
                        </View>
                    </Modal>
                     {/*package type*/}
                    <Modal style={{marginVertical:70,borderRadius:50}} isVisible={this.state.isModalPackageType}
                        onBackdropPress={() => this.setState({ isModalPackageType: false })} onBackButtonPress={() => this.setState({ isModalPackageType: false})}>
                        <Container style={{paddingHorizontal:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>نوع بسته بندی را انتخاب کنید</Text>
                            {/* <View style={{height:50,width:"90%",marginTop:5}}>
                            <Input
                                style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                                placeholder="جستجو ..."                        
                                onChangeText={text => this.searchFilterCarType(text)}
                                autoCorrect={false}             
                            />  
                            </View> */}
                            <Content style={{width:'100%',marginBottom:10}}>
                                {(this.state.LoadingFilter) && <ActivityIndicator color   ="#2395ff" size="large"/>}
                                {(!this.state.LoadingFilter) && 
                                    <FlatList
                                        data={this.state.packageType}
                                        renderItem={this.renderPackageType.bind(this)}
                                        keyExtractor={(item) => item.id.toString()}
                                        style={{ backgroundColor: 'transparent' }}
                                    />
                                }
                            </Content>
                            
                        </Container>
                    </Modal>
                    {/* car_type */}
                    <Modal style={{marginVertical:70,borderRadius:50}} isVisible={this.state.isModalCar_type}
                        onBackdropPress={() => this.setState({ isModalCar_type: false })} onBackButtonPress={() => this.setState({ isModalCar_type: false})}>
                        <Container style={{paddingHorizontal:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>خودرو مورد نظر را انتخاب کنید</Text>
                        {/* <View style={{height:50,width:"90%",marginTop:5}}>
                        <Input
                            style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                            placeholder="جستجو ..."                        
                            onChangeText={text => this.searchFilterCarType(text)}
                            autoCorrect={false}             
                        />  
                        </View> */}
                            <Content style={{width:'100%',marginBottom:10}}>
                                {(this.state.loading_carType) && <ActivityIndicator color   ="#2395ff" size="large"/>}
                                {(!this.state.loading_carType) && 
                                    <FlatList
                                        data={this.state.carTypes}
                                        renderItem={this.renderCarType.bind(this)}
                                        keyExtractor={(item) => item.id.toString()}
                                        style={{ backgroundColor: 'transparent' }}
                                    />
                                }
                            </Content>
                        
                        </Container>
                    </Modal>
                    {/* car_type by id*/}
                    <Modal style={{marginVertical:70,borderRadius:50}} isVisible={this.state.isModalCar_typeById}
                        onBackdropPress={() => this.setState({ isModalCar_typeById: false })} onBackButtonPress={() => this.setState({ isModalCar_typeById: false})}>
                        <Container style={{paddingHorizontal:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>نوع خودرو مورد نظر را انتخاب کنید</Text>
                            {/* <View style={{height:50,width:"90%",marginTop:5}}>
                            <Input
                                style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                                placeholder="جستجو ..."                        
                                onChangeText={text => this.searchFilterCarType(text)}
                                autoCorrect={false}             
                            />  
                            </View> */}
                            <Content style={{width:'100%',marginBottom:10}}>
                                {(this.state.loading_carTypeById) && <ActivityIndicator color   ="#2395ff" size="large"/>}
                                {(!this.state.loading_carTypeById) && 
                                    <FlatList
                                        data={this.state.carTypesById}
                                        renderItem={this.renderCarTypeById.bind(this)}
                                        keyExtractor={(item) => item.id.toString()}
                                        style={{ backgroundColor: 'transparent' }}
                                    />
                                }
                            </Content>
                            
                        </Container>
                    </Modal>
                    {/* Province */}
                    <Modal style={{marginVertical:70}} isVisible={this.state.isModalProvincelist}
                        onBackdropPress={() => this.setState({ isModalProvincelist: false })} onBackButtonPress={() => this.setState({ isModalProvincelist: false})}>
                        <Container style={{paddingHorizontal:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>استان مورد نظر را انتخاب کنید</Text>
                            <View style={{height:50,width:"90%",marginTop:5}}>
                            <Input
                                style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                                placeholder="جستجو ..."                        
                                onChangeText={text => this.searchFilterFunction(text)}
                                autoCorrect={false}             
                            />  
                            </View>
                            <Content style={{width:'100%',marginBottom:10}}>
                                {(this.state.LoadingProvince) && <ActivityIndicator color   ="#2395ff" size="large"/>}
                                {(!this.state.LoadingProvince) && 
                                    <FlatList
                                        data={this.state.dataUserProvince}
                                        renderItem={this.renderProvincelist.bind(this)}
                                        keyExtractor={(item) => item.id.toString()}
                                        style={{ backgroundColor: 'transparent' }}
                                    />
                                }
                            </Content>
                            
                        </Container>
                    </Modal>
                    {/* Province Dst*/}
                    <Modal style={{marginVertical:70}} isVisible={this.state.isModalProvincelistDst}
                        onBackdropPress={() => this.setState({ isModalProvincelistDst: false })} onBackButtonPress={() => this.setState({ isModalProvincelistDst: false})}>
                        <Container style={{paddingTop:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>استان مورد نظر را انتخاب کنید</Text>
                            <View style={{height:50,width:"90%",marginTop:5}}>
                            <Input
                                style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                                placeholder="جستجو ..."                        
                                onChangeText={text => this.searchFilterFunction(text)}
                                autoCorrect={false}             
                            />  
                            </View>
                            <Content style={{width:'100%',marginBottom:10}}>
                                {(this.state.LoadingProvince) && <ActivityIndicator color="#2395ff" size="large"/>}
                                {(!this.state.LoadingProvince) && 
                                    <FlatList
                                        data={this.state.dataUserProvince}
                                        renderItem={this.renderProvincelistDst.bind(this)}
                                        keyExtractor={(item) => item.id.toString()}
                                        style={{ backgroundColor: 'transparent',paddingHorizontal:20 }}
                                    />
                                }
                            </Content>
                            
                        </Container>
                    </Modal>
                    <Modal isVisible={this.state.isModalSupport}
                       backdropOpacity={0.70}
                       onBackdropPress={() => this.setState({isModalSupport: !this.state.isModalSupport})}
                       onBackButtonPress={()=> this.setState({isModalSupport: !this.state.isModalSupport})}
                       style={styles.ModalSearchDriver}>
                    <View style={styles.viewContainerSupport}>
                        <View style={[styles.view_top_support, {height: height * .08}]}>
                            <Text style={styles.txt_top_support}>تماس با پشتیبانی</Text>
                        </View>
                        <View style={[styles.view_top_support, {height: height * .09}]}>
                            <Text style={styles.txt_center_support}>{Helper.ToPersianNumber('09200501200')}</Text>
                        </View>
                        <View style={[styles.view_bottom_support]}>
                            <TouchableOpacity activeOpacity={.7} onPress={() => this.setState({isModalSupport: false})}>
                                <View style={styles.btnSupport1}>
                                    <Text style={styles.txt_bottom_support1}>لغو</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={.7} onPress={() => Linking.openURL('tel:09200501200')}>
                                <View style={styles.btnSupport}>
                                    <Text style={styles.txt_bottom_support}>تماس</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    </Modal>
                    {/* filter */}
                    <Modal style={{margin:0,marginTop:150}} isVisible={this.state.isModalFilter} backdropOpacity={0.80} 
                        onBackdropPress={() => this.setState({ isModalFilter: false })} onBackButtonPress={() => this.setState({ isModalFilter: false})}>      
                        <View style={style.viewContainer_SearchBar}>
                            <View style={style.viewContainer_InputSearchBar}>
                                <View style={[style.view_iconInputBar,{width:"100%",justifyContent:'space-between'}]}>
                                    <TouchableOpacity style={{marginLeft:20}} activeOpacity={.7}
                                                      onPress={() => this.setState({isModalFilter: false,message:''})}>
                                        <Icond size={width * .05} color={'#000'} name={'times'}/>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <Text style={{fontFamily:'IRANSansMobile',color:'black',marginRight:20,fontSize:16}}>مرتب سازی بر اساس :</Text>
                            <Content>
                                <View style={{padding:20,borderWidth:1,borderColor:'#eee',margin:20,borderRadius:20,backgroundColor:'#eee'}}>
                                    <View style={{flexDirection:'row-reverse',alignItems:'center',marginBottom:20,justifyContent:'space-between'}}>
                                        <Text style={{fontFamily:'IRANSansMobile',color:'black'}}>نوع خودرو : </Text>
                                        <TouchableOpacity onPress={()=>this.setState({isModalCar_type:true,loading_carType:true},()=>this.getCarType())} style={{backgroundColor:'#fff',width:'70%',justifyContent:'center',alignItems:'center',padding:10,borderRadius:10,elevation:5}}>
                                            <Text style={{fontFamily:'IRANSansMobile',color:'black'}}>{(this.state.carSelectedName_second != '')? this.state.carSelectedName  + ' , ' +this.state.carSelectedName_second : 'انتخاب کنید'}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flexDirection:'row-reverse',alignItems:'center',marginBottom:20,justifyContent:'space-between'}}>
                                        <Text style={{fontFamily:'IRANSansMobile',color:'black'}}>مبدا : </Text>
                                        <TouchableOpacity onPress={()=>this.setState({isModalProvincelist:true,LoadingProvince:true},()=>this.Provincelist())} style={{backgroundColor:'#fff',width:'70%',justifyContent:'center',alignItems:'center',padding:10,borderRadius:10,elevation:5}}>
                                            <Text style={{fontFamily:'IRANSansMobile',color:'black'}}>{(this.state.Province_selected_name != '')? this.state.Province_selected_name : 'انتخاب کنید'}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flexDirection:'row-reverse',alignItems:'center',marginBottom:20,justifyContent:'space-between'}}>
                                        <Text style={{fontFamily:'IRANSansMobile',color:'black'}}>مقصد : </Text>
                                        <TouchableOpacity onPress={()=>this.setState({isModalProvincelistDst:true,LoadingProvince:true},()=>this.Provincelist())} style={{backgroundColor:'#fff',width:'70%',justifyContent:'center',alignItems:'center',padding:10,borderRadius:10,elevation:5}}>
                                            <Text style={{fontFamily:'IRANSansMobile',color:'black'}}>{(this.state.Province_selected_nameDst != '')? this.state.Province_selected_nameDst : 'انتخاب کنید'}</Text>
                                        </TouchableOpacity>
                                    </View>
                                   
                                    
                                </View>
                            </Content>
                            <TouchableOpacity onPress={()=>this.Filtering()} style={{padding:10,paddingHorizontal:20,backgroundColor:'#2395ff'}}>
                                <Text style={{fontFamily:'IRANSansMobile',textAlign:'center',color:'white',fontSize:18}}>اعمال فیلتر</Text>
                            </TouchableOpacity>
                        </View>
                    </Modal>
                    <Modal style={{margin:0,marginTop:45}} isVisible={this.state.isModalProduct} backdropOpacity={0.80} 
                        onBackdropPress={() => this.setState({ isModalProduct: false },()=>this.GetAll())} onBackButtonPress={() => this.setState({ isModalProduct: false},()=>this.GetAll())}>      
                        <View style={style.viewContainer_SearchBar}>
                            <View style={style.viewContainer_InputSearchBar}>
                                <View style={[style.view_iconInputBar,{width:"100%",justifyContent:'space-between'}]}>
                                    <TouchableOpacity style={{marginLeft:20}} activeOpacity={.7}
                                                      onPress={() => this.setState({isModalProduct: false,message:''},()=>this.GetAll())}>
                                        <Icond size={width * .05} color={'#000'} name={'times'}/>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>this.setState({isModalFilter:true,Province_selected_nameDst:'',Province_selected_name:'',carSelectedName:'',carSelectedName_second:'',carSelectedId:'',carSelectedId_second:''})} style={{marginRight:30}}>
                                        <Icond size={width * .05} color={'#000'} name={'filter'}/>
                                    </TouchableOpacity>
                                </View>
                                {/* <View style={style.view_InputBar}>
                                    <Input
                                        style={style.txt_searchBar}
                                        placeholder='جستجو ...'
                                        placeholderTextColor={'#999999'}
                                        onChangeText={(text) => this.setState({searchBar: text})}
                                    />
                                </View> */}
                            </View>
                            {/* {(this.state.LoadingProduct) &&  <ActivityIndicator color="#2395ff" size="large"/>}
                            {(!this.state.LoadingProduct) &&  */}
                            <View style={[style.viewContainerBar,{marginBottom:10}]}>
                                <FlatList
                                    style={{width:width, marginBottom:70}}
                                    columnWrapperStyle={{justifyContent:'space-around'}}
                                    numColumns={2}
                                    data={this.state.AllProduct}
                                    renderItem={this.renderBars}
                                    ListEmptyComponent={this.handleEmptyProduct.bind(this)}
                                    keyExtractor={(item, index) => index.toString()}/>
                            </View>                            
                        </View>
                    </Modal>    
                    <Modal style={{margin:0,marginTop:45 }} isVisible={this.state.isModalacceptProduct} backdropOpacity={0.80}  
                        onBackdropPress={() => this.setState({ isModalacceptProduct: false })} onBackButtonPress={() => this.setState({ isModalacceptProduct: false})}>
                        
                        <Content style={{borderTopLeftRadius: width * .12, borderTopRightRadius: width * .12,paddingTop:15,backgroundColor:'#fff'}}>
                            <Text style={{fontFamily:'IRANSansMobile', textAlign:'center', fontSize:18,color:'black', paddingTop:10}}>تایید تحویل</Text>
                            <View style={{flexDirection:'row',justifyContent:'space-around', width:width,paddingHorizontal:10 }}>
                                {(this.state.typeProduct == null) &&
                                <View style={{flex:1,flexDirection:'row'}}>
                                    <TouchableOpacity onPress={()=>this.setState({typeProduct:2,messageRec:''})} style={{flex:1,backgroundColor:'#419aff', alignItems:'center', margin:10, borderRadius:10, paddingVertical:5}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17, color:'white'}}>فله</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>this.setState({typeProduct:1,messageRec:''})} style={{flex:1,backgroundColor:'#419aff', alignItems:'center', margin:10, borderRadius:10,paddingVertical:5}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17, color:'white'}}>کانتینر</Text>
                                    </TouchableOpacity>
                                </View>
                                }
                                {(this.state.typeProduct == 1) &&
                                <View style={{flex:1,flexDirection:'row'}}>
                                    <TouchableOpacity onPress={()=>this.setState({typeProduct:null ,contSize:null, totalType:'',messageRec:''},()=>this.clearAcceptParameters())} style={{flex:1,backgroundColor:'#419aff', alignItems:'center', margin:10, borderRadius:10,paddingVertical:5}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17, color:'white'}}>کانتینر</Text>
                                    </TouchableOpacity>
                                </View>
                                }
                                {(this.state.typeProduct == 2) &&
                                <View style={{flex:1,flexDirection:'row'}}>
                                    <TouchableOpacity onPress={()=>this.setState({typeProduct:null,contSize:null, totalType:'',messageRec:''},()=>this.clearAcceptParameters())} style={{flex:1,backgroundColor:'#419aff', alignItems:'center', margin:10, borderRadius:10, paddingVertical:5}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17, color:'white'}}>فله</Text>
                                    </TouchableOpacity>
                                </View>
                                }
                            </View>
                            {(this.state.typeProduct == 2) && 
                                <View style={{flexDirection:'row-reverse',alignItems:'center',marginVertical:5,paddingHorizontal:10}}>
                                    <Text style={{fontFamily:'IRANSansMobile',fontSize:18,marginRight:10,color:'black'}}>شماره بیجک : </Text>
                                    <Input
                                        maxLength={15}
                                        keyboardType='numeric'
                                        style={{borderRadius:10,borderWidth:1,fontSize:18,borderColor:'#419aff',marginHorizontal:10, textAlign:'center',paddingRight:10,fontFamily:'IRANSansMobile'}}
                                        onChangeText={(text) => this.setState({bijak_no: text})}
                                        />
                                </View>
                            }
                            {(this.state.typeProduct == 2) && 
                                <View style={{marginTop:10}}>
                                    <View style={{flexDirection:'row-reverse', alignItems:'center',justifyContent:'space-between',width:width,paddingHorizontal:10}}>
                                        <Text style={{fontFamily:'IRANSansMobile' ,fontSize:18,color:'black',marginRight:10}}>مبدا :</Text>
                                        <Input
                                            style={{borderRadius:10,borderWidth:1,borderColor:'#419aff',marginHorizontal:10, textAlign:'right',paddingRight:10,fontFamily:'IRANSansMobile'}}
                                            onChangeText={(text) => this.setState({origin: text})}
                                            value={this.state.origin}
                                        />
                                    </View>
                                </View>
                            }
                            {(this.state.typeProduct == 2 ) && 
                                <View style={{marginTop:20}}>
                                    <View style={{flexDirection:'row-reverse', alignItems:'center',justifyContent:'space-between',width:width,paddingHorizontal:10}}>
                                        <Text style={{fontFamily:'IRANSansMobile' ,fontSize:18,color:'black',marginRight:10}}>مقصد :</Text>
                                        <Input
                                            style={{borderRadius:10,borderWidth:1,borderColor:'#419aff',marginHorizontal:10, textAlign:'right',paddingRight:10,fontFamily:'IRANSansMobile'}}
                                            onChangeText={(text) => this.setState({destination: text})}
                                            value={this.state.destination}
                                        />
                                    </View>
                                </View>
                            }
                            {(this.state.typeProduct == 2 ) && 
                                <View style={{marginTop:20, marginBottom:20}}>
                                    <View style={{flexDirection:'row-reverse', alignItems:'center',justifyContent:'space-between',width:width,paddingHorizontal:10}}>
                                        <Text style={{fontFamily:'IRANSansMobile' ,fontSize:18,color:'black',marginRight:10}}>نوع بار :</Text>
                                        <Input
                                            style={{borderRadius:10,borderWidth:1,borderColor:'#419aff',marginHorizontal:10, textAlign:'right',paddingRight:10,fontFamily:'IRANSansMobile'}}
                                            onChangeText={(text) => this.setState({typeProductF: text})}
                                            value={this.state.typeProductF}
                                        />
                                    </View>
                                </View>
                            }
                            {(this.state.typeProduct == 1) && 
                                <View style={{ flexDirection:'row-reverse', alignItems:'center',paddingHorizontal:10}}>
                                    <Text style={{fontFamily:'IRANSansMobile', fontSize:16, color:'black',marginRight:10}}> سایز کانتینر :</Text>
                                    {(this.state.contSize == null) && 
                                    <View style={{flex:1, flexDirection:'row'}}>
                                        <TouchableOpacity onPress={()=>this.setState({contSize:"20",messageRec:''})} style={{flex:1,backgroundColor:'#419aff', alignItems:'center', margin:10, borderRadius:10, paddingVertical:5}}>
                                            <Text style={{fontFamily:'IRANSansMobile', color:'white',fontSize:18}}>۲۰</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={()=>this.setState({contSize:"40",messageRec:''})} style={{flex:1,backgroundColor:'#419aff', alignItems:'center', margin:10, borderRadius:10, paddingVertical:5}}>
                                            <Text style={{fontFamily:'IRANSansMobile', color:'white',fontSize:18}}>۴۰</Text>
                                        </TouchableOpacity>
                                    </View>}
                                    {(this.state.contSize == "20") && 
                                    <TouchableOpacity onPress={()=>this.setState({contSize:null,messageRec:''})} style={{flex:1,backgroundColor:'#419aff', alignItems:'center', margin:10, borderRadius:10, paddingVertical:5}}>
                                        <Text style={{fontFamily:'IRANSansMobile', color:'white',fontSize:18}}>۲۰</Text>
                                    </TouchableOpacity>
                                    }
                                    {(this.state.contSize == "40") && 
                                    <TouchableOpacity onPress={()=>this.setState({contSize:null,messageRec:''})} style={{flex:1,backgroundColor:'#419aff', alignItems:'center', margin:10, borderRadius:10, paddingVertical:5}}>
                                        <Text style={{fontFamily:'IRANSansMobile', color:'white',fontSize:18}}>۴۰</Text>
                                    </TouchableOpacity>
                                    }
                                </View>
                                }
                                {(this.state.typeProduct == 1 && (this.state.contSize =="20" || this.state.contSize =="40")) && 
                                <View style={{flexDirection:'row-reverse',alignItems:'center',marginVertical:5,paddingHorizontal:10}}>
                                    <Text style={{fontFamily:'IRANSansMobile',fontSize:18,marginRight:10,color:'black'}}>شماره بیجک : </Text>
                                    <Input
                                        maxLength={15}
                                        keyboardType='numeric'
                                        value={this.state.bijak_no}
                                        style={{borderRadius:10,borderWidth:1,fontSize:18,borderColor:'#419aff',marginHorizontal:10, textAlign:'center',paddingRight:10,fontFamily:'IRANSansMobile'}}
                                        onChangeText={(text) => this.setState({bijak_no: text})}
                                        />
                                </View>
                                }
                                {(this.state.contSize =="20") &&
                                    <View style={{flexDirection:'row-reverse',alignItems:'center',marginVertical:5,paddingHorizontal:10}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,marginRight:10,color:'black'}}>کد : </Text>
                                        <Input
                                            maxLength={7}
                                            keyboardType='numeric'
                                            value={this.state.part1}
                                            style={{borderRadius:10,borderWidth:1,fontSize:18,borderColor:'#419aff',marginHorizontal:10, textAlign:'center',paddingRight:10,fontFamily:'IRANSansMobile'}}
                                            onChangeText={(text) => this.setState({part1: text})}
                                            />
                                        <Input
                                            maxLength={7}
                                            value={this.state.part2}
                                            keyboardType='numeric'
                                            style={{borderRadius:10,borderWidth:1,fontSize:18,borderColor:'#419aff',marginHorizontal:10, textAlign:'center',paddingRight:10,fontFamily:'IRANSansMobile'}}
                                            onChangeText={(text) => this.setState({part2: text})}
                                        />
                                    </View>
                                }
                                {(this.state.contSize =="40") &&
                                    <View style={{flexDirection:'row-reverse',alignItems:'center',marginVertical:5,paddingHorizontal:10}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,marginRight:10,color:'black'}}>کد : </Text>
                                        <Input
                                            maxLength={7}
                                            keyboardType='numeric'
                                            value={this.state.part1}
                                            style={{borderRadius:10,borderWidth:1,fontSize:18,borderColor:'#419aff',marginHorizontal:10, textAlign:'center',paddingRight:10,fontFamily:'IRANSansMobile'}}
                                            onChangeText={(text) => this.setState({part1: text})}
                                            />
                                    </View>
                                }
                                {(this.state.contSize != null && this.state.typeProduct != null) && 
                                    <View style={{marginTop:10}}>
                                        <View style={{flexDirection:'row-reverse', alignItems:'center',justifyContent:'space-between',width:width,paddingHorizontal:10}}>
                                            <Text style={{fontFamily:'IRANSansMobile' ,fontSize:18,color:'black',marginRight:10}}>مبدا :</Text>
                                            <Input
                                                style={{borderRadius:10,borderWidth:1,borderColor:'#419aff',marginHorizontal:10, textAlign:'right',paddingRight:10,fontFamily:'IRANSansMobile'}}
                                                onChangeText={(text) => this.setState({origin: text})}
                                                value={this.state.origin}
                                            />
                                        </View>
                                    </View>
                                }
                                {(this.state.contSize != null && this.state.typeProduct != null) && 
                                    <View style={{marginTop:20}}>
                                        <View style={{flexDirection:'row-reverse', alignItems:'center',justifyContent:'space-between',width:width,paddingHorizontal:10}}>
                                            <Text style={{fontFamily:'IRANSansMobile' ,fontSize:18,color:'black',marginRight:10}}>مقصد :</Text>
                                            <Input
                                                style={{borderRadius:10,borderWidth:1,borderColor:'#419aff',marginHorizontal:10, textAlign:'right',paddingRight:10,fontFamily:'IRANSansMobile'}}
                                                onChangeText={(text) => this.setState({destination: text})}
                                                value={this.state.destination}
                                            />
                                        </View>
                                    </View>
                                }
                                {(this.state.typeProduct == 1 && this.state.contSize != null) &&
                                <View style={{flexDirection:'column',alignItems:'center',marginVertical:10,marginHorizontal:10}}>
                                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                        <TouchableOpacity onPress={()=>this.setState({totalType:1})} style={(this.state.totalType == 1) ? style.selectedBtn : style.unselectedBtn}>
                                            <Text style={(this.state.totalType == 1) ? style.selectedText : style.unselectedText}>صادرات</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={()=>this.setState({totalType:2})} style={(this.state.totalType == 2) ? style.selectedBtn : style.unselectedBtn}>
                                            <Text style={(this.state.totalType == 2) ? style.selectedText : style.unselectedText}>واردات</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View  style={{flexDirection:'row',justifyContent:'space-between'}}>
                                        <TouchableOpacity onPress={()=>this.setState({totalType:3})} style={(this.state.totalType == 3) ? style.selectedBtn : style.unselectedBtn}>
                                            <Text style={(this.state.totalType == 3) ? style.selectedText : style.unselectedText}>استریپ</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={()=>this.setState({totalType:4})} style={(this.state.totalType == 4) ? style.selectedBtn : style.unselectedBtn}>
                                            <Text style={(this.state.totalType == 4) ? style.selectedText : style.unselectedText}>ارسال به شناور</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                }
                                {(this.state.typeProduct ==1 && this.state.contSize != null) && 
                                    <View style={{marginTop:0}}>
                                        <View style={{flexDirection:'row-reverse', alignItems:'center',justifyContent:'space-between',width:width,paddingHorizontal:10}}>
                                            <Text style={{fontFamily:'IRANSansMobile' ,fontSize:18,color:'black',marginRight:10}}>توضیحات :</Text>
                                            <Input
                                                style={{borderRadius:10,height:100,borderWidth:1,borderColor:'#419aff',marginHorizontal:10, textAlign:'right',textAlignVertical:'top',paddingRight:10,fontFamily:'IRANSansMobile'}}
                                                onChangeText={(text) => this.setState({description: text})}
                                                value={this.state.description}
                                                multiline={true}                                             
                                            />
                                        </View>
                                    </View>
                                }
                                {(this.state.typeProduct == 2 ) && 
                                    <View style={{marginTop:0}}>
                                        <View style={{flexDirection:'row-reverse', alignItems:'center',justifyContent:'space-between',width:width,paddingHorizontal:10}}>
                                            <Text style={{fontFamily:'IRANSansMobile' ,fontSize:18,color:'black',marginRight:10}}>توضیحات :</Text>
                                            <Input
                                                style={{borderRadius:10,height:100,borderWidth:1,borderColor:'#419aff',marginHorizontal:10, textAlign:'right',paddingRight:10,textAlignVertical:'top',fontFamily:'IRANSansMobile'}}
                                                onChangeText={(text) => this.setState({description: text})}
                                                value={this.state.description}
                                                multiline={true}
                                            />
                                        </View>
                                    </View>
                                }
                                {this.state.messageRec != "" && <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',color:'red',fontSize:16}}>{this.state.messageRec}</Text>}
                                {((this.state.totalType !=null) && (this.state.contSize != null) && (!this.state.LoadingRec) )&&
                                    <TouchableOpacity onPress={()=>this.checkDriverReceive()} style={{alignItems:'center',margin:20,marginTop:5,borderRadius:10,padding:10,backgroundColor:'green'}}>
                                        <Text style={[style.selectedText,{fontSize:16}]}>تایید و ارسال</Text>
                                    </TouchableOpacity>
                                }
                                
                                {this.state.LoadingRec && <ActivityIndicator size="large" style={{marginTop:10}} color="#2395ff"/>}
                                {((this.state.typeProduct ==2) && !this.state.LoadingRec) &&
                                    <TouchableOpacity onPress={()=>this.checkDriverReceive()} style={{alignItems:'center',margin:20,marginTop:5,borderRadius:10,padding:10,backgroundColor:'green'}}>
                                        <Text style={[style.selectedText,{fontSize:16}]}>تایید و ارسال</Text>
                                    </TouchableOpacity>
                                }
                        </Content>
                    </Modal>    
                    <Modal style={{margin:0,marginTop:45}} isVisible={this.state.isModalDriverAccept} backdropOpacity={0}  
                        onBackdropPress={() => this.setState({ isModalDriverAccept: false,message:'' })} onBackButtonPress={() => this.setState({ isModalDriverAccept: false,message:''})}>
                            <Content style={[style.viewContainer_SearchBar,{marginTop:10}]}>
                                {/* <TouchableOpacity title="Hide modal"
                                                onPress={() => this.setState({isModalVisibleSignUpDriver: false})}
                                                style={style.exitModalLogin}>
                                    <Image source={require('../assets/images/request.png')}
                                        style={style.img_exitModalLogin}
                                        resizeMode={'contain'}/>
                                </TouchableOpacity> */}
                                <View style={{width:width}}>
                                    <View style={{alignItems: 'center', justifyContent: 'center',}}>
                                        <Text style={style.btnLoginAccount}>درخواست حمل و نقل <Text
                                            style={style.txt_confirm}>(در
                                            انتظار تایید)</Text></Text>
                                        <Image source={require('../assets/images/placeholder.png')} resizeMode={'contain'}
                                            style={{width:width*.9,marginVertical:-15}}/>
                                        <View style={style.view_txt_placeholder}>
                                            <Text style={style.btnLoginAccount1}>{this.state.origin_selected}</Text>
                                            <Text style={style.btnLoginAccount1}>{this.state.destination_selected}</Text>
                                        </View>
                                        <View style={style.view_txt_address}>
                                            <Text style={style.btnLoginAccount12}><Text> نشانی دقیق مبدا: </Text>
                                        {this.state.origin_address_selected}
                                        </Text>
                                        </View>
                                        <View style={style.view_txt_address}>
                                            <Text style={style.btnLoginAccount12}>
                                                <Text>نشانی دقیق مقصد: </Text>
                                            {this.state.destination_address_selected}
                                            </Text>
                                        </View>
                                        <View style={style.view_txt_address1}>
                                            <Text style={style.btnLoginAccount12}>
                                            <Text>نوع بار: </Text>
                                            {this.state.cargo_type_selected}
                                            </Text>
                                            <Text style={style.btnLoginAccount12}><Text>تناژ بار : </Text>{Helper.ToPersianNumber(String(this.state.weight_selected))} تن</Text>
                                        </View>
                                        <View style={style.view_txt_address1}>
                                            <Text style={[style.btnLoginAccount12,{paddingLeft:10}]}>
                                                <Text>مسافت : </Text>
                                            {Helper.ToPersianNumber(String(this.state.distance_Selected))} کیلومتر 
                                            </Text>
                                            <Text style={style.btnLoginAccount12}>
                                                <Text>نوع بسته بندی: </Text>
                                            {this.state.pakage_type_selected}
                                            </Text>
                                        </View>
                                        <View style={style.view_txt_address}>
                                            <Text style={[style.btnLoginAccount12]}><Text>نوع ماشین مورد
                                                نیاز : </Text>{this.state.carSelectedName} {","} {this.state.carSelectedName_second}</Text>
                                        </View>
                                        <View style={style.view_txt_address}>
                                            <Text style={style.btnLoginAccount12}>
                                                <Text> توضیحات: </Text>
                                            {this.state.description_selected}
                                            </Text>
                                        </View>         
                                        <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-around',width:'100%', marginBottom:10}}>
                                            <View style={{justifyContent:'center',alignItems:'center'}}>
                                                <Text style={style.txt_count_fix}>قیمت پیشنهادی</Text>
                                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                                    {/* <TouchableOpacity onPress={()=>this.setState({price_selected:this.state.price_selected+500000})}>
                                                        <IconFooter name="plus" color="green" style={{fontSize:26,marginRight:10}} />
                                                    </TouchableOpacity> */}
                                                    <View style={style.view_txt_count}>
                                                        <Text style={style.txt_count}>{Helper.ToPersianNumber(String(this.state.price_selected))} تومان</Text>
                                                    </View>
                                                    {/* <TouchableOpacity  onPress={()=>this.setState({price_selected:this.state.price_selected-500000 })}>
                                                        <IconFooter name="minus" color="red" style={{fontSize:26,marginLeft:10}}/>
                                                    </TouchableOpacity> */}
                                                </View>
                                            </View>
                                        </View>
                                        {(this.state.message != '') && <Text style={{fontFamily:'IRANSansMobile',textAlign:'center',color:'red'}}>{this.state.message}</Text>}
                                        {(this.state.LoadingAccept) && <ActivityIndicator size="large" color="#2395ff"/>}
                                        {(!this.state.LoadingAccept ) && 
                                        <View style={{flexDirection:'column',justifyContent:'space-around',width:width,alignItems:'center',marginBottom:10}}>
                                            <TouchableOpacity onPress={()=>this.DriverAccept()} activeOpacity={.5} style={style.btnSignUpModal}>
                                                <Text style={style.txt_btn_login}>تایید</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>this.DriverCancel()} activeOpacity={.5} style={{width:width*.6,borderColor:'#2395ff',borderWidth:1,alignItems:'center',borderRadius:5}}>
                                                <Text style={style.cancelRequest}>لغو</Text>
                                            </TouchableOpacity>
                                        </View>}
                                    </View>
                                </View>
                            </Content>
                    </Modal>
                    {/* driver Detial */}
                    <Modal style={{margin:0,marginTop:80}} isVisible={this.state.isModalOrderDetail} backdropOpacity={0}  
                        onBackdropPress={() => this.setState({ isModalOrderDetail: false })} onBackButtonPress={() => this.setState({ isModalOrderDetail: false})}>
                            <Content style={[style.viewContainer_SearchBar,{marginTop:10}]}>
                                {/* <TouchableOpacity title="Hide modal"
                                                onPress={() => this.setState({isModalVisibleSignUpDriver: false})}
                                                style={style.exitModalLogin}>
                                    <Image source={require('../assets/images/request.png')}
                                        style={style.img_exitModalLogin}
                                        resizeMode={'contain'}/>
                                </TouchableOpacity> */}
                                <View style={{width:width}}>
                                    <View style={{alignItems: 'center', justifyContent: 'center',}}>
                                        <Text style={[style.btnLoginAccount1,{paddingTop:10}]}>اطلاعات بار</Text>
                                        <Image source={require('../assets/images/placeholder.png')} resizeMode={'contain'}
                                            style={{width:width*.9,marginVertical:-15}}/>
                                        <View style={style.view_txt_placeholder}>
                                            <Text style={style.btnLoginAccount1}>{this.state.origin_selected}</Text>
                                            <Text style={style.btnLoginAccount1}>{this.state.destination_selected}</Text>
                                        </View>
                                        <View style={style.view_txt_address}>
                                            <Text style={style.btnLoginAccount12}><Text> نشانی دقیق مبدا: </Text>
                                        {this.state.origin_address_selected}
                                        </Text>
                                        </View>
                                        <View style={style.view_txt_address}>
                                            <Text style={style.btnLoginAccount12}>
                                                <Text>نشانی دقیق مقصد: </Text>
                                            {this.state.destination_address_selected}
                                            </Text>
                                        </View>
                                        <View style={style.view_txt_address1}>
                                            <Text style={style.btnLoginAccount12}>
                                                <Text>نوع بار: </Text>
                                            {this.state.cargo_type_selected}
                                            </Text>
                                            <Text style={style.btnLoginAccount12}><Text>تناژ بار : </Text>{this.state.weight_selected} تن</Text>
                                        </View>
                                        <View style={style.view_txt_address1}>
                                            <Text style={[style.btnLoginAccount12,{paddingLeft:10}]}>
                                                <Text>مسافت : </Text>
                                            {Helper.ToPersianNumber(String(this.state.distance_Selected))} کیلومتر 
                                            </Text>
                                            <Text style={style.btnLoginAccount12}>
                                                <Text> نوع بسته بندی: </Text>
                                            {this.state.pakage_type_selected}
                                            </Text>
                                        </View>
                                        <View style={style.view_txt_address}>
                                        <Text style={[style.btnLoginAccount12]}><Text>نوع ماشین مورد
                                                نیاز : </Text>{this.state.carSelectedName} {","} {this.state.carSelectedName_second}</Text>
                                        </View>
                                        <View style={style.view_txt_address}>
                                            <Text style={style.btnLoginAccount12}>
                                                <Text> توضیحات: </Text>
                                            {this.state.description_selected}
                                            </Text>
                                        </View>         
                                        <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-around',width:'100%', marginBottom:10}}>
                                            <View style={{justifyContent:'center',alignItems:'center'}}>
                                                <Text style={style.txt_count_fix}>قیمت پیشنهادی</Text>
                                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                                    {/* <TouchableOpacity onPress={()=>this.setState({price_selected:this.state.price_selected+500000})}>
                                                        <IconFooter name="plus" color="green" style={{fontSize:26,marginRight:10}} />
                                                    </TouchableOpacity> */}
                                                    <View style={style.view_txt_count}>
                                                        <Text style={style.txt_count}>{Helper.ToPersianNumber(String(this.state.price_selected))} تومان</Text>
                                                    </View>
                                                    {/* <TouchableOpacity  onPress={()=>this.setState({price_selected:this.state.price_selected-500000 })}>
                                                        <IconFooter name="minus" color="red" style={{fontSize:26,marginLeft:10}}/>
                                                    </TouchableOpacity> */}
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </Content>
                    </Modal>
                    <View>
                        <Animated.View
                            style={{
                                scaleX: this.state.removeAnim,
                                scaleY: this.state.removeAnim,
                                zIndex:99,
                                height:width*.18,
                                width:width*.18,
                                justifyContent:'center',
                                alignItems:'center',
                                alignSelf:'center',
                                bottom: height * .05, zIndex: 99,
                                
                            }}
                        >
                        <TouchableOpacity onPress={()=>this.setState({isModalacceptProduct:true})} style={style.view_add_bottom_center}>
                            <View style={style.view_icon_add_bottom_center}>
                                <IconFooter size={width * .07} color='white' name='check'/>
                            </View>
                        </TouchableOpacity>
                    </Animated.View>
                        
                        <BottomNavigate color_prof={'#419aff'} nameLeft={'پروفایل'} iconLeft={'user'}
                                        nameCenter={'ثبت بیجک'} iconCenter={'check'} nameRight={'پشتیبانی'}
                                        iconRight={'headset'} ModalSupport={this.ModalSupport.bind(this)}
                                        />
                    </View>
            </Container>
        )
    }
}

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,null)(mainPageDriver);


const style = StyleSheet.create({
    selectedBtn:{
        flex:1,
        backgroundColor:'#419aff',
        alignItems:'center',
        margin:10,
        borderRadius:10,
        paddingVertical:5
    },
    selectedText:{
        fontSize:18,
        fontFamily:'IRANSansMobile',
        color:'white'
    },
    unselectedBtn:{
        flex:1,
        backgroundColor:'#fff',
        borderColor:'#419aff',
        borderWidth:.5,
        alignItems:'center',
        margin:10,
        borderRadius:10,
        paddingVertical:5
    },
    unselectedText:{
        fontSize:18,
        fontFamily:'IRANSansMobile',
        color:'#419aff'
    },
    view_containerModalSignUp: {
        height: height * .80,
        width: width,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
        justifyContent: 'center', alignItems: 'center',
        paddingHorizontal: width * .05
    },
    styleModalSignUpDriver: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
        padding: 0,
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    view_add_bottom_center: {
        borderWidth: width * .02,
        borderColor: '#419aff40',
        width: width * .18,
        height: width * .18,
        borderRadius: width * .09,
        justifyContent: 'center',
        alignItems: 'center',

    },
    view_icon_add_bottom_center: {
        justifyContent: 'center', alignItems: 'center', backgroundColor: '#419aff',
        width: width * .12, height: width * .12,
        borderRadius: width * .06,
        // position: 'absolute', bottom: height * .07,
        // zIndex:1
    },
    view_txt_placeholder: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        width: width * .9
    },
    txt_count_fix: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#222222',
        marginBottom: height * .01
    },
    btnSignUpModal: {
        width:width*.6,
        paddingVertical:7,
        marginBottom:10,
        backgroundColor: '#2395ff',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:5
    },
    txt_btn_login: {
        fontFamily:'IRANSansMobile',
        fontSize:17,
        color: '#ffffff'
    },
    cancelRequest: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#2395ff',
        marginTop: height * .01,
        marginBottom: height * .01
    },
    view_txt_address: {
        marginBottom: height * .01,
        backgroundColor: '#f2f5fa',
        borderRadius: width * .02,
        width: width * .90,
        height: height * .08,
        justifyContent: 'center',
        alignItems:'flex-end',
        paddingRight:10
    },
    view_txt_countsell:{
        marginBottom: 10,
        borderRadius: width * .01,
        minWidth: width * .3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    view_txt_count: {
        backgroundColor: '#2395ff',
        borderRadius: width * .01,
        paddingHorizontal:10,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txt_request_name: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#222222',
        marginTop: height * .03,
        marginBottom: height * .04
    },
    border_underline: {borderWidth: 0.5, borderColor: '#e3e3e3', width: "90%"},
    txt_showAll:{
        fontFamily:'IRANSansMobile',
        backgroundColor: '#f8fbff',
        color: '#4a4b4d',
        fontSize: width / 35,
        paddingHorizontal: width * .05,
        paddingVertical: height * .01 + 2,
        borderRadius: width * .10
    },
    view_txt_address1: {
        marginBottom: height * .01,
        backgroundColor: '#f2f5fa',
        borderRadius: width * .02,
        width: width * .90,
        height: height * .08,
        paddingHorizontal: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    txt_count: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 28,
        color: '#fff',
    },

    txt_confirm: {color: '#222222'},
    btnLoginAccount: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#222222',
        marginBottom: height * .04
    },
    txt_request_name: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#222222',
        marginTop: height * .03,
        marginBottom: height * .04
    },
    btnLoginAccount1: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#222222',

        marginBottom: height * .01
    }, btnLoginAccount12: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 32,
        color: '#222222',

        marginBottom: height * .01
    },
    exitModalLogin:{
        alignSelf:'center'
    },
    img_exitModalLogin: {
        width: width * .18,
        height: width * .18,
        borderRadius: width * .09,
    },
    view_containerModalSignUp: {
        height: height * .80,
        width: width,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
        justifyContent: 'center', alignItems: 'center',
    },
    styleModalSignUpDriver: {
        flex:1,
        alignItems: 'center',
        margin: 0,
        padding: 0,
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    viewHeaderMenuFinancial:{
        width:width*.80,
        height:height*.08,
        flexDirection:'row',
        flex:3,
        borderBottomWidth: width*.001,
        borderBottomColor:'#999',
        // backgroundColor:'#000'
    },
    headerMenuFinancial:{flex:1,justifyContent:'center',alignItems:'center'},
    viewRightFieldFinancial:{
        width: width * .07,
        height: width * .07,
        borderRadius:width*.04,
        backgroundColor: '#f8fbff',
        justifyContent:'center',
        alignItems:'center'
    },
    success_Financial_Img:{
        width:width*.09,
        height:height*.09
    },
    viewCenterFieldFinancial:{
        width: width * .43,
        height: height * .15,
        // backgroundColor: '#d85a4e',
        justifyContent:'space-around',
        alignItems:'center',
        flexDirection:'row'
    },
    viewLeftFieldFinancial:{
        width: width * .30,
        height: height * .15,
        // backgroundColor: '#d4d86c',
        justifyContent:'center',
        alignItems:'center'
    },
    txt_showAllFinancial:{
        backgroundColor: '#f8fbff',
        color: '#20b3fe',
        fontSize: width / 30,
        paddingHorizontal: width * .05,
        paddingVertical: height * .01 + 2,
        borderRadius: width * .10
    },
    viewContainerFieldFinancial:{
        width: width * .90,
        height: height * .08,
        marginTop:height*.01,
        // backgroundColor: '#6941d8',
        flexDirection:'row',
        paddingRight:width*.05,
        justifyContent:'flex-end',
        alignItems:'center',

    },
    view_bottom_Financial: {
        // flex: 1,
        width: width * .90,
        height: height * .52,
        // backgroundColor: '#6941d8'
    },
    viewTop_bottomFinancial: {
        // backgroundColor: '#17f0e5',
        width: width * .80,
        height: height * .04,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txt_topFinancial: {fontFamily: 'IRANSansMobile', fontSize: width / 30, color: '#000'},
    viewTop_centerFinancial: {
        // backgroundColor: '#17f0e5',
        width: width * .80,
        height: height * .04,
        justifyContent: 'center',
        alignItems: 'center'
    },
    tik_imgTopFinancial: {width: width * .13, height: height * .13, margin: 0, padding: 0},
    viewTop_topFinancial: {
        // backgroundColor: '#17f0e5',
        width: width * .80,
        height: height * .07,
        justifyContent: 'center',
        alignItems: 'center'
    },
    view_top_Financial: {
        width: width * .80,
        height: height * .23,
        // backgroundColor: '#fffe7b',
        justifyContent: 'flex-start', alignItems: 'center'
    },
    viewContainerFinancial: {
        height: height * .87,
        width: width * .90,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .12,
        borderTopRightRadius: width * .12,
        justifyContent: 'flex-start', alignItems: 'center',
        // paddingHorizontal: width * .05
    },
    ModalFinancial: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
        padding: 0,
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    txt_Bar: {fontSize: width / 25, marginVertical: height * .01},
    imgUserBar: {
        width: width * .26,
        height: width * .26,
        borderRadius: width * .13
    },
    view_imgBar: {
        width: width * .28,
        height: width * .28,
        borderWidth: width * .01,
        borderRadius: width * .14,
        borderColor: '#f7f8f8',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 2,
        // backgroundColor: '#000',
    },
    viewBar: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        borderRadius:60,
    },
    viewContainerBar: {
        paddingTop: width * .01,
        alignSelf:'center'
    },
    txt_searchBar: {color: '#999999', fontSize: width / 25, fontFamily: 'IRANSansMobile', textAlign: 'right'},
    view_InputBar: {width: width * .70, paddingHorizontal: width * .05},
    view_iconInputBar: {
        width: width * .25,
        paddingLeft: width * .05,
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row'
    },
    viewContainer_InputSearchBar: {
        // backgroundColor:'#2089ff',
        borderBottomColor: '#999',
        borderBottomWidth: width * .001,
        marginBottom: height * .02,
        flexDirection: 'row',
        borderTopLeftRadius: width * .10,
        borderTopRightRadius: width * .10, height: height * .08,
    },
    viewContainer_SearchBar: {
        height:"100%",
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .12,
        borderTopRightRadius: width * .12,
        
    },
    ModalSearchBar: {
        margin: 0,        
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    transection_img: {width: width * .08, height: height * .02,},
    menu_top_placeholder: {width: width * .16, height: height * .01,marginBottom:-5},
    menu_Bar_placeholder: {width: width * .23, height: height * .03},
    viewRequest_under: {
        justifyContent: 'center',
        alignItems: 'center',
        position:'absolute'
    },
    viewRequest_under2: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewRequest_under2Bar: {
        justifyContent: 'center',
        alignItems: 'center',
        // width: width * .27,
        // height: width * .12,
        // // borderRadius: width * .13,
        // backgroundColor: '#407dfe',
        // borderBottomRightRadius: width * .13,
        // borderBottomLeftRadius: width * .13
    },
    viewRequest_under1: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .19,
        height: height * .03,
    },
    viewRequest_under1Bar: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .27,
        height: height * .04,
        // backgroundColor:'#000'
    },
    img_placeholder: {width: width * .15, height: height * .05,},
    view_center_menu: {flex: 1, width: width * .90, flexDirection: 'row', justifyContent: 'space-around',},
    view_request: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .175,
        height:width * .175,
        borderRadius: width * .10,
        borderWidth: width * .001,
    },
    view_request2:{
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .26,
        height:width * .26,
        borderRadius: width * .10,
        borderWidth: width * .001,
    },
    view_requestSearchBar: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: width * .28,
        height: width * .28,
        alignSelf:'flex-end',
        marginVertical:height*.01,
        borderRadius: width * .14,
        borderWidth: width * .001,
        borderColor: '#4088fe'
    },
    view_btn_showAll: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#32ca1d',
        borderRadius: width * .13,
        padding:8,
        marginVertical:10,
        elevation:7
    },
    txt_view_top_header_menu: {fontSize: width / 30, color: "#000", fontFamily: 'IRANSansMobile'},
    txt_view_top_header_menu2: {fontSize: 10, color: "#000", fontFamily: 'IRANSansMobile'},
    txt_Financial_Price: {fontSize: width / 25, color: "#000", fontFamily: 'IRANSansMobile'},
    txt_view_Bar_header_menu2: {fontSize: width / 30, color: "#000", fontFamily: 'IRANSansMobile'},
    txt_view_top_header_menu3: {
        fontFamily: 'IRANSansMobile',
        fontSize:9,
        color: "white",
        textAlign:'center'
    },
    txt_view_Bar_header_menu3: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 30,
        color: "black",
        marginBottom: height * .01
    },
    txt_view_top_header_menu1: {fontSize: width / 35, color: '#fff', fontFamily: 'IRANSansMobile'},
    view_top_header_menu: {
        backgroundColor: '#f7f9fc',
        height: height * .06,
        width: width * .94,
        marginBottom:5,
        borderTopRightRadius: width * .12,
        borderTopLeftRadius: width * .12,
        justifyContent: 'center', alignItems: 'center'
    },
    view_top_header_menu1: {
        // backgroundColor: '#000',
        height: height * .06,
        width: width * .90,
        borderBottomRightRadius: width * .12,
        borderBottomLeftRadius: width * .12,
        justifyContent: 'center', alignItems: 'center'
    },
    tik_img: {width: width * .18, height: width * .18},
    tik_img1: {width: width * .25, height: height * .25},
    view_home_container: {},
    img_container_home: {
        position: 'absolute',
        zIndex: 0,
    },
    view_top_header: {
        paddingHorizontal: width * .05,
        // backgroundColor:'#000',
        width: width
    },
    txt_top_header: {
        color: '#fff',
        textAlign: 'center',
        fontSize: width / 25
    },
    view_menu_top: {
        width: width * .10,
        height: width * .10,
        backgroundColor: '#4088fe',
        borderRadius: width * .05,
        justifyContent: 'center',
        alignItems: 'center'
    },
    view_menu_top1: {
        width: width * .10,
        height: width * .10,
        backgroundColor: '#32ca1d',
        borderRadius: width * .05,
        justifyContent: 'center',
        alignItems: 'center'
    },
    menu_top: {width: width * .06, height: height * .03,},
    logo_top: {width: width * .11, height: width * .13},
    img_top_home: {
        width: width,
        height: height / 3,
        position: 'absolute',
        zIndex: 0,
        top: 0
    },
    dot_img: {
        width: width * .90,
        height: height * .16,
        flexDirection: 'column-reverse'
    },
    view_top_home: {
        width: width,
        height: height * .40,
        zIndex: 1,
        position: 'absolute',
        top: 0,
        alignItems: 'center'
    },
    txt_menu: {fontFamily: 'IRANSansMobile', fontSize: width / 30,marginTop:5, color: '#000'},
    txt_menu1: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 30,
        color: '#000',
        width: width * .50,
        marginRight: width * .03,
        textAlign: 'center'
    },
    img_home_tiksan: {width: width * .37, height: width * .37},
    txt_header_home: {fontSize: width / 25, color: '#000',},
    txt_header_home1: {color: '#fac000', fontWeight: 'bold'},
    view_center_home: {
        width: width,
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row'
    }, view_center_home1: {
        width: width,
        height: height * .20,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1,
        position: 'absolute',
        top: height * .47,
        flexDirection: 'row'
    },
    view_underTopMenu_home_img: {
        width: width / 2.5,
        height: height * .28,
        alignItems: 'center',
        justifyContent: 'center'
    },
    view_underTopMenu_home_txt: {
        width: width / 2.4,
        marginLeft: width * .03,
        alignItems: 'center',
        justifyContent: 'center'
    },
    view_top_menu: {
        width: width, position: 'absolute', zIndex: 1
    },
    view_bottom_home: {zIndex: 1, position: 'absolute', bottom: 0},
    view_menu_home1: {
        flexDirection: 'row',
        width: width *.95,
        height: height * .20,
        borderRadius: width * .12,
        backgroundColor: '#fbfcfd',
        borderColor: '#e1eafc',
        borderWidth: width * .004,
        justifyContent: 'space-around',
        alignItems: 'center',
        alignSelf:'center'
    },
    view_menu_home12: {
        // flexDirection:'row',
        borderRadius: width * .12,
        marginHorizontal:10,
        backgroundColor: '#fff',
        borderColor: '#e1eafc',
        borderWidth: width * .004,
        // marginHorizontal: width * .04,
        marginVertical: height * .01,
        justifyContent: 'space-around',
        alignItems: 'center',
        //paddingHorizontal: width * .04,
        marginTop: height *.02,
    },
    view_menu_home: {
        width: width / 2.2,
        height: width / 2.5,
        borderRadius: width / 10,
        backgroundColor: '#fff',
        marginHorizontal: width * .04,
        marginVertical: height * .01,
        justifyContent: 'center',
        alignItems: 'center',
    }
});