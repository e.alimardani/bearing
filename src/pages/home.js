import React, {Component} from 'react';
import {View, Animated,FlatList, StatusBar, Image, Dimensions, Linking, Text, TouchableOpacity,StyleSheet,ActivityIndicator
    ,Platform,Slider} from 'react-native';
import BottomNavigate from '../assets/components/bottomNavigate';
import Icon from "react-native-vector-icons/Feather";
import IconFooter from 'react-native-vector-icons/FontAwesome5';
import IconF from "react-native-vector-icons/FontAwesome5";
import IconM from "react-native-vector-icons/MaterialIcons";
import {Header, Body, Button, Left, Right, Footer, Content, Container, Form, Item, Input,FooterTab} from "native-base";
import {Actions} from "react-native-router-flux";
import Modal from "react-native-modal";
import style from '../assets/styles/home_css';
import {connect} from "react-redux";
import {setUser} from '../Redux/Actions/index';
import PushNotif from './PushNotificationController';
import Helper from '../assets/components/Helper'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
class home extends Component {

    state = {
        isModalSearchDriver: false,
        isModalSupport: false,
        isModalRequest:false,
        searchDriver: '',
        isModalApproved: false,
        isModalWaiting:false,
        displayIconArrowBack:'none',
        display_Footer:'flex',
        cargo_type:'',
        name:'',
        title:'',
        price:'',
        weight:0,
        origin:'',
        originAdr:'',
        destination:'',
        destinationAdr:'',
        distance:null,
        description:'',
        isModalCar_type:false,
        loading_carType:false,
        carSelectedName:'',
        carSelectedId:'',
        carTypes:[],
        carTypesById:[],
        loading_carTypeById:false,
        isModalCar_typeById:false,
        userData:[],
        message:'',
        messageRender:false,
        LoadingaddCarGo:false,
        LoadingApproved:false,
        AllProduct:[],
        LoadingWaiting:false,
        isModalRequestdata:false,
        removeAnim: new Animated.Value(0.8),
        DoneAnim: new Animated.Value(0),
        off:false,
        isModalDriverSee:false,
        data_Driver:[],
        weight_selected:'',
        price_selected:'',
        destination_selected:'',
        origin_selected:'',
        origin_address_selected:'',
        destination_address_selected:'',
        DriverToken:'',
        userDataDriver:[],
        cat_name_selected:'',
        driverPrice:'',
        id_Product_selected:'',

        isModalProvincelist:false,
        dataUserProvince:[],
        LoadingProvince:false,
        Province_selected_name:'',
        Province_selected:'',
        dataUserProvinceById:[],
        LoadingProvinceById:false,
        ProvinceById_selected:'',
        isModalProvincelistById:false,

        isModalProvincelistDst:false,
        isModalProvincelistByIdDst:false,
        Province_selectedDst:'',
        Province_selected_nameDst:'',
        ProvinceById_selectedDst:'',
        arrayholder :[],
        arrayholderCarType:[],
        Done:false,
        LoadingAddData:false,
        ProductType:[],
        LoadingProductType:false,
        isModalProductType:false,
        carSelectedId_second:'',
        carSelectedName_second:'',
        pakage_type_selected:'',
        description_selected:'',
        Province_selectedById_nameDst:'',
        Province_selectedById_name:'',
        isModalDriver:false,
        AllProductAccept:[],
        AllProductWaiting:[],
        Loading:false,
        LoadingPlate:true,
        LoadingCancelProduct:false,
        Province_selectedById_nameDst_id:'',
        Province_selectedById_name_id:''

    }
    constructor(props) {
        super(props);
    }
    Grow(){
        Animated.timing(this.state.removeAnim,{
            toValue:1.3,
            duration:800,
            useNativeDriver:true
        }).start();
    }
    Less(){
        Animated.timing(this.state.removeAnim,{
            toValue:1,
            duration:800,
            useNativeDriver:true
        }).start();
    }
    componentDidMount(){
        this.GetUser()
        setInterval(() => {
            if(!this.state.off){
                this.Grow(),
                this.setState({off:true})
            }
            else{
                this.Less()
                this.setState({off:false})
            }
        }, 800);
    }

    async getProductType() {
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            console.log(formData)
            let response = await fetch(this.props.global.baseApiUrl + '/product/get_cargo_type_list',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)

                if (json.success === true) {
                    this.setState({ProductType:json.data,arrayholderCarType:json.data,LoadingProductType:false})
                    
                    //this.addInRedux(json)

                } else {

                    this.setMessageServerSignUpUser(json);
                }
                

        } catch (error) {
            console.log(error)         
        }
    };

    renderProductType({item}){
        return(
            <TouchableOpacity style={{alignItems:'center',justifyContent:'center',paddingVertical:10,borderBottomColor:'#aaaa',borderBottomWidth:0.5}}
                onPress={()=>this.setState({
                    isModalProductType:false,
                    cargo_type:item.name,
                    })}>
                <Text style={{fontFamily:'IRANSansMobile'}}>{item.name}</Text>
            </TouchableOpacity>
            )
    }

    async GetUser(){
        try {  

            let formData = new FormData();

            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            
            let response = await fetch(this.props.global.baseApiUrl + "/user/get",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                
                this.setState({userData:json.data})
            
            } catch (error) {
                console.log(error)
            }
    }

    async GetDriver(){
        try {  
            let formData = new FormData();

            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.state.DriverToken);
            console.log(formData)
            let response = await fetch(this.props.global.baseApiUrl + "/user/get",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                console.log(json)
                this.setState({userDataDriver:json.data,LoadingPlate:false})
            
            } catch (error) {
                console.log(error)
            }
    }

    setModalTrueSearchDriver(tru) {
        this.setState({isModalSearchDriver: tru});
    }

    ModalSupport(tru) {
        this.setState({isModalSupport: tru});
    }

    ModalRequest(tru) {
        this.setState({isModalRequest: tru});
    }
    async GetPrice(){
        if(this.state.carSelectedId == 1){
            try {

                let formData = new FormData();
    
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("car_type_id", this.state.carSelectedId_second);
                formData.append("weight", parseInt(this.state.weight));
                formData.append("distance", this.state.distance);

                let response = await fetch(this.props.global.baseApiUrl + "/product/getprice",
                    {
                        method: "POST",
                        body: formData
                    });
                    let json = await response.json();
                    if(json.success){
                        this.setState({price:json.data.price ,LoadingAddData:false},()=>this.setState({isModalRequestdata:false ,isModalRequest:true , message:''}))
                    }
                    else{
                        console.log('ertrororr')
                    }
                    //console.log(json.data)
                    this.setState({LoadingAddData:false})
                
                } catch (error) {
                    console.log(error)
                }
        }
        else{
            this.setState({LoadingAddData:false},()=>this.setState({isModalRequestdata:false ,isModalRequest:true}))
        }
    }

    async getCarType(){
        try {

            let formData = new FormData();

            formData.append("admintoken", "2dab06cddd5a3d6e6a40b874a457c25de769963bf4485c3e004b570bf74a4118");

            let response = await fetch(this.props.global.baseApiUrl + "/product/get_carCategory_list",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                //console.log(response)

                this.setState({carTypes:json.data,loading_carType:false})
            
            } catch (error) {
                console.log(error)
            }
    }

    async getCarTypeById(){
        try {

            let formData = new FormData();

            formData.append("admintoken", "2dab06cddd5a3d6e6a40b874a457c25de769963bf4485c3e004b570bf74a4118");
            formData.append("car_category_id", this.state.carSelectedId);

            let response = await fetch(this.props.global.baseApiUrl + "/product/get_car_by_carCategory_id",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                //console.log(response)

                this.setState({carTypesById:json.data,loading_carTypeById:false})
            
            } catch (error) {
                console.log(error)
            }
    }

    clickBackArrowApproved = (st) => {
        this.setState({displayIconArrowBack:'none',isModalApproved: false})
        // alert(st)
    }
    renderCarType({item}){
        return (
            <TouchableOpacity style={{padding:15,borderBottomColor:'#eaeaea',borderBottomWidth:1}}
                onPress={()=>this.setState({carSelectedId:item.id , carSelectedName:item.name , isModalCar_type:false,isModalCar_typeById:true,loading_carTypeById:true},()=>this.getCarTypeById())}>
                <Text style={{fontFamily:'IRANSansMobile',fontSize:17,textAlign:'center',paddingRight:10}}>{item.name}</Text>
            </TouchableOpacity>
        );
    }
    renderCarTypeById({item}){
        return (
            <TouchableOpacity style={{padding:15,borderBottomColor:'#eaeaea',borderBottomWidth:1}}
                onPress={()=>this.setState({carSelectedId_second:item.id , carSelectedName_second:item.name ,isModalCar_typeById:false,})}>
                <Text style={{fontFamily:'IRANSansMobile',fontSize:17,textAlign:'center',paddingRight:10}}>{item.name}</Text>
            </TouchableOpacity>
        );
    }
    CheckModal(){
        if(this.state.carSelectedName === ''){
            this.setState({message:'لطفا نوع خودرو را انتخاب کنید'})

        }
        else if(this.state.cargo_type === ''){
            this.setState({message:'لطفا نوع بار را انتخاب کنید'})

        }
        else if(this.state.PackageTypeName === ''){
            this.setState({message:'لطفا نوع بسته بندی را انتخاب کنید'})

        }
        else if(this.state.price === '' && this.state.carSelectedId != 1 ){
            this.setState({message:'لطفا هزینه را تعیین کنید'})

        }
        // else if(this.state.distance === null){
        //     this.setState({message:'لطفا مسافت را تعیین کنید'})

        // }
        else if(this.state.weight === 0){
            this.setState({message:'لطفا وزن را تعیین کنید'})

        }
        else if(this.state.Province_selected_name === ''){
            this.setState({message:'لطفا یک مبدا انتخاب کنید'})

        }
        else if(this.state.originAdr === ''){
            this.setState({message:'لطفا ادرس مبدا انتخاب کنید'})

        }
        else if(this.state.Province_selected_nameDst === ''){
            this.setState({message:'لطفا یک مقصد انتخاب کنید'})

        }
        else if(this.state.destinationAdr === ''){
            this.setState({message:'لطفا ادرس مقصد انتخاب کنید'})

        }
        else{
            this.setState({LoadingAddData:true , message:''})
            this.GetPrice()
        }
    }

    clearSates(){
        this.setState({
            title:'',
            cargo_type:'',
            carSelectedName:'',
            carSelectedId:'',
            weight:0,
            origin:'',
            destination:'',
            price:'',
            destinationAdr:'',
            originAdr:'',
            distance:null,
            Province_selected_name:'',
            Province_selected_nameDst:'',
            Province_selectedById_name:'',
            Province_selectedById_nameDst:'',
            ProvinceById_selected:'',
            ProvinceById_selectedDst:'',
            LoadingPackageType:false,
            packageType:[],
            PackageTypeName:'',
            description:'',
            carSelectedId_second:'',
            carSelectedId_second:'',
            carSelectedName_second:'',
            carSelectedName:'',            
        })
    }

    async addCarGo(){
        try {
            this.setState({LoadingaddCarGo:true})
            let formData = new FormData();

            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("cargo_type", this.state.cargo_type);
            formData.append("car_type_id", parseInt(this.state.carSelectedId_second));
            formData.append("weight", parseInt(this.state.weight));
            formData.append("origin", this.state.Province_selectedById_name);
            formData.append("destination", this.state.Province_selectedById_nameDst);
            formData.append("origin_address", this.state.originAdr);
            formData.append("destination_address", this.state.destinationAdr);
            formData.append("price", this.state.price);
            formData.append("package_type", this.state.PackageTypeName);
            formData.append("description", this.state.description);
            formData.append("title", this.state.title);
            formData.append("distance", parseInt(this.state.distance));

            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + "/product/addproduct",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
               
                    console.log(json)
                    this.setState({LoadingaddCarGo:false , isModalRequest:false,Done:true}  , ()=> this.GrowDone() , this.clearSates())
                
            
            } catch (error) {
                console.log(error)
            }
    }
    async getAll(){
        try {
            this.setState({LoadingaddCarGo:true,LoadingApproved:true})
            let formData = new FormData();

            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + "/product/all",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                if(json.success){
                    this.setState({LoadingApproved:false,LoadingWaiting:false,LoadingaddCarGo:false,AllProduct:json.data})
                    this.state.AllProduct.forEach(item => {
                        //console.log("item : "+item)
                        // console.log(item)
                        if(item.drivers_accepted !== null && this.props.user.id == item.customer_id){
                            this.setState({AllProductAccept:item})
                            //console.log(item)
                        }
                        if(item.drivers_accepted === null && this.props.user.id == item.customer_id){
                            //console.log(item)
                            this.setState({AllProductWaiting:item})
                            //console.log(this.state.AllProductWaiting)
                        }
                    });
                    
                }
                console.log(this.state.AllProductWaiting)

            } catch (error) {
                console.log(error)
            }
    }

    async getAccepted(){
        try {
            this.setState({LoadingApproved:true})
            let formData = new FormData();

            formData.append("admintoken", this.props.global.adminToken);
            formData.append("customer_id", this.props.user.id);

            let response = await fetch(this.props.global.baseApiUrl + "/product/get_accepted",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                if(json.success){
                    this.setState({AllProductAccept:json.data,LoadingApproved:false})
                }
                else{
                    this.setState({LoadingApproved:false,AllProductAccept:[]})
                }
                //console.log(this.state.AllProductWaiting)

            } catch (error) {
                console.log(error)
            }
    }

    async getWaiting(){
        try {
            this.setState({LoadingWaiting:true})
            let formData = new FormData();

            formData.append("admintoken", this.props.global.adminToken);
            formData.append("customer_id", this.props.user.id);

            let response = await fetch(this.props.global.baseApiUrl + "/product/get_waiting",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                if(json.success){
                    this.setState({AllProductWaiting:json.data,LoadingWaiting:false})
                }
                else{
                    this.setState({LoadingWaiting:false,AllProductWaiting:[]})
                }
                //console.log(this.state.AllProductWaiting)

            } catch (error) {
                console.log(error)
            }
    }

    renderAllProductAccept({item}){
            return (
                <View style={style.viewContainerFieldApproved}>
                    <TouchableOpacity
                        onPress={()=>this.setState({
                            isModalDriverList:true,
                            data_Driver:item.drivers_accepted ,
                            price_selected:item.price,
                            weight_selected:item.weight,
                            destination_selected:item.destination,
                            origin_selected:item.origin,
                            origin_address_selected:item.origin_address,
                            destination_address_selected:item.destination_address,
                            carSelectedName:item.car_name,
                            carSelectedName_second:item.car_category_name,
                            pakage_type_selected:item.package_type,
                            distance_Selected:item.distance,
                            description_selected:item.description,
                            id_Product_selected:item.id,
                            cargo_type_selected:item.cargo_type
                            })} style={style.viewLeftFieldApproved}>
                            <Text style={style.txt_showAll}>لیست راننده ها</Text>
                    </TouchableOpacity>
                    <View style={style.viewCenterFieldApproved}>
                        <Image source={require('../assets/images/success.png')}
                            style={style.success_Approved_Img}
                            resizeMode={'contain'}/>
                        <Text style={style.txt_view_top_header_menu2}>وضعیت درخواست:</Text>
                    </View>
                    <View
                        style={styles.viewBar}>
                        <View style={{height:100 , width:100,  borderRadius:50,padding:5,marginVertical:5,borderColor:'#2395ff',borderWidth:1,alignItems:'center',justifyContent:'center'}}>
                         <Text style={[style.txt_view_top_header_menu2,{fontSize:8,textAlign:'center',marginTop:2}]}>{item.origin}</Text>
                            <Image source={require('../assets/images/map_small.png')}
                                style={[style.menu_top_placeholder,{width: width * .2}]}
                                resizeMode={'contain'}/>
                            <Text style={[style.txt_view_top_header_menu2,{fontSize:8,textAlign:'center',marginTop:2}]}>{item.destination}</Text>
                            <Text style={[style.txt_view_top_header_menu3,{marginTop:2,fontSize:10,color:'#2395ff',paddingHorizontal:5}]}>{Helper.ToPersianNumber(item.price)} تومان</Text>
                        </View>
                    </View>
                </View>
            );
    }

    renderPackageType({item}){
        return (
            <TouchableOpacity style={{padding:15,borderBottomColor:'#eaeaea',borderBottomWidth:1}}
                onPress={()=>this.setState({PackageTypeName:item.name ,isModalPackageType:false,})}>
                <Text style={{fontFamily:'IRANSansMobile',fontSize:17,textAlign:'center',paddingRight:10}}>{item.name}</Text>
            </TouchableOpacity>
        );
    }

    async getPackageType(){
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            console.log(formData)
            let response = await fetch(this.props.global.baseApiUrl + '/product/get_package_type_list',
                {
                    method: "POST",
                    body: formData,
                });
                let json = await response.json();
                console.log(json)

                if (json.success) {
                    this.setState({packageType:json.data,LoadingPackageType:false})
                    
                    //this.addInRedux(json)

                } else {

                }
                

        } catch (error) {
            console.log(error)         
        }
    }

    renderAllWaiting({item}){
            return (
                <View style={style.viewContainerFieldApproved}>
                    <TouchableOpacity
                        onPress={()=>this.setState({
                            isModalDriver:true,
                            price_selected:item.price,
                            weight_selected:item.weight,
                            destination_selected:item.destination,
                            origin_selected:item.origin,
                            origin_address_selected:item.origin_address,
                            destination_address_selected:item.destination_address,
                            carSelectedName:item.car_name,
                            distance_Selected:item.distance,
                            carSelectedName_second:item.car_category_name,
                            pakage_type_selected:item.package_type,
                            description_selected:item.description,
                            id_Product_selected:item.id,
                            cargo_type_selected:item.cargo_type
                            })}
                        style={style.viewLeftFieldApproved}>
                        <Text style={style.txt_showAll}>مشاهده جزئیات</Text>
                    </TouchableOpacity>
                    <View style={style.viewCenterFieldApproved}>
                        <Image source={require('../assets/images/success.png')}
                            style={style.success_Approved_Img}
                            resizeMode={'contain'}/>
                        <Text style={style.txt_view_top_header_menu2}>وضعیت درخواست:</Text>
                    </View>
                    <View
                        style={styles.viewBar}>
                        <View style={{height:100 , width:100,  borderRadius:50,padding:5,marginVertical:5,borderColor:'#2395ff',borderWidth:1,alignItems:'center',justifyContent:'center'}}>
                         <Text style={[style.txt_view_top_header_menu2,{fontSize:8,textAlign:'center',marginTop:2}]}>{item.origin}</Text>
                            <Image source={require('../assets/images/map_small.png')}
                                style={[style.menu_top_placeholder,{width: width * .2}]}
                                resizeMode={'contain'}/>
                            <Text style={[style.txt_view_top_header_menu2,{fontSize:8,textAlign:'center',marginTop:2}]}>{item.destination}</Text>
                            <Text style={[style.txt_view_top_header_menu3,{marginTop:2,fontSize:10,color:'#2395ff',paddingHorizontal:5}]}>{Helper.ToPersianNumber(item.price)} تومان</Text>
                        </View>
                    </View>
                </View>
            );
    }

    async customerAccept(){
        this.setState({LoadingAccept:true})
        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("product_id", this.state.id_Product_selected);
            formData.append("driver_id", this.state.userDataDriver.id);

            let response = await fetch(this.props.global.baseApiUrl + "/product/customeraccept",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                    console.log(json)
                    if(json.status){
                        this.setState({isModalDriverSee:false,Done:true,LoadingAccept:false},()=>this.GrowDone())
                    }
            
            } catch (error) {
                console.log(error)
            }
    }

    renderDrivers({item}){
            return (
                <View style={style.viewContainerFieldApproved}>
                    <TouchableOpacity onPress={()=>this.setState({isModalDriverSee:true , DriverToken:item.driver_token ,driverPrice: item.accepted_driver_price},()=>this.GetDriver())} style={style.viewLeftFieldApproved}>
                        <Text style={style.txt_showAll}>مشاهده جزئیات</Text>
                    </TouchableOpacity>
                    <View style={style.viewCenterFieldApproved}>
                        <Image source={require('../assets/images/success.png')}
                            style={style.success_Approved_Img}
                            resizeMode={'contain'}/>
                        <Text style={style.txt_view_top_header_menu2}>وضعیت درخواست:</Text>
                    </View>
                    {/* <View style={style.viewCenterFieldApproved}>
                        <Text style={{fontFamily:'IRANSansMobile',color:'black',fontSize:12}}>{(item.accepted_driver_price == "") ? Helper.ToPersianNumber(this.state.price_selected) : Helper.ToPersianNumber(item.accepted_driver_price)} تومان</Text>
                    </View> */}
                </View>
            );
    }

    async Provincelist(){
        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + "/user/get_province_list",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                console.log(json)
                this.setState({dataUserProvince:json.data ,arrayholder:json.data , LoadingProvince:false})
            } catch (error) {
                console.log(error)
            }
    }

    async ProvinceByIdList(){
        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("province_id", this.state.Province_selected);

            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + "/user/get_city_by_province_id",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                console.log(json)
                this.setState({dataUserProvinceById:json.data ,arrayholder:json.data , LoadingProvinceById:false})
            } catch (error) {
                console.log(error)
            }
    }

    async ProvinceByIdListDst(){
        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("province_id", this.state.Province_selectedDst);

            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + "/user/get_city_by_province_id",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                console.log(json)
                this.setState({dataUserProvinceById:json.data ,arrayholder:json.data , LoadingProvinceById:false})
            } catch (error) {
                console.log(error)
            }
    }

    renderProvincelistByIdDst({item}){
        return(
            <TouchableOpacity style={{alignItems:'center',justifyContent:'center',paddingVertical:10,borderBottomColor:'#aaaa',borderBottomWidth:0.5}}
                onPress={()=>this.setState({
                    isModalProvincelistByIdDst:false, 
                    Province_selectedById_nameDst:item.name,
                    Province_selectedById_nameDst_id:item.id,
                    },()=>this.GetDistance())}>
                <Text style={{fontFamily:'IRANSansMobile'}}>{item.name}</Text>
            </TouchableOpacity>
        )
    }

    renderProvincelistById({item}){
        return(
            <TouchableOpacity style={{alignItems:'center',justifyContent:'center',paddingVertical:10,borderBottomColor:'#aaaa',borderBottomWidth:0.5}}
                onPress={()=>this.setState({
                    isModalProvincelistById:false, 
                    Province_selectedById_name:item.name,
                    Province_selectedById_name_id:item.id,
                    })}>
                <Text style={{fontFamily:'IRANSansMobile'}}>{item.name}</Text>
            </TouchableOpacity>
        )
    }

    renderProvincelist({item}){
        return(
            <TouchableOpacity style={{alignItems:'center',justifyContent:'center',paddingVertical:10,borderBottomColor:'#aaaa',borderBottomWidth:0.5}}
                onPress={()=>this.setState({
                    Province_selected:item.id,
                    isModalProvincelist:false, 
                    Province_selected_name:item.name,
                    isModalProvincelistById:true,
                    LoadingProvinceById:true
                    },()=>this.ProvinceByIdList())}>
                <Text style={{fontFamily:'IRANSansMobile'}}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    renderProvincelistDst({item}){
        return(
            <TouchableOpacity style={{alignItems:'center',justifyContent:'center',paddingVertical:10,borderBottomColor:'#aaaa',borderBottomWidth:0.5}}
                onPress={()=>this.setState({
                    Province_selectedDst:item.id,
                    isModalProvincelistDst:false, 
                    Province_selected_nameDst:item.name,
                    isModalProvincelistByIdDst:true,
                    LoadingProvinceById:true
                    },()=>this.ProvinceByIdListDst())}>
                <Text style={{fontFamily:'IRANSansMobile'}}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    searchFilterCarType(text){
        const newData = this.state.arrayholderCarType.filter(item => {      
            const itemData = `${item.name.toUpperCase()}`;
            
             const textData = text.toUpperCase();
              
             return itemData.indexOf(textData) > -1;    
          });
          
          this.setState({ ProductType: newData });  
    }
    searchFilterFunction (text) {    
        const newData = this.state.arrayholder.filter(item => {      
          const itemData = `${item.name.toUpperCase()}`;
          
           const textData = text.toUpperCase();
            
           return itemData.indexOf(textData) > -1;    
        });
        
        this.setState({ dataUserProvince: newData });  
    };
    searchFilterFunctionById (text) {    
        const newData = this.state.arrayholder.filter(item => {      
          const itemData = `${item.name.toUpperCase()}`;
          
           const textData = text.toUpperCase();
            
           return itemData.indexOf(textData) > -1;    
        });
        
        this.setState({ dataUserProvinceById: newData });  
    };
    async GetDistance(){
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("from", this.state.Province_selectedById_name_id);
            formData.append("to", this.state.Province_selectedById_nameDst_id);

            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + "/user/get_distance",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                console.log(json)
                if(json.success){
                    this.setState({distance:json.data})
                }
                
            } catch (error) {
                console.log(error)
            }
    }
    GrowDone(){
        Animated.timing(this.state.DoneAnim,{
            toValue:1,
            duration:800,
            useNativeDriver:true
        }).start();
    }
    handleEmpty() {
        if(this.state.LoadingApproved)
        {
            return(
                <ActivityIndicator  color="#2395ff" size='large' />
            );
        }
        else
        {
            return(
                <Text style={{fontFamily:'IRANSansMobile',textAlign:'center'}}>درخواستی وجود ندارد</Text>
            );
        }
    }

    handleEmptyWaiting() {
        if(this.state.LoadingWaiting)
        {
            return(
                <ActivityIndicator  color="#2395ff" size='large' />
            );
        }
        else
        {
            return(
                <Text style={{fontFamily:'IRANSansMobile',textAlign:'center'}}>درخواستی وجود ندارد</Text>
            );
        }
    }
    async cancelProduct(){
        this.setState({LoadingCancelProduct:true})
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("cargo_id", this.state.id_Product_selected);

            console.log(formData)

            let response = await fetch(this.props.global.baseApiUrl + "/product/deletecargo",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                console.log(json)
                if(json.status){
                    this.setState({isModalDriver:false,Done:true,LoadingCancelProduct:false,carSelectedName_second:'',carSelectedName:''},()=>{this.getWaiting(),this.GrowDone()})
                }
                
            } catch (error) {
                console.log(error)
            }
    }
    async CustomerCancel(){
        this.setState({LoadingAccept:true})
        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("product_id", this.state.id_Product_selected);
            formData.append("driver_id", this.state.userDataDriver.id);

            let response = await fetch(this.props.global.baseApiUrl + "/product/customerCancel",
                {
                    method: "POST",
                    body: formData
                });
                let json = await response.json();
                    console.log(json)
                    if(json.status){
                        this.setState({isModalDriverSee:false,Done:true,carSelectedName_second:'',carSelectedName:'',isModalDriverList:false},()=>{this.getAccepted(),this.GrowDone()})
                    }
            
            } catch (error) {
                console.log(error)
            }
    }
    render() {
        return (
            <Container style={{backgroundColor: '#000', flex: 1}}>
                <PushNotif/>
                <Image source={require('../assets/images/background.png')} style={style.img_container_home} resizeMode={'cover'}/>
                <View style={{backgroundColor:'#aaa', position:'absolute',top:0,height:'100%',width:'100%',opacity:.12}}/>
                <Image source={require('../assets/images/trans_blue1.png')} style={style.img_top_home}/>

                <Header transparent style={style.view_top_header}>
                    <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                    <Left style={{flex: 1,flexDirection:'row'}}>
                        <Button transparent style={{display:this.state.displayIconArrowBack}}>
                            <TouchableOpacity activeOpacity={.5} onPress={this.clickBackArrowApproved}>
                                <IconM name="keyboard-backspace" color={"#ffffff"} size={width * .08}/>
                            </TouchableOpacity>
                        </Button>
                        <Button transparent>
                            <TouchableOpacity activeOpacity={.5}>
                                <IconF name="bell" color={"#ffffff"} size={width * .06}/>
                            </TouchableOpacity>
                        </Button>
                    </Left>
                    <Body style={{flex: 1, alignItems: 'center'}}>
                        <Image source={require('../assets/images/logo_top1.png')} style={style.logo_top}
                               resizeMode={'cover'}/>
                    </Body>
                    <Right style={{flex: 1}}>
                        <Button transparent>
                            <TouchableOpacity activeOpacity={.5} onPress={() => Actions.drawerOpen()}>
                                <Image source={require('../assets/images/menu.png')} style={style.menu_top}
                                       resizeMode={'cover'}/>
                            </TouchableOpacity>
                        </Button>
                    </Right>
                </Header>

                <Modal isVisible={this.state.isModalSupport}
                        backdropOpacity={0.70} 
                        onBackdropPress={() => this.setState({isModalSupport: !this.state.isModalSupport})}
                        onBackButtonPress={()=> this.setState({isModalSupport: !this.state.isModalSupport})}
                        style={style.ModalSearchDriver}>
                    <View style={style.viewContainerSupport}>
                        <View style={[style.view_top_support, {height: height * .08}]}>
                            <Text style={style.txt_top_support}>تماس با پشتیبانی</Text>
                        </View>
                        <View style={[style.view_top_support, {height: height * .09}]}>
                            <Text style={style.txt_center_support}>{Helper.ToPersianNumber('09200501200')}</Text>
                        </View>
                        <View style={[style.view_bottom_support]}>
                            <TouchableOpacity activeOpacity={.7} onPress={() => this.setState({isModalSupport: false})}>
                                <View style={style.btnSupport1}>
                                    <Text style={style.txt_bottom_support1}>لغو</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={.7} onPress={() => Linking.openURL('tel:09200501200')}>
                                <View style={style.btnSupport}>
                                    <Text style={style.txt_bottom_support}>تماس</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                
                <Content>
                    <Image 
                        source={require('../assets/images/logo.jpg')}
                        style={{borderRadius:width*.3,width:width*.6,height:width*.6,alignSelf:'center'}}
                        resizeMode={'cover'}
                    />
                    <View style={style.view_center_home}>
                        <View>
                            <TouchableOpacity onPress={()=>this.setState({isModalWaiting:true,LoadingWaiting:true},()=>this.getWaiting())}>
                                <View style={style.view_menu_home}>
                                    <Image source={require('../assets/images/clock.png')} style={style.tik_img}
                                        resizeMode={'contain'}/>
                                    <Text style={style.txt_menu}>درخواست های در انتظار تایید</Text>
                                    <Image source={require('../assets/images/transaction_dot1.png')}
                                        style={style.transection_img}
                                        resizeMode={'contain'}/>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View>
                            <TouchableOpacity onPress={()=>this.setState({isModalApproved:true,LoadingApproved:true},()=> this.getAccepted())}>
                                <View style={style.view_menu_home}>
                                    <Image source={require('../assets/images/tik.png')} style={style.tik_img}
                                           resizeMode={'contain'}/>
                                    <Text style={style.txt_menu}>درخواست های تایید شده</Text>
                                    <Image source={require('../assets/images/transaction_dot1.png')}
                                           style={style.transection_img}
                                           resizeMode={'contain'}/>
                                </View>
                            </TouchableOpacity>
                            {/* <View style={style.view_menu_home}>
                                <Image source={require('../assets/images/setting.png')} style={style.tik_img}
                                       resizeMode={'contain'}/>
                                <Text style={style.txt_menu}>تنظیمات حساب کاربری</Text>
                                <Image source={require('../assets/images/transaction_dot1.png')}
                                       style={style.transection_img}
                                       resizeMode={'contain'}/>
                            </View> */}
                        </View>
                    </View>
                </Content>
                
                {/*package type*/}
                <Modal style={{marginVertical:70,borderRadius:50}} isVisible={this.state.isModalPackageType}
                    onBackdropPress={() => this.setState({ isModalPackageType: false })} onBackButtonPress={() => this.setState({ isModalPackageType: false})}>
                    <Container style={{paddingHorizontal:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>نوع بسته بندی را انتخاب کنید</Text>
                        {/* <View style={{height:50,width:"90%",marginTop:5}}>
                        <Input
                            style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                            placeholder="جستجو ..."                        
                            onChangeText={text => this.searchFilterCarType(text)}
                            autoCorrect={false}             
                        />  
                        </View> */}
                        <Content style={{width:'100%',marginBottom:10}}>
                            {(this.state.LoadingPackageType) && <ActivityIndicator color   ="#2395ff" size="large"/>}
                            {(!this.state.LoadingPackageType) && 
                                <FlatList
                                    data={this.state.packageType}
                                    renderItem={this.renderPackageType.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            }
                        </Content>
                        
                    </Container>
                </Modal>
                
                {/*add request*/}
                <Modal style={{margin:0,marginTop:45,backgroundColor:'#fff', borderTopLeftRadius:30,borderTopRightRadius:30}} isVisible={this.state.isModalRequest} backdropOpacity={0.80}  
                        onBackdropPress={() => this.setState({ isModalRequest: false })} onBackButtonPress={() => this.setState({ isModalRequest: false})}>
                                <Content style={{flex:1,width:width}}>
                                    <View style={{alignItems: 'center', justifyContent: 'center',}}>
                                        <Text style={styles.btnLoginAccount}>درخواست حمل و نقل <Text
                                            style={styles.txt_confirm}>(در
                                            حال تایید)</Text></Text>
                                        <View style={styles.border_underline}/>

                                        <Text style={styles.txt_request_name}>نام درخواست کننده : 
                                            <Text style={styles.txt_confirm}> {this.state.userData.firstname} {this.state.userData.lastname}</Text>
                                        </Text>
                                        <Image source={require('../assets/images/placeholder.png')} resizeMode={'contain'}
                                            style={styles.img_placeholder}/>
                                        <View style={styles.view_txt_placeholder}>
                                            <View style={{flexDirection:'column',alignItems:'center'}}>
                                                <Text style={styles.btnLoginAccount1}>{this.state.Province_selectedById_name}</Text>
                                            </View>
                                            <View style={{flexDirection:'column',alignItems:'center'}}>
                                            <Text style={styles.btnLoginAccount1}>{this.state.Province_selectedById_nameDst}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.view_txt_address}>
                                            <Text style={styles.btnLoginAccount12}><Text> نشانی دقیق مبدا: </Text>
                                            {this.state.originAdr}
                                            </Text>
                                        </View>
                                        <View style={styles.view_txt_address}>
                                            <Text style={styles.btnLoginAccount12}>
                                                <Text>نشانی دقیق مقصد: </Text>
                                            {this.state.destinationAdr}
                                            </Text>
                                        </View>
                                        <View style={styles.view_txt_address1}>
                                            <Text style={[styles.btnLoginAccount12,{flex:1,paddingRight:25}]}><Text>نوع ماشین مورد
                                                نیاز : </Text>{this.state.carSelectedName_second} {","} {this.state.carSelectedName}</Text>
                                            <Text style={styles.btnLoginAccount12}><Text>تناژ بار : </Text>{this.state.weight} تن</Text>
                                        </View>
                                        <View style={[styles.view_txt_address,{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}]}>
                                            <Text style={[styles.btnLoginAccount12,{paddingLeft:10}]}>
                                                <Text>مسافت : </Text>
                                            {Helper.ToPersianNumber(String(this.state.distance))} کیلومتر 
                                            </Text>
                                            <Text style={styles.btnLoginAccount12}>
                                                <Text>نوع بسته بندی: </Text>
                                            {this.state.PackageTypeName}
                                            </Text>
                                        </View>
                                        <View style={styles.view_txt_address}>
                                            <Text style={styles.btnLoginAccount12}>
                                                <Text> توضیحات: </Text>
                                            {this.state.description}
                                            </Text>
                                        </View>         
                                        <Text style={styles.txt_count_fix}>قیمت پیشنهادی</Text>
                                        <View style={{flexDirection:'row', alignItems:'center'}}>
                                            <TouchableOpacity onPress={()=>this.setState({price:parseInt(this.state.price)+parseInt(100000)})}>
                                                <IconFooter name="plus" color="green" style={{fontSize:26,marginRight:10}} />
                                            </TouchableOpacity>
                                            <View style={styles.view_txt_count}>
                                                <Text style={styles.txt_count}>{Helper.ToPersianNumber(String(this.state.price))} تومان</Text>
                                            </View>
                                            {this.state.price-100000 > 0 && 
                                            <TouchableOpacity  onPress={()=>this.setState({price:parseInt(this.state.price)-parseInt(100000)})}>
                                                <IconFooter name="minus" color="red" style={{fontSize:26,marginLeft:10}}/>
                                            </TouchableOpacity>
                                            }
                                        </View>
                                                    
                                        {(this.state.LoadingaddCarGo) && <ActivityIndicator size="large" color="#2395ff"/>}
                                        {(!this.state.LoadingaddCarGo) && 
                                        <TouchableOpacity activeOpacity={.5} style={styles.btnSignUpModal}
                                                        onPress={()=>this.addCarGo()}>
                                            <Text style={styles.txt_btn_login}>تایید درخواست صاحب بار</Text>
                                        </TouchableOpacity>}

                                        <TouchableOpacity activeOpacity={.5}
                                                        onPress={() => this.setState({isModalRequest: false} , ()=>this.clearSates())}>
                                            <Text style={styles.cancelRequest}>لغو درخواست</Text>
                                        </TouchableOpacity>
                                    </View>
                                </Content>
                    </Modal>
                
                {/* car_type */}
                <Modal style={{marginVertical:70,borderRadius:50}} isVisible={this.state.isModalCar_type}
                    onBackdropPress={() => this.setState({ isModalCar_type: false })} onBackButtonPress={() => this.setState({ isModalCar_type: false})}>
                    <Container style={{paddingHorizontal:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>خودرو مورد نظر را انتخاب کنید</Text>
                        {/* <View style={{height:50,width:"90%",marginTop:5}}>
                        <Input
                            style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                            placeholder="جستجو ..."                        
                            onChangeText={text => this.searchFilterCarType(text)}
                            autoCorrect={false}             
                        />  
                        </View> */}
                        <Content style={{width:'100%',marginBottom:10}}>
                            {(this.state.loading_carType) && <ActivityIndicator color   ="#2395ff" size="large"/>}
                            {(!this.state.loading_carType) && 
                                <FlatList
                                    data={this.state.carTypes}
                                    renderItem={this.renderCarType.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            }
                        </Content>
                        
                    </Container>
                </Modal>

                {/* car_type by id*/}
                <Modal style={{marginVertical:70,borderRadius:50}} isVisible={this.state.isModalCar_typeById}
                    onBackdropPress={() => this.setState({ isModalCar_typeById: false })} onBackButtonPress={() => this.setState({ isModalCar_typeById: false})}>
                    <Container style={{paddingHorizontal:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>نوع خودرو مورد نظر را انتخاب کنید</Text>
                        {/* <View style={{height:50,width:"90%",marginTop:5}}>
                        <Input
                            style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                            placeholder="جستجو ..."                        
                            onChangeText={text => this.searchFilterCarType(text)}
                            autoCorrect={false}             
                        />  
                        </View> */}
                        <Content style={{width:'100%',marginBottom:10}}>
                            {(this.state.loading_carTypeById) && <ActivityIndicator color   ="#2395ff" size="large"/>}
                            {(!this.state.loading_carTypeById) && 
                                <FlatList
                                    data={this.state.carTypesById}
                                    renderItem={this.renderCarTypeById.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            }
                        </Content>
                        
                    </Container>
                </Modal>
                
                {/* Product Type */}
                <Modal style={{marginVertical:70,borderRadius:50}} isVisible={this.state.isModalProductType}
                    onBackdropPress={() => this.setState({ isModalProductType: false })} onBackButtonPress={() => this.setState({ isModalProductType: false})}>
                    <Container style={{paddingHorizontal:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>بار مورد نظر را انتخاب کنید</Text>
                        <View style={{height:50,width:"90%",marginTop:5}}>
                        <Input
                            style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                            placeholder="جستجو ..."                        
                            onChangeText={text => this.searchFilterCarType(text)}
                            autoCorrect={false}             
                        />  
                        </View>
                        <Content style={{width:'100%',marginBottom:10}}>
                            {(this.state.LoadingProductType) && <ActivityIndicator color   ="#2395ff" size="large"/>}
                            {(!this.state.LoadingProductType) && 
                                <FlatList
                                    data={this.state.ProductType}
                                    renderItem={this.renderProductType.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            }
                        </Content>
                        
                    </Container>
                </Modal>

                {/* Province */}
                <Modal style={{marginVertical:70}} isVisible={this.state.isModalProvincelist}
                    onBackdropPress={() => this.setState({ isModalProvincelist: false })} onBackButtonPress={() => this.setState({ isModalProvincelist: false})}>
                    <Container style={{paddingHorizontal:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>استان مورد نظر را انتخاب کنید</Text>
                        <View style={{height:50,width:"90%",marginTop:5}}>
                        <Input
                            style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                            placeholder="جستجو ..."                        
                            onChangeText={text => this.searchFilterFunction(text)}
                            autoCorrect={false}             
                        />  
                        </View>
                        <Content style={{width:'100%',marginBottom:10}}>
                            {(this.state.LoadingProvince) && <ActivityIndicator color   ="#2395ff" size="large"/>}
                            {(!this.state.LoadingProvince) && 
                                <FlatList
                                    data={this.state.dataUserProvince}
                                    renderItem={this.renderProvincelist.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            }
                        </Content>
                        
                    </Container>
                </Modal>

                {/* Province Dst*/}
                <Modal style={{marginVertical:70}} isVisible={this.state.isModalProvincelistDst}
                    onBackdropPress={() => this.setState({ isModalProvincelistDst: false })} onBackButtonPress={() => this.setState({ isModalProvincelistDst: false})}>
                    <Container style={{paddingTop:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                    <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>استان مورد نظر را انتخاب کنید</Text>
                        <View style={{height:50,width:"90%",marginTop:5}}>
                        <Input
                            style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                            placeholder="جستجو ..."                        
                            onChangeText={text => this.searchFilterFunction(text)}
                            autoCorrect={false}             
                        />  
                        </View>
                        <Content style={{width:'100%',marginBottom:10}}>
                            {(this.state.LoadingProvince) && <ActivityIndicator color="#2395ff" size="large"/>}
                            {(!this.state.LoadingProvince) && 
                                <FlatList
                                    data={this.state.dataUserProvince}
                                    renderItem={this.renderProvincelistDst.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent',paddingHorizontal:20 }}
                                />
                            }
                        </Content>
                        
                    </Container>
                </Modal>
                
                {/* Done */}
                <Modal isVisible={this.state.Done} onBackdropPress={()=>this.setState({Done:false})} onBackButtonPress={()=>this.setState({Done:false})}>
                        <View style={{ flex: 1 ,justifyContent:'center'}}>
                        <Animated.View style={{scaleX:this.state.DoneAnim,
                                        scaleY:this.state.DoneAnim}}>
                                    <TouchableOpacity
                                        onPress={()=>this.setState({Done:false})}
                                        style={{height:150,width:150,elevation:75, borderRadius:75,backgroundColor:'#25B92E',alignSelf:'center',alignItems:'center',justifyContent:'center'}}>
                                        <IconFooter size={width * .1} color='white' name='check'/>
                                        <Text style={{color:'white',fontFamily:'IRANSansMobile', textAlign:'center',fontSize:13}}>با موفقیت انجام شد</Text>
                                    </TouchableOpacity>
                            </Animated.View>
                        </View>
                </Modal>

                {/*add data*/}
                <Modal style={{margin:0,marginTop:45}} isVisible={this.state.isModalRequestdata} backdropOpacity={0.80} backdropColor='#203fff' backdropOpacity={0.8}
                    onBackdropPress={() => this.setState({ isModalRequestdata: false },() => this.clearSates())} onBackButtonPress={() => this.setState({ isModalRequestdata: false},() => this.clearSates())}>
                    <Container style={{borderTopRightRadius:50,paddingTop:10,borderTopLeftRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <TouchableOpacity style={styles.exitModalLogin}>
                            <Image source={require('../assets/images/usericon.png')} style={styles.img_exitModalLogin} resizeMode={'contain'}/>
                        </TouchableOpacity>
                        <Text style={{marginTop:20,textAlign:'center',marginBottom:20,fontFamily:'IRANSansMobile'}}>فرم اطلاعات بار</Text>

                        <Content style={{width:"100%",paddingHorizontal:20}}>
                            <Form>
                                {/* <Item regular style={{marginBottom:10}}>
                                    <View style={{position:"absolute",right:width*0.03,fontFamily:'IRANSansMobile'}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.9}}>عنوان 
                                            <Text style={{color:'red'}}> * </Text>
                                            :</Text>
                                    </View>
                                    <Input
                                        style={{textAlign:'right', paddingRight:width*0.2,fontFamily:'IRANSansMobile_Bold',fontSize:17}}
                                        placeholderTextColor={'#999999'}
                                        value={this.state.title}
                                        onChangeText={(text) => this.setState({title: text})}
                                    />
                                </Item> */}
                                <TouchableOpacity style={{justifyContent:'center',borderColor:'#e1e1e1',borderWidth:1,marginBottom:10}} onPress={()=>this.setState({isModalCar_type:true , loading_carType:true},()=>this.getCarType())}>
                                    <View style={{position:"absolute",right:width*0.03,fontFamily:'IRANSansMobile'}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.9}}>نوع خودرو 
                                            <Text style={{color:'red'}}> * </Text>
                                            :</Text>
                                    </View>
                                    <View style={{paddingVertical:10}}>
                                        <Text style={{textAlign:'right',color:'black',fontWeight:'bold',paddingRight:width*0.3,fontSize:18}}>{this.state.carSelectedName}  {this.state.carSelectedName_second}</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{justifyContent:'center',borderColor:'#e1e1e1',borderWidth:1,marginBottom:10}} onPress={()=>this.setState({isModalProductType:true , LoadingProductType:true},()=>this.getProductType())}>
                                <View style={{position:"absolute",right:width*0.03,fontFamily:'IRANSansMobile'}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.9}}>نوع بار 
                                            <Text style={{color:'red'}}> * </Text>
                                            :</Text>
                                    </View>
                                    <View style={{paddingVertical:10}}>
                                        <Text style={{textAlign:'right',color:'black',fontWeight:'bold',paddingRight:width*0.25,fontSize:18}}>{this.state.cargo_type}</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{justifyContent:'center',borderColor:'#e1e1e1',borderWidth:1,marginBottom:10}} onPress={()=>this.setState({isModalPackageType:true , LoadingPackageType:true},()=>this.getPackageType())}>
                                <View style={{position:"absolute",right:width*0.03,fontFamily:'IRANSansMobile'}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.9}}>نوع بسته بندی 
                                            <Text style={{color:'red'}}> * </Text>
                                            :</Text>
                                    </View>
                                    <View style={{paddingVertical:10}}>
                                        <Text style={{textAlign:'right',color:'black',fontWeight:'bold',paddingRight:width*0.39,fontSize:18}}>{this.state.PackageTypeName}</Text>
                                    </View>
                                </TouchableOpacity>
                                {this.state.carSelectedId != 1  && 
                                    <Item regular style={{marginBottom:10}}>
                                    <View style={{position:"absolute",right:width*0.03,fontFamily:'IRANSansMobile'}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.9}}>هزینه 
                                            <Text style={{color:'red'}}> * </Text>
                                            :</Text>
                                    </View>
                                    <Input
                                        style={{textAlign:'right', paddingRight:width*0.2,fontFamily:'IRANSansMobile_Bold'}}
                                        placeholderTextColor={'#999999'}
                                        keyboardType="numeric"
                                        value={this.state.price}
                                        onChangeText={(text) => this.setState({price: text})}
                                    />
                                    </Item>
                                }
                                {/* <Item regular style={{marginBottom:10,paddingVertical:15}}>
                                    <View style={{position:"absolute",right:width*0.03,fontFamily:'IRANSansMobile'}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.9}}>مسافت 
                                            <Text style={{color:'red'}}> * </Text>
                                            :</Text>
                                    </View>
                                    <Text style={{textAlign:'right',fontFamily:'IRANSansMobile'}}>{this.state.distance}</Text>
                                </Item> */}
                                <Item regular style={{marginBottom:10,height:50}}>
                                    <View style={{position:"absolute",right:width*0.03,fontFamily:'IRANSansMobile'}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.9}}>وزن 
                                            <Text style={{color:'red'}}> * </Text>
                                            :</Text>
                                    </View>
                                    <View style={{width:'100%',marginBottom:10}}>
                                        <Text style={{fontFamily:'IRANSansMobile',marginLeft:20,color:'black',textAlign:'left'}}>{Helper.ToPersianNumber(String(this.state.weight))} تن</Text>
                                        <Slider
                                            thumbTintColor={'#2395ff'}
                                            style={{width:"75%",color:'red'}}
                                            step={1}
                                            maximumValue={22}
                                            onValueChange={(text)=>this.setState({weight:text})}
                                            value={this.state.weight}
                                        />
                                    </View>
                                    
                                    {/* <Input
                                        style={{textAlign:'right', paddingRight:width*0.17,fontFamily:'IRANSansMobile_Bold'}}
                                        placeholderTextColor={'#999999'}
                                        value={this.state.weight}
                                        keyboardType="numeric"
                                        onChangeText={(text) => this.setState({weight: text})}
                                    /> */}
                                </Item>
                                <TouchableOpacity style={{justifyContent:'center',borderColor:'#e1e1e1',borderWidth:1,marginBottom:10}} onPress={()=>this.setState({Province_selected_nameDst:'',Province_selectedById_nameDst:'',isModalProvincelist:true , LoadingProvince:true},()=>this.Provincelist())}>
                                    <View style={{position:"absolute",right:width*0.03,fontFamily:'IRANSansMobile'}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.9}}>مبداء 
                                            <Text style={{color:'red'}}> * </Text>
                                            :</Text>
                                    </View>
                                    <View style={{paddingVertical:10}}>
                                        <Text style={{textAlign:'right',color:'black',fontWeight:'bold',paddingRight:width*0.21,fontSize:18}}>{this.state.Province_selected_name+'  '+this.state.Province_selectedById_name}</Text>
                                    </View>
                                </TouchableOpacity>
                                <Item regular style={{marginBottom:10}}>
                                    <View style={{position:"absolute",right:width*0.03}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.9}}>ادرس مبداء 
                                            <Text style={{color:'red'}}> * </Text>
                                            :</Text>
                                    </View>
                                    <Input
                                        style={{textAlign:'right', paddingRight:width*0.33,fontFamily:'IRANSansMobile_Bold'}}
                                        value={this.state.originAdr}
                                        onChangeText={(text) => this.setState({originAdr: text})}
                                    />
                                </Item>
                                <TouchableOpacity style={{justifyContent:'center',borderColor:'#e1e1e1',borderWidth:1,marginBottom:10}} onPress={()=>this.setState({isModalProvincelistDst:true , LoadingProvince:true},()=>this.Provincelist())}>
                                    <View style={{position:"absolute",right:width*0.03}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.9}}>مقصد 
                                            <Text style={{color:'red'}}> * </Text>
                                            :</Text>
                                    </View>
                                    <View style={{paddingVertical:10}}>
                                        <Text style={{textAlign:'right',color:'black',fontWeight:'bold',paddingRight:width*0.21,fontSize:18}}>{this.state.Province_selected_nameDst+'  '+this.state.Province_selectedById_nameDst}</Text>
                                    </View>
                                </TouchableOpacity>
                                <Item regular style={{marginBottom:10}}>
                                    <View style={{position:"absolute",right:width*0.03}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.9}}>ادرس مقصد 
                                            <Text style={{color:'red'}}> * </Text>
                                            :</Text>
                                    </View>
                                    <Input
                                        style={{textAlign:'right', paddingRight:width*0.33,fontFamily:'IRANSansMobile_Bold'}}
                                        value={this.state.destinationAdr}
                                        onChangeText={(text) => this.setState({destinationAdr: text})}
                                    />
                                </Item>
                                
                                <Item regular style={{marginBottom:10}}>
                                    <View style={{position:"absolute",right:width*0.03}}>
                                        <Text style={{fontFamily:'IRANSansMobile',fontSize:17,opacity:.9}}>توضیحات  
                                            <Text style={{color:'red'}}></Text>
                                            :</Text>
                                    </View>
                                    <Input
                                        style={{textAlign:'right', paddingRight:width*0.25,fontFamily:'IRANSansMobile_Bold'}}
                                        value={this.state.description}
                                        onChangeText={(text) => this.setState({description: text})}
                                    />
                                </Item>
                            </Form>
                            {(this.state.message !== '') && <Text style={{textAlign:'center',color:'red',marginBottom:5,fontSize:16, fontFamily:'IRANSansMobile'}}>{this.state.message}</Text>}
                            {!this.state.LoadingAddData && 
                            <View>
                                <TouchableOpacity onPress={()=> this.CheckModal()} style={{width:"100%" ,backgroundColor:'#2395ff'}}>
                                    <Text style={{textAlign:'center',color:'#fff',fontFamily: 'IRANSansMobile',paddingVertical:10,fontSize:18}}>ثبت اطلاعات</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>this.setState({isModalRequestdata:false},() => this.clearSates())}>
                                    <Text style={{textAlign:'center',fontFamily: 'IRANSansMobile',marginTop:5, marginBottom:5,fontSize:18}}>لغو</Text>
                                </TouchableOpacity>
                            </View>
                            }
                            {this.state.LoadingAddData && <ActivityIndicator size="large" color="#2395ff"/>}
                            
                        </Content>
                    </Container>
                </Modal>
                
                {/*accepted request*/}
                <Modal style={{margin:0,marginTop:120,marginBottom:0}} isVisible={this.state.isModalApproved} backdropOpacity={0.80} backdropColor='#203fff' backdropOpacity={0.8}
                    onBackdropPress={() => this.setState({ isModalApproved: false })} onBackButtonPress={() => this.setState({ isModalApproved: false})}>
                    <Container style={{borderTopRightRadius:50,paddingTop:10,borderTopLeftRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <View style={style.view_top_Approved}>
                            <View style={style.viewTop_topApproved}>
                                <Image source={require('../assets/images/tik.png')} style={style.tik_imgTopApproved}
                                       resizeMode={'contain'}/>
                            </View>
                            <View style={style.viewTop_centerApproved}>
                                <Text style={style.txt_topApproved}>درخواست های تایید شده</Text>
                            </View>
                            <View style={style.viewTop_bottomApproved}>
                                <Image source={require('../assets/images/transaction_dot1.png')}
                                       style={style.transection_img}
                                       resizeMode={'contain'}/>
                            </View>
                        </View>
                        <Content style={{width:'100%'}}>
                            
                                <FlatList
                                    data={this.state.AllProductAccept}
                                    renderItem={this.renderAllProductAccept.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                    ListEmptyComponent={this.handleEmpty.bind(this)}
                                />
                                
                        </Content>
                        
                    </Container>
                </Modal>

                {/*waiting request*/}
                <Modal style={{margin:0,marginTop:120,marginBottom:0}} isVisible={this.state.isModalWaiting} backdropOpacity={0.80} backdropColor='#203fff' backdropOpacity={0.8}
                    onBackdropPress={() => this.setState({ isModalWaiting: false })} onBackButtonPress={() => this.setState({ isModalWaiting: false})}>
                    <Container style={{borderTopRightRadius:50,paddingTop:10,borderTopLeftRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <View style={style.view_top_Approved}>
                            <View style={style.viewTop_topApproved}>
                            <Image source={require('../assets/images/clock.png')} style={[style.tik_img,{width:50,height:50}]}
                                       resizeMode={'contain'}/>
                            </View>
                            <View style={style.viewTop_centerApproved}>
                                <Text style={style.txt_topApproved}>درخواست های در انتظار تایید</Text>
                            </View>
                            <View style={style.viewTop_bottomApproved}>
                                <Image source={require('../assets/images/transaction_dot1.png')}
                                       style={style.transection_img}
                                       resizeMode={'contain'}/>
                            </View>
                        </View>
                        
                        <Content style={{width:'100%'}}>
                            {/* {((this.state.AllProductWaiting.length == 0) && !this.state.LoadingWaiting) && <Text style={{fontFamily:'IRANSansMobile',textAlign:'center'}}>درخواستی وجود ندارد</Text>}
                            {this.state.LoadingWaiting && <ActivityIndicator  color="#2395ff" size='large' />}
                            {(!this.state.LoadingWaiting && this.state.AllProductWaiting.length != 0) && */}
                                <FlatList
                                    data={this.state.AllProductWaiting}
                                    renderItem={this.renderAllWaiting.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                    ListEmptyComponent={this.handleEmptyWaiting.bind(this)}
                                /> 
                        </Content>
                        
                    </Container>
                </Modal>
                
                {/* Driver accepted List */}
                <Modal style={{margin:0,marginTop:120,marginBottom:0}} isVisible={this.state.isModalDriverList} backdropOpacity={0.80} backdropColor='#203fff' backdropOpacity={0.8}
                    onBackdropPress={() => this.setState({ isModalDriverList: false,carSelectedName_second:'',carSelectedName:'' })} onBackButtonPress={() => this.setState({ isModalDriverList: false,carSelectedName_second:'',carSelectedName:''})}>
                    <Container style={{borderTopRightRadius:50,paddingTop:10,borderTopLeftRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <View style={style.view_top_Approved}>
                            <View style={style.viewTop_topApproved}>
                            <Image source={require('../assets/images/clock.png')} style={[style.tik_img,{width:50,height:50}]}
                                       resizeMode={'contain'}/>
                            </View>
                            <View style={style.viewTop_centerApproved}>
                                <Text style={style.txt_topApproved}>لیست راننده ها</Text>
                            </View>
                            <View style={style.viewTop_bottomApproved}>
                                <Image source={require('../assets/images/transaction_dot1.png')}
                                       style={style.transection_img}
                                       resizeMode={'contain'}/>
                            </View>
                        </View>
                        <Content style={{width:'100%'}}>
                            {(this.state.LoadingWaiting) &&  <ActivityIndicator color="#2395ff" size="large"/>}
                            {(!this.state.LoadingWaiting) && 
                                <FlatList
                                    data={this.state.data_Driver}
                                    renderItem={this.renderDrivers.bind(this)}
                                    keyExtractor={(item,index) => index.toString() }
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            }
                        </Content>
                        
                    </Container>
                </Modal>
                
                {/* Driver Data */}
                <Modal style={{margin:0,marginTop:45}} isVisible={this.state.isModalDriverSee} backdropOpacity={0}  
                        onBackdropPress={() => this.setState({ isModalDriverSee: false,carSelectedName_second:'',carSelectedName:'' })} onBackButtonPress={() => this.setState({ isModalDriverSee: false,carSelectedName_second:'',carSelectedName:''})}>
                                
                            <Content style={styles.viewContainer_SearchBar}>
                               
                                <View style={{width:width}}>
                                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                        <Text style={styles.btnLoginAccount}>درخواست حمل و نقل <Text
                                            style={styles.txt_confirm}>(در
                                            حال تایید)</Text></Text>
                                        {this.state.LoadingPlate && <ActivityIndicator color="#2395ff" style={{marginTop:5}}/>}
                                         
                                        {!this.state.LoadingPlate &&
                                            <View style={{justifyContent:'center',alignItems:'center',}}>
                                                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-around'}}>
                                                    <Text style={styles.btnLoginAccount1}>{this.state.userDataDriver.firstname} {this.state.userDataDriver.lastname}</Text>
                                                    <Text style={styles.btnLoginAccount1}>نام راننده : </Text>
                                                </View>
                                                {this.state.carSelectedName_second != 'تریلی' && 
                                                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-around'}}>
                                                    
                                                        <Text style={styles.btnLoginAccount1}>{Helper.ToPersianNumber(this.state.userDataDriver.phone_number)}</Text>

                                                        <Text style={styles.btnLoginAccount1}>شماره تماس راننده : </Text>

                                                </View>
                                                }
                                            </View>
                                        }
                                        {!this.state.LoadingPlate &&
                                            <View style={{flex:1,paddingTop:0,width:width*.55}}>
                                                <Image source={require('../assets/images/plate.jpg')} style={{width:width*.9,height:45,marginHorizontal:10,alignSelf:'center',justifyContent:'center'}} resizeMode='contain'/>
                                                    <View style={{flexDirection:'row-reverse',position:'absolute',top:2,right:0,marginHorizontal:20,justifyContent:'space-around',alignItems:'center'}}>
                                                        <View style={{flex:.3,flexDirection:'row',justifyContent:'center'}}>
                                                        <Text style={{textAlign:'center',padding:0,height:100,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:20,color:'black'}}>{Helper.ToPersianNumber(this.state.userDataDriver.plak_no.slice(6,8))}</Text>                                   
                                                    </View>
                                                    <View style={{flex:1,flexDirection:'row-reverse',marginHorizontal:15}}>
                                                        <Text style={{textAlign:'center',padding:0,height:100,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:20,color:'black'}}>{Helper.ToPersianNumber(this.state.userDataDriver.plak_no.slice(3,6))}</Text>
                                                        <Text style={{textAlign:'center',marginBottom:5,padding:0,height:100,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:20,color:'black'}}>{this.state.userDataDriver.plak_no.slice(2,3)}</Text>
                                                        <Text style={{textAlign:'center',padding:0,marginRight:-10,height:100,justifyContent:'center',flex:1,fontFamily:'IRANSansMobile',alignSelf:'center',fontSize:20,color:'black'}}>{Helper.ToPersianNumber(this.state.userDataDriver.plak_no.slice(0,2))}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        
                                        }
                                        
                                        <Image source={require('../assets/images/placeholder.png')} resizeMode={'contain'}
                                            style={{width:width*.9}}/>
                                        <View style={styles.view_txt_placeholder}>
                                            <Text style={styles.btnLoginAccount1}>{this.state.origin_selected}</Text>
                                            <Text style={styles.btnLoginAccount1}>{this.state.destination_selected}</Text>
                                        </View>
                                        <View style={[styles.view_txt_address,{alignItems:'flex-end',paddingRight:10}]}>
                                            <Text style={styles.btnLoginAccount12}><Text> نشانی دقیق مبدا: </Text>
                                        {this.state.origin_address_selected}
                                        </Text>
                                        </View>
                                        <View style={[styles.view_txt_address,{alignItems:'flex-end',paddingRight:10}]}>
                                            <Text style={styles.btnLoginAccount12}>
                                                <Text>نشانی دقیق مقصد: </Text>
                                            {this.state.destination_address_selected}
                                            </Text>
                                        </View>
                                        <View style={styles.view_txt_address1}>
                                            <Text style={styles.btnLoginAccount12}>
                                            <Text>نوع بار: </Text>
                                            {this.state.cargo_type_selected}
                                            </Text>
                                            <Text style={styles.btnLoginAccount12}><Text>تناژ بار : </Text>{Helper.ToPersianNumber(this.state.weight_selected)} تن</Text>
                                        </View>
                                        <View style={[styles.view_txt_address,{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}]}>
                                            <Text style={[styles.btnLoginAccount12,{paddingLeft:10}]}>
                                                <Text>مسافت : </Text>
                                            {Helper.ToPersianNumber(String(this.state.distance_Selected))} کیلومتر 
                                            </Text>
                                            <Text style={styles.btnLoginAccount12}>
                                                <Text>نوع بسته بندی: </Text>
                                            {this.state.pakage_type_selected}
                                            </Text>
                                        </View>
                                        <View style={style.view_txt_address}>
                                            <Text style={[style.btnLoginAccount12]}><Text>نوع ماشین مورد
                                                نیاز : </Text>{this.state.carSelectedName} {","} {this.state.carSelectedName_second}</Text>
                                        </View>
                                        <View style={styles.view_txt_address}>
                                            <Text style={styles.btnLoginAccount12}>
                                                <Text> توضیحات: </Text>
                                            {this.state.description_selected}
                                            </Text>
                                        </View>  
                                        <Text style={styles.txt_count_fix}>قیمت پیشنهادی</Text>
                                        <View style={styles.view_txt_count}>
                                            <Text style={styles.txt_count}>{Helper.ToPersianNumber(this.state.price_selected)} تومان</Text>
                                        </View>
                                         
                                        {/* {this.state.driverPrice != "" &&
                                        <View>
                                            <Text style={styles.txt_count_fix}>قیمت پیشنهادی راننده</Text>
                                            <View style={[styles.view_txt_countsell,{backgroundColor:'#3ae02e'}]}>
                                                    <Text style={{textAlign:'center',color:'white',fontFamily:'IRANSansMobile',fontSize:15,paddingVertical:5}}>{Helper.ToPersianNumber(this.state.driverPrice)} تومان</Text>
                                            </View>
                                        </View>
                                        } */}
                                        
                                        {(this.state.LoadingAccept) && <ActivityIndicator size="large" color="#2395ff"/>}
                                        {(!this.state.LoadingAccept) && 
                                        <View style={{flexDirection:'row',justifyContent:'space-around',width:width,alignItems:'center'}}>
                                            <TouchableOpacity onPress={()=>this.customerAccept()} activeOpacity={.5} style={styles.btnSignUpModalHome}>
                                                <Text style={styles.txt_btn_login}>تایید</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>this.CustomerCancel()} activeOpacity={.5} style={{width:width*.3,borderColor:'#2395ff',borderWidth:1,alignItems:'center',borderRadius:5}}>
                                                <Text style={styles.cancelRequest}>لغو</Text>
                                            </TouchableOpacity>
                                        </View>}
                                    </View>
                                </View>
                            </Content>
                    </Modal>
                
                {/* Waiting Data */}
                <Modal style={{margin:0,marginTop:45}} isVisible={this.state.isModalDriver} backdropOpacity={0}  
                        onBackdropPress={() => this.setState({ isModalDriver: false,carSelectedName_second:'',carSelectedName:'' })} onBackButtonPress={() => this.setState({ isModalDriver: false,carSelectedName_second:'',carSelectedName:''})}>
                                
                            <Content style={styles.viewContainer_SearchBar}>
                               
                                <View style={{width:width}}>
                                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                        <Text style={styles.btnLoginAccount}>درخواست حمل و نقل <Text
                                            style={styles.txt_confirm}>(در
                                            انتظار تایید)</Text></Text>
                                        <View style={styles.border_underline}/>
                                        <Image source={require('../assets/images/placeholder.png')} resizeMode={'contain'}
                                            style={{width:width*.9}}/>
                                        <View style={styles.view_txt_placeholder}>
                                            <Text style={styles.btnLoginAccount1}>{this.state.origin_selected}</Text>
                                            <Text style={styles.btnLoginAccount1}>{this.state.destination_selected}</Text>
                                        </View>
                                        <View style={[styles.view_txt_address,{alignItems:'flex-end',paddingRight:10}]}>
                                            <Text style={styles.btnLoginAccount12}><Text> نشانی دقیق مبدا: </Text>
                                        {this.state.origin_address_selected}
                                        </Text>
                                        </View>
                                        <View style={[styles.view_txt_address,{alignItems:'flex-end',paddingRight:10}]}>
                                            <Text style={styles.btnLoginAccount12}>
                                                <Text>نشانی دقیق مقصد: </Text>
                                            {this.state.destination_address_selected}
                                            </Text>
                                        </View>
                                        <View style={styles.view_txt_address1}>
                                            <Text style={styles.btnLoginAccount12}>
                                            <Text>نوع بار: </Text>
                                            {this.state.cargo_type_selected}
                                            </Text>
                                            <Text style={styles.btnLoginAccount12}><Text>تناژ بار : </Text>{Helper.ToPersianNumber(this.state.weight_selected)} تن</Text>
                                        </View>
                                        <View style={styles.view_txt_address1}>
                                            <Text style={[styles.btnLoginAccount12,{paddingLeft:10}]}>
                                                <Text>مسافت : </Text>
                                            {Helper.ToPersianNumber(String(this.state.distance_Selected))} کیلومتر 
                                            </Text>
                                            <Text style={styles.btnLoginAccount12}>
                                                <Text>نوع بسته بندی: </Text>
                                            {this.state.pakage_type_selected}
                                            </Text>
                                        </View>
                                        <View style={styles.view_txt_address}>
                                            <Text style={[styles.btnLoginAccount12]}><Text>نوع ماشین مورد
                                                نیاز : </Text>{this.state.carSelectedName} {","} {this.state.carSelectedName_second}</Text>
                                        </View>
                                        <View style={styles.view_txt_address}>
                                            <Text style={styles.btnLoginAccount12}>
                                                <Text> توضیحات: </Text>
                                            {this.state.description_selected}
                                            </Text>
                                        </View>  
                                        <Text style={styles.txt_count_fix}>قیمت پیشنهادی</Text>
                                        <View style={styles.view_txt_count}>
                                            <Text style={styles.txt_count}>{Helper.ToPersianNumber(this.state.price_selected)} تومان</Text>
                                        </View>
                                        {!this.state.LoadingCancelProduct &&
                                        <TouchableOpacity onPress={()=>this.cancelProduct()}>
                                            <Text style={{fontFamily:'IRANSansMobile',color:'red',fontSize:16}}>حذف درخواست</Text>
                                        </TouchableOpacity>
                                        }
                                        {this.state.LoadingCancelProduct && <ActivityIndicator style={{marginTop:5}} color="red"/>}
                                        
                                         
                                        {/* {this.state.driverPrice != "" &&
                                        <View>
                                            <Text style={styles.txt_count_fix}>قیمت پیشنهادی راننده</Text>
                                            <View style={[styles.view_txt_countsell,{backgroundColor:'#3ae02e'}]}>
                                                    <Text style={{textAlign:'center',color:'white',fontFamily:'IRANSansMobile',fontSize:15,paddingVertical:5}}>{Helper.ToPersianNumber(this.state.driverPrice)} تومان</Text>
                                            </View>
                                        </View>
                                        } */}
                                        
                                    </View>
                                </View>
                            </Content>
                    </Modal>
                
                {/* Province by id Dst */}
                <Modal style={{marginVertical:70}} isVisible={this.state.isModalProvincelistByIdDst}
                    onBackdropPress={() => this.setState({ isModalProvincelistByIdDst: false })} onBackButtonPress={() => this.setState({ isModalProvincelistByIdDst: false})}>
                    <Container style={{paddingTop:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>شهر مورد نظر را انتخاب کنید</Text>
                        <View style={{height:50,width:"90%",marginTop:5}}>
                        <Input
                            style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                            placeholder="جستجو ..."                        
                            onChangeText={text => this.searchFilterFunctionById(text)}
                            autoCorrect={false}             
                        />  
                        </View>
                        <Content style={{width:'100%',marginBottom:10}}>
                            {(this.state.LoadingProvinceById) && <ActivityIndicator color="#2395ff" size="large"/>}
                            {(!this.state.LoadingProvinceById) && 
                                <FlatList
                                    data={this.state.dataUserProvinceById}
                                    renderItem={this.renderProvincelistByIdDst.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            }
                        </Content>
                        
                    </Container>
                </Modal>
                
                {/* Province by id */}
                <Modal style={{marginVertical:70}} isVisible={this.state.isModalProvincelistById}
                    onBackdropPress={() => this.setState({ isModalProvincelistById: false })} onBackButtonPress={() => this.setState({ isModalProvincelistById: false})}>
                    <Container style={{paddingTop:10,borderRadius:50,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:18,color:'black',paddingTop:10}}>شهر مورد نظر را انتخاب کنید</Text>
                        <View style={{height:50,width:"90%",marginTop:5}}>
                        <Input
                            style={{backgroundColor:'#eee',borderRadius:15,fontFamily:'IRANSansMobile'}}
                            placeholder="جستجو ..."                        
                            onChangeText={text => this.searchFilterFunctionById(text)}
                            autoCorrect={false}             
                        />  
                        </View>
                        <Content style={{width:'100%',marginBottom:10}}>
                            {(this.state.LoadingProvinceById) && <ActivityIndicator color="#2395ff" size="large"/>}
                            {(!this.state.LoadingProvinceById) && 
                                <FlatList
                                    data={this.state.dataUserProvinceById}
                                    renderItem={this.renderProvincelistById.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            }
                        </Content>
                        
                    </Container>
                </Modal>
                <View >
                    <Animated.View
                        style={{
                            scaleX: this.state.removeAnim,
                            scaleY: this.state.removeAnim,
                            zIndex:99,
                            height:width*.18,
                            width:width*.18,
                            justifyContent:'center',
                            alignItems:'center',
                            alignSelf:'center',
                            bottom: height * .05, zIndex: 99,
                            
                        }}
                    >
                        <TouchableOpacity onPress={()=>this.setState({isModalRequestdata:true})} style={styles.view_add_bottom_center}>
                            <View style={styles.view_icon_add_bottom_center}>
                                <IconFooter size={width * .05} color='white' name='plus'/>
                            </View>
                        </TouchableOpacity>
                    </Animated.View>

                    <BottomNavigate color_prof={'#419aff'} nameLeft={'پروفایل'} iconLeft={'user'}
                                    nameCenter={'درخواست ماشین'} iconCenter={'plus'} nameRight={'پشتیبانی'}
                                    iconRight={'headset'} ModalSupport={this.ModalSupport.bind(this)}
                                    onp/>
                </View>
            </Container>
        )
    }
}
const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(home);

const styles = StyleSheet.create({

    view_txt_countsell:{
        marginBottom: height * .01,
        borderRadius: width * .01,
        width: width * .38,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnSignUpModalHome: {
        width:width*.3,
        paddingVertical:7,
        backgroundColor: '#2395ff',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:5
    },
    viewContainer_SearchBar: {
        height:"100%",
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .12,
        borderTopRightRadius: width * .12,
        
    },
    txt_view_Bar_header_menu2: {fontSize: width / 30, color: "#000", fontFamily: 'IRANSansMobile'},

    txt_view_top_header_menu3: {
        fontFamily: 'IRANSansMobile',
        fontSize:9,
        color: "white",
        textAlign:'center'
    },
    view_request2:{
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .26,
        height:width * .26,
        borderRadius: width * .10,
        borderWidth: width * .001,
    },
    viewBar: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    ItemInputSignUp: {marginTop: 20, width: width * .80},
    InputSignUp: {color: '#999999', fontSize: width / 25, textAlign: 'right'},
    formRow:{
        flexDirection: 'row',
        justifyContent:'center',
        alignItems:'center',
        borderBottomWidth:2,
        borderBottomColor:'#e91e63',
        marginHorizontal: 30,
        paddingVertical: 10
    },
    img_exitModalLogin: {
        width: width * .18,
        height: width * .18,
        borderRadius: width * .09,
    },
    exitModalLogin: {
        position: 'absolute',
        top: height * (-.04),
        width: width * .13,
        height: width * .13,
        borderRadius: width * .07,
        borderColor: '#3d56ee',
        borderWidth: width * .01,
        justifyContent: 'center',
        alignItems: 'center'
    },
    view_txt_placeholder: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        width: width * .92
    },
    view_add_bottom_center: {
        borderWidth: width * .02,
        borderColor: '#419aff40',
        width: width * .18,
        height: width * .18,
        borderRadius: width * .09,
        justifyContent: 'center',
        alignItems: 'center',

    },
    view_icon_add_bottom_center: {
        justifyContent: 'center', alignItems: 'center', backgroundColor: '#419aff',
        width: width * .12, height: width * .12,
        borderRadius: width * .06,
        // position: 'absolute', bottom: height * .07,
        // zIndex:1
    },
    txt_navigate_bottom: {fontSize: width / 28, fontFamily: 'IRANSansMobile', color: '#6c6c6c'},
    img_placeholder: {width: width * .90, height: height * .05,},
    border_underline: {borderWidth: width * .001, borderColor: '#e3e3e3', width: width},
    view_home_container: {justifyContent: 'center', margin: 0, padding: 0, alignItems: 'center', flex: 1,},
    img_top_home: {
        width: width,
        height: height * .40,
        margin: 0,
        padding: 0
    },
    view_center_home: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: height * .40,
    },
    view_btn_login: {
        backgroundColor: '#41ea13', marginTop: height * .02,
        height: height * .09,
        width: width * .80,
        justifyContent: 'space-around', alignItems: 'center', flexDirection: 'row'
    },
    txt_btn_login: {
        fontFamily:'IRANSansMobile',
        fontSize: 18,
        color: '#ffffff'
    },
    styleModalSignUpDriver: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
        padding: 0,
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    view_containerModalSignUp: {
        height: height * .80,
        width: width,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
        justifyContent: 'center', alignItems: 'center',
        paddingHorizontal: width * .05
    },
    img_exitModalLogin: {
        width: width * .18,
        height: width * .18,
        borderRadius: width * .09
    },
    exitModalLogin: {
        position: 'absolute',
        top: height * (-.04),
        width: width * .13,
        height: width * .13,
        borderRadius: width * .07,
        borderColor: '#acc4fb',
        borderWidth: width * .01,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex:1
    },
    btnLoginAccount: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#222222',
        marginTop: 15,
        marginBottom: 15
    },
    txt_request_name: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#222222',
        marginTop: 15,
        marginBottom: 15
    },
    btnLoginAccount1: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#222222',

        marginBottom: height * .01
    }, btnLoginAccount12: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 32,
        color: '#222222',

        marginBottom: height * .01
    },
    txt_count_fix: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 25,
        color: '#222222',
        marginBottom: height * .01
    },
    view_txt_address: {
        marginBottom: height * .01,
        backgroundColor: '#f2f5fa',
        borderRadius: width * .02,
        width: width * .90,
        height: height * .08,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingRight:10
    },
    view_txt_count: {
        marginBottom: height * .01,
        backgroundColor: '#2395ff',
        borderRadius: width * .01,
        width: width * .38,
        height: height * .05,
        justifyContent: 'center',
        alignItems: 'center'
    },
    view_txt_address1: {
        marginBottom: height * .01,
        backgroundColor: '#f2f5fa',
        borderRadius: width * .02,
        width: width * .90,
        height: height * .08,
        paddingHorizontal: width * .02,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    txt_count: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 28,
        color: '#fff',
    },

    txt_confirm: {color: '#222222'},
    cancelRequest: {
        fontFamily: 'IRANSansMobile',
        fontSize: 18,
        color: '#2395ff',
        marginTop: height * .01,
        marginBottom: height * .01
    },
    btnSignUpModal: {
        marginTop: height * .03,
        width: width * .80,
        height: height * .08,
        backgroundColor: '#2395ff',
        justifyContent: 'center',
        alignItems: 'center'
    },
});