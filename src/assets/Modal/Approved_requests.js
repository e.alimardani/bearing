import React, {Component} from 'react';
import {View, StyleSheet, Image, Dimensions, Text} from 'react-native';
import Modal from "react-native-modal";
// import style from "../styles/home_css";
import Icon from "react-native-vector-icons/index";
// import style from '../../assets/styles/home_css';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class ApprovedRequests extends Component {

    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         // isModalSupport:false
    //     }
    // }

    componentWillMount() {
        // this.Support
    }

    // Support = () => {
    //     this.props.ModalApproved(true)
    // };

    render() {
        const sss = this.props.ModalApproved;
        console.log('asdsdad!-----------------------------!---------------------------------');
        return (
            <View>
                <Modal isVisible={true}
                       deviceWidth={width}
                       deviceHeight={height}
                       animationType={"slide"}
                       hasBackdrop={false}
                       coverScreen={false}
                       backdropColor={'#000000'}
                       backdropOpacity={0.70}
                       transparent={false}
                       onRequestClose={() => {
                           alert("Modal has been closed.")
                       }}
                       onBackdropPress={() => this.props.ModalApproved(false)}
                       style={style.ModalApproved}
                >
                    <View style={style.viewContainerApproved}>
                        <View style={style.view_top_Approved}>
                            <Image source={require('../../assets/images/tik.png')} style={style.tik_imgTopApproved}
                                   resizeMode={'contain'}/>
                            <Text style={style.txt_topApproved}>درخواست های تایید شده</Text>
                            <Icon name="more-horizontal" color={"#17e617"} size={width * .07}/>
                        </View>
                        <View style={style.view_bottom_Approved}/>
                    </View>

                </Modal>
            </View>
        );
    }
}
const style = StyleSheet.create({
    // tik_imgTopApproved: {width: width * .10, height: height * .10},
    // txt_topApproved: {fontFamily: 'IRANYEKANLIGHT', fontSize: width / 30, color: '#000'},
    // view_top_Approved: {
    //     width: width * .80,
    //     height: height * .15,
    //     backgroundColor: '#fffe7b',
    //     justifyContent: 'center', alignItems: 'center'
    // },
    // view_bottom_Approved: {
    //     flex: 1,
    //     width: width * .90,
    //     height: height * .60,
    //     backgroundColor: '#6941d8'
    // },
    // ModalApproved: {
    //     flex: 1,
    //     alignItems: 'center',
    //     justifyContent: 'flex-start',
    //     margin: 0,
    //     padding: 0,
    //     borderTopLeftRadius: width * .15,
    //     borderTopRightRadius: width * .15,
    // },
    // viewContainerApproved: {
    //     backgroundColor: "#fff",
    //     borderTopLeftRadius: width * .10,
    //     borderTopRightRadius: width * .10,
    //     width: width * .90,
    //     height: height * .80,
    //     // marginBottom: height * .20,
    //     alignItems: 'center'
    // },
})
