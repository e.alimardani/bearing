import React, {Component} from 'react';
import {Alert, View, StyleSheet, TouchableOpacity, Image, Dimensions, Text} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import {ConfirmDialog} from "react-native-simple-dialogs";
import {Container} from "native-base";
import {Actions} from 'react-native-router-flux'
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class bottomNavigate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pic: 'https://www.gravatar.com/avatar/52f0fbcbedee04a121cba8dad1174462?s=200&d=mm&r=g',
        }
    }

    onProfile = () => {
        Actions.push('profile');
    };
    Support = () => {
        this.props.ModalSupport(true)
    };


    render() {
        const {nameLeft, iconLeft, nameCenter, iconCenter, nameRight, iconRight,} = this.props;
        const {color_search = '#6c6c6c', color_group = '#6c6c6c', color_prof = '#6c6c6c'} = this.props;
        return (
            <View style={style.view_bottomNavigate_container}>

                <TouchableOpacity activeOpacity={.5} onPress={this.onProfile}>
                    <View style={style.view_left_bottom}>
                        <Icon size={width * .05} color={color_search} name={iconLeft}/>
                        <Text style={[style.txt_navigate_bottom, {color: color_search}]}>{nameLeft}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity >
                    <View style={style.view_center_bottom}>
                        {/*<Image source={require('../images/add.png')} style={style.img_add_center_bottom} resizeMode={'cover'}/>*/}
                        <Text style={[style.txt_navigate_bottom_add, {color: color_prof}]}>{nameCenter}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.Support}>
                    <View style={style.view_right_bottom}>
                        <Icon size={width * .05} color={color_group} name={iconRight}/>
                        <Text style={[style.txt_navigate_bottom, {color: color_group}]}>{nameRight}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const style = StyleSheet.create({
    view_bottomNavigate_container: {
        backgroundColor: "#fff",
        width: width,
        height: height * .10,
        zIndex: 2,
        position: 'absolute',
        bottom: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    view_left_bottom: {
        // backgroundColor: '#493ff1',
        width: width / 3, height: height * .10, alignItems: 'center',
        justifyContent: 'center'
    },
    view_center_bottom: {
        // backgroundColor: '#ed5d81',
        width: width / 3, height: height * .10, alignItems: 'center',
        justifyContent: 'center',

    },
    view_right_bottom: {
        // backgroundColor: '#c9bc47',
        width: width / 3, height: height * .10, alignItems: 'center',
        justifyContent: 'center'
    },
    view_add_bottom_center: {
        borderWidth: width * .02,
        borderColor: '#419aff40',
        width: width * .18,
        height: width * .18,
        borderRadius: width * .09,
        position: 'absolute',
        bottom: height * .05, zIndex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom:height*.02
    },
    add_icon: {
        // backgroundColor: '#000',
        // width:width*.12,height:width*.12,
        // borderRadius: width*.06,

    },
    img_add_center_bottom: {},
    txt_navigate_bottom: {fontSize: width / 28, fontFamily: 'IRANSansMobile', color: '#000',},
    txt_navigate_bottom_add: {
        fontSize: width / 28,
        fontFamily: 'IRANSansMobile',
        color: '#000',
        position: 'absolute',
        bottom: height * .02
    },
    view_icon_add_bottom_center: {
        justifyContent: 'center', alignItems: 'center', backgroundColor: '#419aff',
        width: width * .12, height: width * .12,
        borderRadius: width * .06,
        // position: 'absolute', bottom: height * .07,
        // zIndex:1
    }
});