import React, {Component} from 'react';
import {Actions} from "react-native-router-flux";
import {
    Dimensions, Image, StyleSheet, Text, TouchableOpacity,
    View,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from "react-redux";
import {setUser} from '../../Redux/Actions/index'


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class drawer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userData:[]
        }
    };

    componentDidMount() {


    }

    exitAccount = () => {
        AsyncStorage.removeItem('UserToken');
        Actions.replace('login')
    }
    render() {

        return (

            <View style={style.drawer_container}>
                
                    <Image source={require('../images/bannermenu.png')} resizeMode={'cover'} style={style.img_header_drawer}/>
                    <View style={{alignItems:'center',position:'absolute',top:70,right:width*0.2}}>
                        <Image resizeMode="contain" source={require('../images/logo_top1.png')} style={{width:100,height:100}}/>
                        <Text style={{textAlign:'center',color:'white',fontSize:16,marginTop:15,fontFamily:'IRANSansMobile'}}>{this.props.user.firstname} {this.props.user.lastname}</Text>
                    </View>


                <View style={style.center_drawer}>

                    {/* <Image source={require('../images/user.png')}
                           style={style.img_instegram}
                           resizeMode={'contain'}/>
                    <Text style={style.txt_menu}>پروفایل کاربری</Text> */}
                    {/* <Image source={require('../images/medical.png')}
                           style={style.img_instegram1}
                           resizeMode={'contain'}/>
                    <Text style={style.txt_menu}>تماس با پشتیبانی</Text> */}
                    <TouchableOpacity activeOpacity={.5} style={style.exitBtn} onPress={this.exitAccount}>
                        <Image source={require('../images/power.png')}
                               style={style.img_instegram1}
                               resizeMode={'contain'}/>
                        <Text style={style.txt_menu}>خروج از حساب کاربری</Text>
                    </TouchableOpacity>

                </View>

                {/* <View style={style.bottom_drawer}>
                    <View style={style.left_bottom_drawer}>
                        <Image source={require('../images/Telegram.png')}
                               style={style.img_instegram}
                               resizeMode={'contain'}/>
                        <Text style={style.txt_menu}>تلگرام</Text>
                    </View>
                    <View style={style.center_bottom_drawer}>
                        <Image source={require('../images/Instegram.png')}
                               style={style.img_instegram}
                               resizeMode={'contain'}/>
                        <Text style={style.txt_menu}>اینستگرام</Text>
                    </View>
                    <View style={style.right_bottom_drawer}>
                        <Image source={require('../images/websaite.png')}
                               style={style.img_instegram}
                               resizeMode={'contain'}/>
                        <Text style={style.txt_menu}>وبسایت</Text>
                    </View>
                </View> */}

            </View>

        )
    }
}
const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(drawer);

const style = StyleSheet.create({
    exitBtn:{justifyContent:'center',alignItems:'center'},
    img_header_drawer: {width: width * .90, height: height * .42},
    drawer_container: {flex: 10},
    top_drawer: {flex: 3,},
    center_drawer: {flex: 6, justifyContent: 'center', alignItems: 'center'},
    bottom_drawer: {flex: 1,paddingTop:5, flexDirection: 'row', borderTopWidth: width * .001, borderTopColor: '#dadada'},
    left_bottom_drawer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    center_bottom_drawer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    right_bottom_drawer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    img_instegram: {width: width * .17, height: height * .04},
    img_instegram1: {width: width * .17, height: height * .04, marginTop: height * .04},
    txt_menu: {fontSize: width / 28, marginTop: height * .001, fontFamily: 'IRANSansMobile'}
});
