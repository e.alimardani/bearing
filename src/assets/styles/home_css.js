import React, {Component} from 'react';
import {StyleSheet, Dimensions, Platform} from 'react-native';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
    viewContainerSupport: {
        backgroundColor: "#fff",
        borderRadius: width * .10,
        width: width * .80,
        height: height * .27,
        marginBottom: height * .20,
        alignItems: 'center'
    },
    btnSupport1: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .25,
        height: height * .05,
        backgroundColor: '#f0f6fc',
        borderRadius: width * .05,
        elevation: 1
    },
    btnSupport: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .25,
        height: height * .05,
        backgroundColor: '#2089ff',
        borderRadius: width * .05,
        elevation: 1
    },
    view_top_support: {
        width: width * .80,
        justifyContent: 'center',
        alignItems: 'center'
    },
    view_bottom_support: {
        width: width * .60,
        justifyContent: 'space-around',
        alignItems: 'center',
        height: height * .10,
        flexDirection: 'row'
    },
    txt_top_support: {fontSize: width / 25, color: '#2089ff', fontFamily: 'IRANSansMobile'},
    txt_center_support: {fontSize: width / 22, color: '#000', fontFamily: 'IRANSansMobile'},
    txt_bottom_support: {fontSize: width / 22, color: '#fff', fontFamily: 'IRANSansMobile'},
    txt_bottom_support1: {fontSize: width / 22, color: '#2089ff', fontFamily: 'IRANSansMobile'},
    txt_searchDriver: {color: '#999999', fontSize: width / 25, fontFamily: 'IRANSansMobile', textAlign: 'right'},
    viewContainer_InputSearch: {
        // backgroundColor:'#2089ff',
        borderBottomColor: '#999',
        borderBottomWidth: width * .001,
        marginBottom: height * .02,
        flexDirection: 'row',
        borderTopLeftRadius: width * .10,
        borderTopRightRadius: width * .10, height: height * .08, width: width * .90,
    },
    view_Input: {width: width * .70, paddingHorizontal: width * .05},
    view_iconInput: {
        width: width * .25,
        paddingLeft: width * .05,
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row'
    },
    viewContainerDriver: {
        height: height * .46,
        // backgroundColor: '#7466ff',
        paddingTop: width * .01,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-end'
    },
    viewDriver: {
        flex: 1,
        alignItems: 'center',
        alignSelf: 'flex-end',
        justifyContent: 'space-around',
    },
    imgUserDriver: {
        width: width * .26,
        height: width * .26,
        borderRadius: width * .13
    },
    view_imgDriver: {
        width: width * .28,
        height: width * .28,
        borderWidth: width * .01,
        borderColor: '#f7f8f8',
        borderRadius: width * .14,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 2,
        // backgroundColor: '#000',
    },
    txt_Driver: {fontSize: width / 25, marginVertical: height * .01},
    viewContainer_SearchDriver: {
        height: height * .65,
        width: width,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
        justifyContent: 'flex-start', alignItems: 'center',
    },
    ModalSearchDriver: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
        padding: 0,
        borderTopLeftRadius: width * .15,
        borderTopRightRadius: width * .15,
    },
    tik_img: {width: width * .18, height: width * .18},
    view_home_container: {},
    img_container_home: {
        position: 'absolute',
        zIndex: 0
    },
    view_top_header: {
        paddingHorizontal: width * .05,
    },
    txt_top_header: {
        color: '#fff',
        textAlign: 'center',
        fontSize: width / 25
    },
    menu_top: {width: width * .06, height: height * .03},
    logo_top: {width: width * .11, height: width * .13},
    img_top_home: {
        width: width,
        height: height * .40,
        position: 'absolute',
        zIndex: 0
    },
    dot_img: {
        width: width * .90,
        height: height * .16,
        flexDirection: 'column-reverse'
    },
    view_top_home: {
        width: width,
        height: height * .40,
        zIndex: 1,
        position: 'absolute',
        top: 0,
        alignItems: 'center'
    },
    txt_menu: {fontFamily: 'IRANSansMobile', fontSize: width / 30, color: '#000',textAlign:'center'},
    img_home_tiksan: {width: width * .37, height: width * .37},
    view_underTopMenu_home: {
        width: width * .90,
        backgroundColor: '#7af06c',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    top_rectangle: {
        width: width*.9,
        height: height*.25,
        alignSelf: 'center',
        marginTop: 10

    },
    txt_header_home: {fontSize: width / 25, color: '#000',},
    txt_header_home1: {color: '#fac000', fontWeight: 'bold'},
    view_center_home: {
        width: width,
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row'
    }, view_center_home1: {
        width: width,
        height: height * .20,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1,
        position: 'absolute',
        top: height * .47,
        flexDirection: 'row'
    },
    view_underTopMenu_home_img: {
        width: width / 2.5,
        height: height * .28,
        alignItems: 'center',
        justifyContent: 'center'
    },
    view_underTopMenu_home_txt: {
        width: width / 2.4,
        marginLeft: width * .03,
        alignItems: 'center',
        justifyContent: 'center'
    },
    view_top_menu: {
        // height: height * .10,
        width: width, position: 'absolute', zIndex: 1
    },
    view_bottom_home: {zIndex: 1, position: 'absolute', bottom: 0},
    view_menu_home: {
        width: width/2.5,
        height: width/2.5,
        borderRadius: width / 10,
        backgroundColor: '#fff',
        marginHorizontal: width * .04,
        marginVertical: height * .01,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        paddingHorizontal: width * .02,

    },
    tik_imgTopApproved: {width: width * .13, height: height * .13, margin: 0, padding: 0},
    txt_topApproved: {fontFamily: 'IRANSansMobile', fontSize: width / 30, color: '#000'},
    view_top_Approved: {
        width: width * .80,
        height: height * .15,
        // backgroundColor: '#fffe7b',
        justifyContent: 'flex-start', alignItems: 'center'
    },
    view_bottom_Approved: {
        // flex: 1,
        width: width * .90,
        height: height * .60,
        // backgroundColor: '#6941d8'
    },
    viewContainerFieldApproved:{
        //backgroundColor: '#6941d8',
        paddingVertical:5,
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center',
        borderTopWidth:width*.001,
        borderTopColor:'#999',

    },
    viewLeftFieldApproved:{
        justifyContent:'center',
        alignItems:'center'
    },
    txt_showAll:{
        fontFamily:'IRANSansMobile',
        backgroundColor: '#f8fbff',
        color: '#4a4b4d',
        fontSize: width / 35,
        paddingHorizontal: width * .05,
        paddingVertical: height * .01 + 2,
        borderRadius: width * .10
    },
    viewCenterFieldApproved:{
        justifyContent:'space-around',
        alignItems:'center',
        flexDirection:'row'
    },
    success_Approved_Img:{
        width:width*.07,
        height:height*.07
    },
    viewRightFieldApproved:{
        width: width * .30,
        height: height * .15,
        // backgroundColor: '#3dd2d8',
        justifyContent:'center',
        alignItems:'center'
    },
    view_request: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .25,
        height: width * .25,
        //backgroundColor: '#ff7569',
        borderRadius: width * .125,
        borderWidth: width * .003,
        borderColor: '#4088fe'
    },
    viewRequest_under: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .19,
        height: height * .04,
        borderTopRightRadius: width * .15,
        borderTopLeftRadius: width * .15
    },
    txt_view_top_header_menu2: {fontSize: width / 35, color: "#000",marginLeft:3, fontFamily: 'IRANSansMobile'},
    menu_top_placeholder: {width: width * .16, height: height * .02},
    viewRequest_under1: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * .19,
        height: height * .03,
    },
    viewRequest_under2: {

        justifyContent: 'center',
        alignItems: 'center',
        // width: width * .2,
        // height: width * .1,
        // backgroundColor: '#407dfe',
        // borderBottomRightRadius: width * .48,
        // borderBottomLeftRadius: width * .48,
        // position:'absolute',
        // bottom:0

    },
    txt_view_top_header_menu3: {
        fontFamily: 'IRANSansMobile',
        fontSize: width / 37,
        color: "black",
    },
    ModalApproved: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 0,
        padding: 0,
        borderTopLeftRadius: width * .12,
        borderTopRightRadius: width * .12,

    },
    viewContainerApproved: {
        height: height * .87,
        width: width * .90,
        backgroundColor: '#fff',
        borderTopLeftRadius: width * .12,
        borderTopRightRadius: width * .12,
        justifyContent: 'flex-start', alignItems: 'center',
        // paddingHorizontal: width * .05
    },
    viewTop_topApproved: {
        // backgroundColor: '#17f0e5',
        width: width * .80,
        height: height * .07,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewTop_centerApproved: {
        // backgroundColor: '#17f0e5',
        width: width * .80,
        height: height * .04,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewTop_bottomApproved: {
        // backgroundColor: '#17f0e5',
        width: width * .80,
        height: height * .04,
        justifyContent: 'center',
        alignItems: 'center'
    },
    transection_img: {width: width * .08, height: height * .02, },

});

