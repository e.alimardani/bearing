import React, {Component} from 'react';
import {Dimensions,BackHandler,ToastAndroid,I18nManager} from 'react-native'
import {Router, Scene, Drawer,Actions,Stack} from 'react-native-router-flux'
import home from "./src/pages/home";
import login from "./src/pages/login";
import drawer from "./src/assets/components/drawer";
import requestDriver from "./src/pages/requestDriver";
import mainPageDriver from "./src/pages/mainPageDriver";
import { connect , Provider } from 'react-redux';
import store from '.././Bearing/src/Redux/Store/index'
import drawerDriver from './src/assets/components/DrawerDriver'
import Profile from './src/pages/Profile'
import RNRestart from 'react-native-restart';

const width = Dimensions.get('window').width;
let backButtonPressedOnceToExit = false;

export default class App extends Component {

        constructor(props) {
        super(props);

        I18nManager.allowRTL(false);

        if(I18nManager.isRTL)
        {
            RNRestart.Restart();
        }
    }

    onBackPress() {
        if (backButtonPressedOnceToExit) {
            BackHandler.exitApp()
        } else {

            if (Actions.currentScene !== 'login' && Actions.currentScene !== 'home' && Actions.currentScene !== "mainPageDriver") {
                Actions.pop();
                return true;
            } else {
                backButtonPressedOnceToExit = true;
                ToastAndroid.show("برای خروج از برنامه دوباره دکمه بازگشت را بفشارید.",ToastAndroid.SHORT);
                //setting timeout is optional
                setTimeout( () => { backButtonPressedOnceToExit = false }, 2000);
                return true;

            }
        }
    }

    render() {
        const RouterWithRedux = connect()(Router);

        return (
            <Provider store={store}>
                <RouterWithRedux backAndroidHandler={this.onBackPress.bind(this)}>
                    <Stack hideNavBar>
                        <Scene key='root' hideNavBar>
                            <Scene key='login' component={login}/>
                            <Drawer drawerPosition={'right'} key='drawer' contentComponent={drawer}>
                                <Scene hideNavBar>
                                    <Scene key='home' component={home} type="reset"/>
                                </Scene>
                            </Drawer>

                            <Drawer drawerPosition={'right'} key='drawerDriver' contentComponent={drawerDriver}>
                                <Scene hideNavBar>
                                    <Scene key='mainPageDriver' component={mainPageDriver} type="reset"/>
                                </Scene>
                            </Drawer>
                            <Scene key='requestDriver' component={requestDriver}/>
                            <Scene key='profile' component={Profile}/> 

                        </Scene>
                    </Stack>
                </RouterWithRedux>
            </Provider>
        );
    }
}

